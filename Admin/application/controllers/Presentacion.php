<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Presentacion extends CI_Controller{
	private $user;
	// private $token;

	function __CONSTRUCT(){
		parent::__construct();

		$this->user = ['user' => RestApi::getUserData()];
		if($this->user['user'] === null)redirect('');

		// Model Respectivo
		$this->load->model('presentacionmodel','presentacion');
		$this->load->model('detallepresentacionmodel','detallepresentacion');
		$this->load->model('materiaindustrialmodel','materiaindustrial');
		
	}

	public function index(){
		$this->load->view('header',$this->user);
		try {
			$result = $this->presentacion->listar();
			$data = $result->data;
		} catch (\Throwable $th) {
			//throw $th;
		}
        $this->load->view('presentacion/index',[
			'model' => $data
		]);
        $this->load->view('footer',
		[
				'footer'=>"lista"
		]);
	}
	
	public function crud($id = 0)
	{

		$this->load->view('header',$this->user);
		//valores de mat ind
		try {
			$result = $this->materiaindustrial->listar();
			$data = $result->data;
			$res = $this->presentacion->obtener($id);
			$construccion = $res->data;
			$presentacion = $res->inf;
		} catch (\Throwable $th) {
			//throw $th;
		}
		$this->load->view('presentacion/crud',[
			'model'=>$data,
			'construccion'=>$construccion,
			'presentacion'=>$presentacion,
			"id"=>$id,
		]);

		$this->load->view('footer',
		[
				'footer'=>"lista"
		]);
	}

	public function registrar($id=0)
	{
		print_r($this->input->post());
		try {
			$presentacion = [
				'Presentacion' => $this->input->post('presentacion'),
				'Descripcion'  => $this->input->post('descripcion')
			];
			if($id === 0){
				$result = $this->presentacion->registrar($presentacion);
				$id = $result->result;
			}
			else{
				$result = $this->presentacion->actualizar($presentacion);
			}
			$this->detallepresentacion->eliminar($id);
			$detalle = $this->input->post('cantidad');
			foreach ($detalle as $key => $value) {
				$data = [
					'idPresentacion' => $id,
					'idMat_ind'		 => $key,
					'Cantidad'		 => $value
				];
				$this->detallepresentacion->registrar($data);
				// print_r($data);
			}


		} catch (\Throwable $th) {
			throw $th;
		}
		redirect(Presentacion);
	}


}

?>