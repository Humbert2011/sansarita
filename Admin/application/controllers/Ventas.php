<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Ventas extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('ventamodel','venta');
    $this->load->model('detalleventamodel','detalleventa');
    $this->load->model('productosmodel','prod');
    $this->load->model('recetamodel','receta');
    $this->load->model('clientemodel','cliente');
    $this->load->model('personalmodel','personal');
    $this->load->model('pedidosmodel','pedidos');
    $this->load->model('transaccionmodel','transaccion');
    $this->token = RestApi::getToken();
    // Model Respectivo
    // $this->load->model('personalmodel','personal');

    // $this->token = RestApi::getToken();
  }
public function generar()
  {
    $this->load->view('header',$this->user);
    try{
      $result = $this->pedidos->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

      $result = $this->venta->listar();
      $venta = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($cliente);
    if(isset($data)){
    $this->load->view('venta/index',
    [
      'model' => $data,
      'cliente' => $cliente,
      'venta' => $venta,
      'token' => $this->token
    ]
    );
  }

    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  
}

  public function crud(){
    $id = $this->user['user']->id;
  $this->load->view('header',$this->user);
    try{
      $result = $this->pedidos->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($cliente);
    if(isset($data)){
    $this->load->view('venta/crud',
    [
      'model' => $data,
      'cliente' => $cliente,
    ]
    );
  }

    $this->load->view('footer',
        [
            'footer' => ""
        ]);
  }

  public function index(){
    $id = $this->user['user']->id;
  $this->load->view('header',$this->user);
    try{
      $result = $this->pedidos->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($cliente);
    if(isset($data)){
    $this->load->view('venta/index',
    [
      'model' => $data,
      'cliente' => $cliente,
    ]
    );
  }

    $this->load->view('footer',
        [
            'footer' => ""
        ]);
  }

 /* public function registrar()
  {    
    $id = $this->user['user']->id;
   //  var_dump($id); #mandar id a la vista y colocarla en input type hidden
    $array = $this->input->post();
    $cantidad = $array['cantidad'];
    $cliente = $array['cliente'];
    //$personal = $array['personal'];
    $this->load->view('header',$this->user);
    try{
     $prod = array();
      foreach ($cantidad as $clave => $valor) {
        $prod[] = $this->prod->obtener($clave);  
      }
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('venta/crud',
      [
        'prod'=>$prod,
        'cantidad'=>$cantidad,
        'cliente' => $cliente,
        //'personal' => $personal
      ]);
    
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }
  */

  public function listaV()
  {
    //$data = $this->venta;
    $this->load->view('header',$this->user);
    try{
      $result = $this->venta->listar();
      $data = $result->data;
      
      $result =$this->pedidos->listar();
      $pedidos =$result->data;

      $result=$this->transaccion->listar();
      $transaccion = $result->data;



    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($pedidos);
    if(isset($data)){
    $this->load->view('venta/listaV',
    [
      'model' => $data,
      'pedidos' => $pedidos,
      'transaccion' => $transaccion
    ]
    );
  }
    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  
}

public function listaT($id)
  {
   if ($id>0) $trans = $this->transaccion->obtener($id);
    $this->load->view('header', $this->user);
    try {
      $result = $this->transaccion->transaccionlista($id);
      $transaccionlista = $result->data;
      
    }catch (Exception $e){
    }
    if (!isset($data)) {
    $this->load->view('venta/listaT',[
      'model' => $result,
      'trans' => $trans
      ]);
    

  }
      $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
}

  public function detalleV($id)
  {
    //var_dump($id);
   if($id>0) $vent = $this->venta->obtener($id);
    $this->load->view('header',$this->user);
    try{
      //if($id>0) $vent = $this->venta->obtener($id);
     // $data = $result->data;
      $result = $this->detalleventa->detalleVenta($id);
      $detalleVenta = $result->data;


    }catch(Exception $e){
      // var_dump($e);
    }
   // var_dump($detalleVenta);
   // var_dump($data);
    if(!isset($data)){
    $this->load->view('venta/detalleV',
    [
      'vent' => $vent,
      'model'=> $result
    ]
    );
     //   var_dump($pedido);    

  }

    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  
}

  public function registrar()
  {
    //var_dump($this->input->post('Descripcion'));echo "Descripcion <br>";
    //var_dump($this->input->post('idprod'));echo "productos <br>";
    //var_dump($this->input->post('numP'));echo "num productos <br>";
    //var_dump($this->input->post('pre'));echo "precio prosducto <br>";
    //var_dump($this->input->post('idCliente'));echo "id <br>";
    //var_dump($this->input->post('total'));echo "totalP <br>";
    $id = $this->user['user']->id;
    $idsPersona = $this->input->post('id');
    //$idPersona = $idsPersona[0];
    var_dump($id); #mandar id a la vista y colocarla en input type hidden
    $idScliente = $this->input->post('idCliente');
    $idCliente = $idScliente[0];
    $idSPedidos = $this->input->post('idPedidos');
    $idPedidos = $idSPedidos[0];
    $total = $this->input->post('total');
    $Descripcion = 'venta a publico en general';
    echo $total;
    if(empty($id)) {
      $data= 
    [
      'idCliente' => $idCliente,
      'Total_Pago' => $total,
      'idPersona' => $id,
      'idPedidos' => $idPedidos,
      'Descripcion' => $Descripcion

    ];
    //var_dump($data);
    // $idCliente = $this->input->post('idCliente');
    // $total = $this->input->post('total');
    }else{
      $data = 
    [
      'idCliente' => $idCliente,
      'Total_Pago' => $total,
      'idPersona' => $id,
       'idPedidos' => $idPedidos,
      'Descripcion' => $Descripcion
      //'Descripcion' => $this->input->post('Descripcion')

    ];
  }
   var_dump($data);
  $this->venta->registrar($data);
    
    //  var_dump($tipo);
    redirect(Ventas);

  }
}
?>
