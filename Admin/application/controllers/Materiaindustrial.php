<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Materiaindustrial extends CI_Controller{
	private $user;
	// private $token;

	function __CONSTRUCT(){
		parent::__construct();

		$this->user = ['user' => RestApi::getUserData()];
		if($this->user['user'] === null)redirect('');

		// Model Respectivo
		$this->load->model('materiaindustrialmodel','matindus');
		// $this->token = RestApi::getToken();	 
	}

	public function index(){
		$this->load->view('header',$this->user);
		try {
			$result = $this->matindus->listar();
			$data = $result->data;
		} catch (\Throwable $th) {
			//throw $th;
		}
		$this->load->view('materiaindustrial/index',
			[
				'model' => $data
			]
		);
        $this->load->view('footer',
			[
				'footer'=>"lista"
			]
		);
	}
	
	public function crud($id = 0){
		$this->load->view('header',$this->user);
		$data = null;
		try {
			if($id>0) $data = $this->matindus->obtener($id);
		} catch (\Throwable $th) {
			//throw $th;
		}
        $this->load->view('materiaindustrial/crud',
			[
				"model"=>$data
			]
		);
        $this->load->view('footer',
		[
				'footer'=>"Materiaprima"
		]);
	}

	public function registrar()
	{
		$id = $this->input->post('id');
		$data =
		[	
			'Mat_ind' 		=> $this->input->post('Mat_ind'),
			'Descripcion' 	=> $this->input->post('Descripcion'),
			'Precentacion' 	=> $this->input->post('Precentacion'),
			'Cantidad' 		=> $this->input->post('Cantidad'),
			'Unidad' 		=> $this->input->post('Unidad'),
			'CodigoBarras'	=> $this->input->post('CodigoBarras'),
        	'Precio'		=> $this->input->post('Precio')
		];
		if(empty($id)){
			$this->matindus->registrar($data);
		  }else{
			$this->matindus->actualizar($data,$id);
		  }
		  redirect(Materiaindustrial);
	}

	public function eliminar($id)
	{
		$this->matindus->eliminar($id);
     	redirect(MateriaPrima);
	}
}

?>