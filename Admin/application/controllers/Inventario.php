<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Inventario extends CI_Controller{
  private $user;
  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
    $this->load->model('almacenesmodel','almacenes');
    $this->load->model('detallealmacenmodel','detallealmacen');
    $this->load->model('productosmodel','prod');
    $this->load->model('materiaprimamodel','materiaprima');
    // $this->token = RestApi::getToken();
  }

  public function index(){
    
  }
  public function listarAlmacenes($a = 1){
    $this->load->view('header',$this->user);
    $data = null;
    try {
      $result = $this->almacenes->listar($a);
      $data = $result->data;
     } catch (\Throwable $th) {
      //throw $th;
    }
    $this->load->view('inventario/almacenes',
    [
      'model'=>$data,
      'modo'=> $a
    ]
    );
    $this->load->view('footer',[
      "footer" => "lista"
    ]);
  }
  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->almacenes->obtener($id);
    $this->load->view('header',$this->user);
    $this->load->view('inventario/crud',
      [
        'model'=>$data
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]);
    }


  public function mat_prima(){
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->detallealmacen->listarMateriaP();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    if(isset($data)){
      $this->load->view('inventario/mat_prima',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

    public function mat_ind(){
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->detallealmacen->listarMateriaInd();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    // var_dump($data);
    if(isset($data)){
      $this->load->view('inventario/mat_ind',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

    public function linea(){
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->detallealmacen->listarLinea();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    // var_dump($data);
    if(isset($data)){
      $this->load->view('inventario/linea',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }
  
    public function inventario(){
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->detallealmacen->listarInventario();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    // var_dump($data);
    if(isset($data)){
      $this->load->view('inventario/inventario',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function add($id = 0){
    $data = null;
    //if($id>0) $data = $this->materiaprima->obtener($id);
    
    $this->load->view('header',$this->user);
    #cargar vista de crud
    //var_dump($data);
    try{
      $result = $this->materiaprima->listar();
      $data = $result->data;
      $recetadata = $this->detallealmacen->obtener($id);
      $detallealmacen = $recetadata->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('inventario/add',
      [
        "model"=>$data,
        "id"=>$id,
        "detallealmacen"=>$detallealmacen
      ]
    );
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

   public function registrarDetalle()
  {
    $array = $this->input->post();
    $cantidad = $array['cantidad'];
    $id = $array['id'];
    $this->detallealmacen->eliminar($id);
    foreach ($cantidad as $key => $value) {
      $data = 
      [
        'Cantidad'=>$value,
        'idMat_Prima'=>$id,
        'idAlmacen'=>$key,
        'idMat_ind'=>$key,
        'linea_idLinea'=>$id
      ];
       print_r($data);
      echo "<br>";
      $this->detallealmacen->registrarDetalle($data);
    }
    redirect('inventario/mat_prima');
  }

  public function registrar(){
    $id = $this->input->post('id');
    $data = 
    [
      'Nombre'=>$this->input->post('Almacen'),
      'Descripcion'=>$this->input->post('Descripcion'),
      'Tipo'=>$this->input->post('Tipo')
    ];
    if(empty($id)){
      $this->almacenes->registrar($data);
    }else{
      $this->almacenes->actualizar($data,$id);
    }
    redirect('inventario/listarAlmacenes');
  }

  public function eliminar($id){
      $data = [
          'Activo' => 0
      ];
      $this->almacenes->actualizar($data,$id);
      redirect('inventario/listarAlmacenes');
  }

  public function activar($id){
      $data = [
          'Activo' => 1
      ];
      $this->almacenes->actualizar($data,$id);
      redirect('inventario/listarAlmacenes');
  }
  

  public function eliminarElementoI($id,$idALmacen){
    // $data = [
    //   'Activo' => 0
    // ];
    $this->detail->eliminar($id);
    redirect('inventario/detail/'.$idALmacen);
  }

  
}
?>
