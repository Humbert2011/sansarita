<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Produccion extends CI_Controller{
	private $user;
	// private $token;

	function __CONSTRUCT(){
		parent::__construct();

		$this->user = ['user' => RestApi::getUserData()];
		if($this->user['user'] === null)redirect('');

		// Model Respectivo
		 $this->load->model('recetamodel','receta');
		 $this->load->model('productosmodel','prod');
		 $this->load->model('almacenesmodel','almacenes');
		 $this->load->model('detallealmacenmodel','detail');
		 $this->load->model('produccionmodel','produccion');
		 $this->load->model('loteproduccionmodel','loteproduccion');	 
	}

	public function index(){
		$this->load->view('header',$this->user);
		#data de la consulta de listar productos
		try{
			$result = $this->prod->listar();
			$data = $result->data;

			$result = $this->almacenes->listar('1');
			$almacenes = $result->data;

		}catch(Exception $e){
			// var_dump($e);
		}
			if(isset($data)){
				$this->load->view('produccion/index',
					[
						'model'=>$data,
						'almacenes'=>$almacenes
					]
				);
			}
		$this->load->view('footer',
		[
				'footer'=>"lista"
		]);
	}


//CAMBIOS ACTUALES 2019
  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->produccion->obtener($id);
    $this->load->view('header',$this->user);
    #cargar vista de crud
    $this->load->view('produccion/crud',
      [
        "model"=>$data
      ]
    );
    $this->load->view('footer',
    [
        'footer'=>"produccion"
    ]);
  }

public function listaProd()
{
	$this->load->view('header',$this->user);
	try{
		$result = $this->produccion->listar();
		$data = $result->data;
	}catch(exception $e){

	}
	//var_dump($data);
	if(isset($data)){
		$this->load->view('produccion/listaProd',
		[
			'model' => $data
		]);
	}
	 $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
}


	public function registrar()
	{
		$array = $this->input->post();
		$cantidad = $array['cantidad'];
		$idalmacen = $array['almacen'];
		$almacen = $array['almacen'];
		//var_dump($array);
		$this->load->view('header',$this->user);
		try{
			$data = array();
			$prod = array();
			foreach ($cantidad as $clave => $valor) {
				$prod[] = $this->prod->obtener($clave);  
				$recetadata = $this->receta->obtener($clave);
				$receta = $recetadata->data;
				//array_push ( $receta , ['valor' => $valor] );
				//$receta[] = ["valor" => $valor]; 
				$data[] = $receta;
			}
			$detalle = $this->detail->obtener($idalmacen);
			$detalle = $detalle->data;
		}catch(Exception $e){
			// var_dump($e);
		}
		$this->load->view('produccion/crud',
			[
				'model'=>$data,
				'prod'=>$prod,
				'cantidad'=>$cantidad,
				'detalle' => $detalle,
				'almacen' => $almacen
			]);
		$this->load->view('footer',
		[
				'footer'=>"lista"
		]);
	}



	public function alta()
	{
		var_dump($this->input->post('idprod'));echo "id_productos <br>";
		var_dump($this->input->post('Mat_Prima'));echo "Mat_prima <br>";
		var_dump($this->input->post('cantidad'));echo "Cantidad <br>";
		var_dump($this->input->post('numP'));echo "num. Producto <br>";
		var_dump($this->input->post('almacen'));echo "num. almacen <br>";
		var_dump($this->input->post('ids_prod'));echo "num. id prods <br>";
		var_dump($this->input->post('total')); echo "total";
		$idSproducto = $this->input->post('idprod');
		$idProducto = $idSproducto[0];
		$total = $this->input->post('total');
		$Cantidad_Real = $this->input->post('numP');
		$idNomenclatura = $this->input->post('almacen');
		$Nomenclatura = $idNomenclatura[0];
		
		$monto = $this->input->post('total');

		$producccion = [
			'Descripcion' => "Produccion del ".date('Y-m-d H:i:s'),
			'Monto' 	  => $monto
		];
		try {
			
			$this->produccion->registrar($producccion);
		} catch (Exception $e) {
			//throw $th;
		}
       	
		
        
        //  redirect(Produccion);
	}
}
?>
