<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Recetas extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
    $this->load->model('materiaprimamodel','materiaprima');
    $this->load->model('recetamodel','receta');
    $this->load->model('productosmodel','prod');
    // $this->token = RestApi::getToken();
  }

  public function index(){
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->receta->listar();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    // var_dump($data);
    if(isset($data)){
      $this->load->view('receta/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    //if($id>0) $data = $this->materiaprima->obtener($id);
    
    $this->load->view('header',$this->user);
    #cargar vista de crud
    //var_dump($data);
    try{
      $result = $this->materiaprima->listar();
      $data = $result->data;
      $result = $this->prod->obtener($id);
      $prod = $result;
      $recetadata = $this->receta->obtener($id);
      $receta = $recetadata->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($recetadata);
    $this->load->view('receta/crud',
      [
        "model"=>$data,
        "id"=>$id,
        "prod"=>$prod,
        "receta"=>$receta
      ]
    );
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function registrar()
  {
    $array = $this->input->post();
    $cantidad = $array['cantidad'];
    $id = $array['id'];
    $this->receta->eliminar($id);
    foreach ($cantidad as $key => $value) {
      $data = 
      [
        'Cantidad'=>$value,
        'idMat_Prima'=>$key,
        'idProducto'=>$id
      ];
      // print_r($data);
      echo "<br>";
      $this->receta->registrar($data);
    }
    redirect(recetas);
    //Cantidad  idMat_Prima idProducto
    // $id = $this->input->post('id');
    //   $data = 
    //   [
    //     'Mat_Prima'=>$this->input->post('Mat_Prima'),
    //     'Descripcion'=>$this->input->post('Descripcion'),
    //     'Precentacion'=>$this->input->post('Precentacion'),
    //     'Cantidad'=>$this->input->post('Cantidad'),
    //     'Unidad'=>$this->input->post('Unidad'),
    //     'CodigoBarras'=>$this->input->post('CodigoBarras')
    //   ];
    //   var_dump($id);  
    //   if(empty($id)){
    //     $this->receta->registrar($data);
    //   }else{
    //     $this->receta->actualizar($data,$id);
    //   }
    //   redirect(receta);
  }
    // public function eliminar($id)
    // {
    //   $this->receta->eliminar($id);
    //  redirect(receta);
    // }
  }
?>
