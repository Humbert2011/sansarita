<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Linea extends CI_Controller{
    private $user;
    public function __construct(){
        parent::__construct();

        $this->user = ['user' => RestApi::getUserData()];
        if($this->user['user'] === null)redirect('');
        // Model Respectivo
        $this->load->model('lineamodel','linea');
        // $this->load->model('productosmodel','producto');
        $this->load->model('recetamodel','receta');
        $this->load->model('presentacionmodel','presentacion');
    }

    public function index()
    {
        $this->load->view('header',$this->user);
        try {
            $result = $this->linea->listar();
            $data = $result->data;
            $this->load->view('linea/index',
            [
                'model'=>$data
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
        $this->load->view('footer',
        [
            'footer'=>"lista"
        ]);
    }
    public function crud($id = 0)
    {
        $data = null;
        $presentacion = null;
        $producto = null;
        if($id>0) $data = $this->linea->obtener($id);
        $this->load->view('header',$this->user);
        #cargar vista de crud
        try {
            $resultReceta = $this->receta->listar();
            $resultPresentacion = $this->presentacion->listar();
            $receta = $resultReceta->data;
            $presentacion = $resultPresentacion->data;
        } catch (\Throwable $th) {
            //throw $th;
        }

        $this->load->view('linea/crud',
        [
            "model"=>$data,
            "receta"=>$receta,
            "presentacion"=>$presentacion
        ]
        );
        $this->load->view('footer',
        [
            'footer'=>"linea"
        ]);
    }

    public function registrar()
    {
        $post = $this->input->post();
        $id = $post['idLinea'];
        unset($post['idLinea']);
        print_r($post);
        echo $id;
        if($id == ''){
            $this->linea->registrar($post);
        }else{
            $this->linea->actualizar($post,$id);
        }
        redirect(Linea);
    }


}
