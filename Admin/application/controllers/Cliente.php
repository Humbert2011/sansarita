<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Cliente extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('clientemodel','cliente');
  }

  public function index(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->cliente->listar();
      $data = $result->data;
      // var_dump($data);
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('cliente/index',
        [
          'model'=>$data
        ]
      );
    // }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = NULL;
    $data2 =NULL;
    $this->load->view('header',$this->user);
    if ($id > 0 ){
      $data = $this->cliente->obtener($id);
      
    }
    $result= $this->cliente->listarPersonal();
      $data2 = $result->data;
    $this->load->view('cliente/crud',
      [
        'model'=>$data,
        'model2'=>$data2
      ]);
    $this->load->view('footer',
    [
        'footer'=>''
    ]);
  }
  public function registrar()
  {
    $id = $this->input->post('id');
    $idpersona2 = $this->input->post('idPersona2');
    var_dump($idpersona2);
    if($idpersona2 == ''){
      $data = 
      [
        'Cliente'=>$this->input->post('Cliente'),
        'Nombre_Comercial'=>$this->input->post('Nombre_Comercial'),
        'RFC'=>$this->input->post('RFC'),
        'Telefono'=>$this->input->post('Telefono'),
        'Correo'=>$this->input->post('Correo'),
        'idPersona1'=>$this->input->post('idPersona1'),
        'Codigo' => $this->input->post('Codigo'),
        'Personal' => $this->input->post('Personal'),
        'Direccion' => $this->input->post('Direccion'),
        'Margen' =>$this->input->post('Margen'),
        'CodigoPostal' => $this->input->post('CodigoPostal')
      ];
    }else {
      $data = 
    [
      'Cliente'=>$this->input->post('Cliente'),
      'Nombre_Comercial'=>$this->input->post('Nombre_Comercial'),
      'RFC'=>$this->input->post('RFC'),
      'Telefono'=>$this->input->post('Telefono'),
      'Correo'=>$this->input->post('Correo'),
      'idPersona1'=>$this->input->post('idPersona1'),
      'idPersona2'=> $idpersona2,
      'Codigo' => $this->input->post('Codigo'),
      'Personal' => $this->input->post('Personal'),
      'Direccion' => $this->input->post('Direccion'),
      'Margen' =>$this->input->post('Margen'),
      'CodigoPostal' => $this->input->post('CodigoPostal')
    ];
    }
    var_dump($idpersona2);
    
    if(empty($id)){
      $this->cliente->registrar($data);
    }else{
      $this->cliente->actualizar($data,$id);
    }
    redirect(Cliente);

  }
  public function eliminar($id){
    $this->cliente->eliminar($id);
    redirect(Cliente);
  }
}
?>
