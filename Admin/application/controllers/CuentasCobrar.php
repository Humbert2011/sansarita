<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class cuentascobrar extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('cuentascobrarmodel','cuentascobrar');
    $this->load->model('clientemodel','cliente');
    $this->load->model('pedidosmodel','pedidos');
    $this->load->model('transaccionmodel','transaccion');

    // Model Respectivo
    // $this->load->model('personalmodel','personal');

    // $this->token = RestApi::getToken();
  }

  public function index(){
    //$id = $this->user['user']->id;
    //var_dump($id); #mandar id a la vista y colocarla en input type hidden
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->cuentascobrar->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

      $result = $this->pedidos->listar();
      $pedidos = $result->data;

      $result = $this->transaccion->listar();
      $transaccion = $result->data;

      //$result = $this->personal->listar('1');
      //$personal = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
      if(isset($data)){
        $this->load->view('cuentascobrar/index',
          [
            'model'=>$data,
            'cliente'=>$cliente,
            'transaccion'=>$transaccion
          ]
        );
      }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }  

public function crud(){
    $id = $this->user['user']->id;
  $this->load->view('header',$this->user);
    try{
      $result = $this->pedidos->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($cliente);
    if(isset($data)){
    $this->load->view('venta/crud',
    [
      'model' => $data,
      'cliente' => $cliente,
    ]
    );
  }

    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  }
}
?>
