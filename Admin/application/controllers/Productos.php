<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Productos extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('productosmodel','prod');
    // $this->token = RestApi::getToken();
  }

  public function index(){
    $footer = 'Productos';
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->prod->listar();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    if(isset($data)){
      $this->load->view('productos/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->prod->obtener($id);
    $this->load->view('header',$this->user);
    $this->load->view('productos/crud',
      [
        'model'=>$data
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]);
  }

  public function registrar()
  {
    $id = $this->input->post('id');
    $Precioprod = $this->input->post('PrecioProd');
    if($Precioprod == null){
        $Precioprod = 0;
    }
    $data = 
    [
      'Producto'=>$this->input->post('Producto'),
      'Descripcion'=>$this->input->post('Descripcion'),
      'PrecioProd'=>$Precioprod
      // 'PrecioDistribuidor' => $this->input->post('PrecioDistribuidor'),
      // 'PrecioPublico'=>$this->input->post('PrecioPublico'),
      // 'CodigoBarras'=>$this->input->post('CodigoBarras'),
      // 'Precentacion'=>$this->input->post('Precentacion'),
      // 'Unidad'=>$this->input->post('Unidad')
    ];
    if(empty($id)){
      print_r($data);
      $res = $this->prod->registrar($data);
      $id = $res->result;
      var_dump($res);
    }else{
      $this->prod->actualizar($data,$id);
    }
    redirect('productos/crud/'.$id);
  }

  public function eliminar($id)
  {
     $this->prod->eliminar($id);
     redirect(Productos);
  }

}
 ?>
