<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Compraproducto extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('compraproductomodel','compraproducto');
    $this->load->model('compramodel','compra');
    $this->load->model('materiaprimamodel','materiaprima');
    $this->load->model('lineamodel','linea');
    // $this->load->model('proveedormodel','proveedor');
    $this->load->model('materiaindustrialmodel','matindus');
    $this->load->model('productosmodel','prod');
    $this->load->model('almacenesmodel','almacenes');
  }

  public function index(){
    //$footer = 'Compraproducto';
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->compraproducto->listar();
      $data = $result->data;
    }catch(Exception $e){
      // var_dump($e);
    }
    if(isset($data)){
      $this->load->view('compraproducto/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    $materiaprima = null;
    $linea = null;
    $matindus = null;
    $proveedor = null;
    if($id>0) $data = $this->prod->obtener($id);
    $this->load->view('header',$this->user);
    try {
        $resultReceta = $this->materiaprima->listar();
        $resultPresentacion = $this->matindus->listar();
        $almacenes = $this->almacenes->listar(1);
        $resultLinea = $this->linea->listar();
        $almacenes = $almacenes->data;
        $materiaprima = $resultReceta->data;
        $matindus = $resultPresentacion->data;
        $linea = $resultLinea->data;
        // $proveedor = $resultproveedor->data;
    } catch (\Throwable $th) {
        //throw $th;
    }
    $this->load->view('compraproducto/crud',
      [
        'model'=>$data,
        'materiaprima'=>$materiaprima,
        'matindus'=>$matindus,
        'proveedor'=>$proveedor,
        'almacenes' => $almacenes,
        'linea' => $linea
      ]
    );
    $this->load->view('footer',
        [
            'footer' => "compraproducto"
        ]);
  }

    public function registrar()
    {
        $post = $this->input->post();
        $id = $post['idCompra_Producto'];
        unset($post['idCompra_Producto']);
        print_r($post);
        echo $id;
        if($id == ''){
            $this->compraproducto->registrar($post);
        }else{
            $this->compraproducto->actualizar($post,$id);
        }
        redirect(CompraProducto);
    }


  public function eliminar($id)
  {
     $this->prod->eliminar($id);
     redirect(CompraProducto);
  }

}
 ?>