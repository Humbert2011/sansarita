<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Pedidos extends CI_Controller{
  private $user;
  private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
     $this->load->model('recetamodel','receta');
     $this->load->model('productosmodel','prod');
     $this->load->model('almacenesmodel','almacenes');
     $this->load->model('detallealmacenmodel','detail');
     $this->load->model('pedidosmodel','pedidos');
     $this->load->model('clientemodel','cliente');
     $this->load->model('detallepedidosmodel','detallepedidos');
     $this->token = RestApi::getToken();    
  }

  public function index(){
    $id = $this->user['user']->Nombre;
    //var_dump($id);
    $this->load->view('header',$this->user);
    #data de la consulta de listar productos
    try{
      $result = $this->prod->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;
          
      $result = $this->prod->listar();
      $prod = $result->data;


    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($prod);
      if(isset($data)){
        $this->load->view('pedidos/index',
          [
            'model'=>$data,
            'cliente'=>$cliente,
            'prod'=>$prod,
            'token' => $this->token
          ]
        );
      }
    $this->load->view('footer',
    [
        'footer'=>""
    ]);
  }


//CAMBIOS ACTUALES 2019
  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->pedidos->obtener($id);
    $this->load->view('header',$this->user);
    #cargar vista de crud
    $this->load->view('pedidos/crud',
      [
        "model"=>$data
      ]
    );
    $this->load->view('footer',
    [
        'footer'=>"pedidos"
    ]);
  }




  public function registrar()
  {
    $id = $this->user['user']->Nombre;
    //var_dump($id);
    $array = $this->input->post();
    $cantidad = $array['cantidad'];
    $cliente = $array['cliente'];
    $this->load->view('header',$this->user);
    try{
     $prod = array();
      foreach ($cantidad as $clave => $valor) {
        $prod[] = $this->prod->obtener($clave);  
      }
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('pedidos/crud',
      [
        'prod'=>$prod,
        'cantidad'=>$cantidad,
        "cliente"=>$cliente
        //'detalle' => $detalle
      ]);
    
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function listaP()
  {
    $this->load->view('header',$this->user);
    try{
      $result = $this->pedidos->listar();
      $data = $result->data;

      $result = $this->cliente->listar();
      $cliente = $result->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    //var_dump($cliente);
    if(isset($data)){
    $this->load->view('pedidos/listaP',
    [
      'model' => $data,
      'cliente' => $cliente
    ]
    );
  }

    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  
}

  public function detalleP($id)
  {
   if($id>0) $pedido = $this->pedidos->obtener($id);
    $this->load->view('header',$this->user);
    try{
      $result = $this->detallepedidos->detallePedidos($id);
      $result = $result->data;

      $almacen = $this->almacenes->listar(1);
      $a = $almacen->data;

    }catch(Exception $e){
      // var_dump($e);
    }
    if(!isset($data)){
    $this->load->view('pedidos/detalleP',
    [
      'pedido'=> $pedido,
      'model'=> $result,
      'almacenes' => $a
    ]
    );
  }
    $this->load->view('footer',
        [
            'footer' => "lista"
        ]);
  
}
  public function alta()
  {
      // print_r($this->input->post());
      $ivaVal = $this->input->post('ivaVal');
      if ($ivaVal>0) {
        $ivaVal = 1;
      }
      $data = [
        "idCliente" => $this->input->post('cliente'),
        "Total"=>$this->input->post('totalPVal'),
        "SubTotal"=>$this->input->post('subtotalVal'),
        "IVA"=>$ivaVal,
        "Notas"=>$this->input->post('Notas'),
        "Politicas"=>$this->input->post('Politicas')
      ];
      print_r($data);
      try{
        $result = $this->pedidos->registrar($data);
        $idPedido = $result->result;
        // $idPedido=$altaPedido;
      }catch(Exception $e){
        // var_dump($e);
      }
      // $idPedido = null;
      $productos = $this->input->post('idProducto');
      $cantidad = $this->input->post('cantidad');
      $descuento = $this->input->post('descuento');
      $monto = $this->input->post('subtotalUnitario');
      $prodL = count($productos);
      for ($i=0; $i < $prodL ; $i++) {
          $data = [
            "idProducto" => $productos[$i],
            "Cantidad" => $cantidad[$i],
            "Descuento" => $descuento[$i],
            "monto" => $monto[$i],
            "idPedido" => $idPedido
          ];
          try{
            $result = $this->detallepedidos->registrar($data);
            // $idPedido = $result->result;
            // $idPedido=$altaPedido;
          }catch(Exception $e){
            // var_dump($e);
          }
          print_r($data);
      }
      redirect('Pedidos/listaP');
  }
}
?>