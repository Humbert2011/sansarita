<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Personal extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
    $this->load->model('personalmodel','personal');
    $this->load->model('tipopersonamodel','tipo');
    $this->load->model('areasmodel','area');
    $this->load->model('imgmodel','img');

    // $this->token = RestApi::getToken();
  }

  public function administrador(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->personal->listar(1);
      $data = $result->data;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('personal/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }
  
  public function vendedor(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->personal->listar(2);
      $data = $result->data;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('personal/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function cosinero(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->personal->listar(3);
      $data = $result->data;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('personal/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }


  public function crud($id = 0){
    $data = null;
    $cargos = $this->tipo->listar();
    $areas = $this->area->listar();

    if ($id > 0) {
      $data = $this->personal->obtener($id);      
    }    
    $this->load->view('header',$this->user);
    $this->load->view('personal/crud',
      [
        'model'=>$data,
        'cargos'=> $cargos->result,
        'areas'=> $areas->result
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]
    );

  }

  public function registrar(){
    $id = $this->input->post('id');
    $data = 
    [
      'Nombre'=>$this->input->post('Nombre'),
      'Apellidos'=>$this->input->post('Apellidos'),
      'Telefono'=>$this->input->post('Telefono'),
      'Email'=>$this->input->post('Email'),
      'Password'=>$this->input->post('Password'),
      'Grado_estudios'=>$this->input->post('GradoEstudios'),
      'idtipoPersona'=>$this->input->post('Cargo'),
      'idArea'=>$this->input->post('Area')
    ];
    $dir_subida = 'C:\AppServ\www\sansarita\Backend\img/';#ruta de subida local #;linux
    $dir_server = 'http://localhost:8080/sansarita/Backend/img/';#linux
    // $dir_server = 'http://admin.sansarita.com/Backend/img/';#server
    // $dir_subida ='/home/stardust007/public_html/admin/Backend/img/';#ruta de subida en linea
		$fichero_subido = $dir_subida . basename($_FILES['img']['name']);
        $NombreArchivo = $dir_server . basename($_FILES['img']['name']);
    move_uploaded_file($_FILES['img']['tmp_name'], $fichero_subido);

    $form =$_FILES['img'];
    if (empty($this->input->post('Password'))) {
      unset($data['Password']);      
    }    
    if(empty($id)){
      $this->personal->registrar($data);
    }else{
      $img = $this->img->cargar('persona',$id,$form);
      var_dump($img);
      $val = $this->personal->actualizar($data,$id);
    }
    redirect(site_url('Personal/administrador'));
    
  }

  public function eliminar($id){
    
  }
  
}
 ?>
