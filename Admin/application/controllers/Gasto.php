<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Gasto extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('gastomodel','gasto');
     $this->load->model('tipogastomodel','tipogasto');
    
  }

  public function index(){
    // $footer = 'Gasto';
    $this->load->view('header',$this->user);
    #data de la consulta de listar Gasto
    $de = $this->input->post('de');
    $a = $this->input->post('a');
    // var_dump($de);
    // var_dump($a);
    if(is_null($de)){
      $de = date("Y-m-d");
      $a = date("Y-m-d");
    }
    try{
      $result = $this->gasto->listar($de,$a);
      $data = $result->data;
      // var_dump($data);
    }catch(Exception $e){
      // var_dump($e);
    }
    // $this->load->view('gasto/index');
    if(isset($data)){
      $this->load->view('gasto/index',
        [
          'model'=>$data,
          'de'=>$de,
          'a'=>$a
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->gasto->obtener($id);
    $this->load->view('header',$this->user);

        try {
            $resultGasto = $this->tipogasto->listar();
            $tipogasto = $resultGasto->data;
        } catch (\Throwable $th) {
            //throw $th;
        }
        // var_dump($tipogasto);
    $this->load->view('gasto/crud',
      [
        'model'=>$data,
        'tipogasto'=>$tipogasto
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]);
  }

  public function registrar()
  {
    $id = $this->input->post('id');
    $data = 
    [
      'Concepto'=>$this->input->post('Concepto'),
      'idTipo_Gasto'=>$this->input->post('idTipo_Gasto'),
      'Fecha'=>$this->input->post('Fecha'),
      'Precio'=>$this->input->post('Precio'),
    ];
    if(empty($id)){
      $this->gasto->registrar($data);
    }else{
      $this->gasto->actualizar($data,$id);
    }
    redirect(Gasto);
  }

  public function eliminar($id)
  {
     $this->gasto->eliminar($id);
     redirect(Gasto);
  }

}
 ?>
