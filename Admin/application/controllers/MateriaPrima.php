<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Materiaprima extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
    $this->load->model('materiaprimamodel','materiaprima');
    // $this->token = RestApi::getToken();
  }

  public function index(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->materiaprima->listar();
      $data = $result->data;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('materiaprima/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    if($id>0) $data = $this->materiaprima->obtener($id);
    $this->load->view('header',$this->user);
    #cargar vista de crud
    $this->load->view('materiaprima/crud',
      [
        "model"=>$data
      ]
    );
    $this->load->view('footer',
    [
        'footer'=>"Materiaprima"
    ]);
  }

  public function registrar()
  {
    $id = $this->input->post('id');
    $data = 
      [
        'Mat_Prima'=>$this->input->post('Mat_Prima'),
        'Descripcion'=>$this->input->post('Descripcion'),
        'Precentacion'=>$this->input->post('Precentacion'),
        'Cantidad'=>$this->input->post('Cantidad'),
        'Unidad'=>$this->input->post('Unidad'),
        'CodigoBarras'=>$this->input->post('CodigoBarras'),
        'Precio'=>$this->input->post('Precio')
      ];
      var_dump($id);  
      if(empty($id)){
        $this->materiaprima->registrar($data);
      }else{
        $this->materiaprima->actualizar($data,$id);
      }
      redirect(MateriaPrima);
    }

    public function eliminar($id)
    {
      $this->materiaprima->eliminar($id);
     redirect(MateriaPrima);
    }
    
  }
?>
