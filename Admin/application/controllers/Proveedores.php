<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Proveedores extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('personalmodel','personas');   
    $this->load->model('proveedormodel','proveedor');  


    // $this->token = RestApi::getToken();
  }
  
  public function index(){
    $this->load->view('header',$this->user);
    try{
      $data = $this->proveedor->listar();
      // $data = $result->result;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('proveedores/index',
        [
          'model'=>$data->result
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  } 

  public function crud($id = 0){
    $data = null;
    // $proveedores = $this->proveedor->listar();
    $result = $this->personas->listartodos();
    $contactos = $result->result;
    // var_dump($contactos);
    if ($id > 0) {
      $data = $this->proveedor->obtener($id);      
    }    
    $this->load->view('header',$this->user);
    $this->load->view('proveedores/crud',
      [
        'model'=>$data,
        'contactos'=> $contactos
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]
    );
  }

  public function registrar()
  {
    $id = $this->input->post('id');
    $data = 
      [
        'Proveedor'=>$this->input->post('Proveedor'),
        'Direccion'=>$this->input->post('Direccion'),
        'Telefono'=>$this->input->post('Telefono'),
        'RFC'=>$this->input->post('RFC'),
        'idPersona'=>$this->input->post('contacto1'),
        'idPersona2'=>$this->input->post('contacto2')
      ];
      var_dump($data);  
      if(empty($id)){
        $this->proveedor->registrar($data);
      }else{
        $this->proveedor->actualizar($data,$id);
      }
      redirect(Proveedores);
    }

public function eliminar($id)
  {
     $this->proveedor->eliminar($id);
     redirect(Proveedores);
  }
  
/*
  public function registrar(){
    $id = $this->input->post('id');
    $data = 
    [
      'Nombre'=>$this->input->post('Nombre'),
      'Apellidos'=>$this->input->post('Apellidos'),
      'Telefono'=>$this->input->post('Telefono'),
      'Email'=>$this->input->post('Email'),
      'Password'=>$this->input->post('Password'),
      'Foto'=>$_FILES['FotoPerfil'],
      'Grado_estudios'=>$this->input->post('GradoEstudios'),
      'idtipoPersona'=>$this->input->post('Cargo'),
      'idArea'=>$this->input->post('Area')
    ];
    // var_dump($data);
    if (empty($this->input->post('Password'))) {
      unset($data['Password']);      
    }
    // var_dump($data);
    if(empty($id)){
      $this->personal->registrar($data);
    }else{
      $this->personal->actualizar($data,$id);
    }
    redirect(site_url('Proveedores/index'));    
  }
*/  

}
?>
