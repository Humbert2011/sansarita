<?php
 class LoteproduccionModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "loteproduccion/listar"
        );
    }

    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "loteproduccion/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "loteproduccion/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "loteproduccion/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "loteproduccion/eliminar/$id"
        );
    }
}