<?php
 class TransaccionModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "transaccion/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "transaccion/obtener/$id"
        );
    }
    
    public function transaccionlista($id)
    {
        return RestApi::call(
            RestApiMethod::GET,
            "transaccion/transaccionlista/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "transaccion/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "transaccion/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "transaccion/eliminar/$id"
        );
    }
}