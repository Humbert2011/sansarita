 <?php
 class CompraProductoModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "compraproducto/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "compraproducto/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "compraproducto/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "compraproducto/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "compraproducto/eliminar/$id"
        );
    }
}