<?php
 class GastoModel extends CI_Model{
    public function listar($de,$a){
        return RestApi::call(
            RestApiMethod::GET,
            "gasto/listar/$de/$a"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "gasto/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "gasto/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "gasto/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "gasto/eliminar/$id"
        );
    }
}