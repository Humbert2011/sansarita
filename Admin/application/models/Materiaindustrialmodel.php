<?php
 class MateriaindustrialModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "materiaindustrial/listar"
        );
    }

    public function listarProduccion($var){
        return RestApi::call(
            RestApiMethod::GET,
            "materiaindustrial/listarProd/$var"
        );
    }
       public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "materiaindustrial/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "materiaindustrial/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "materiaindustrial/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "materiaindustrial/eliminar/$id"
        );
    }
}