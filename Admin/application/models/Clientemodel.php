<?php
 class ClienteModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "cliente/listar"
        );
    }
    public function listarPersonal()
    {
        return RestApi::call(
            RestApiMethod::GET,
            "cliente/listarPersonal"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "cliente/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "cliente/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "cliente/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "cliente/eliminar/$id"
        );
    }
}