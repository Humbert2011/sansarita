 <?php
 class MateriaPrimaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "materiaprima/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "materiaprima/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "materiaprima/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "materiaprima/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "materiaprima/eliminar/$id"
        );
    }
}