 <?php
 class PersonalModel extends CI_Model{
    public function listar($tipo){
        return RestApi::call(
            RestApiMethod::GET,
            "personal/listar/".$tipo
        );
    }
    public function listartodos(){
        return RestApi::call(
            RestApiMethod::GET,
            "persona/listartodos"
        );
    }


   public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "persona/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "persona/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "persona/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "persona/eliminar/$id"
        );
    }
}