<?php
 class DetallepresentacionModel extends CI_Model{
    public function listar($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detallepresentacion/listar/$id"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detallepresentacion/obtener/$id"
        );
    }
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "detallepresentacion/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "detallepresentacion/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "detallepresentacion/eliminar/$id"
        );
    }
}