<?php
 class VentaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "venta/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "venta/obtener/$id"
        );
    }

    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "venta/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "venta/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "venta/eliminar/$id"
        );
    }
}