<?php
 class DetalleventaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "detalleventa/listar"
        );
    }
       public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detalleventa/obtener/$id"
        );
    }
   public function detalleVenta($id)
   {
    return RestApi::call(
        RestApiMethod::GET,
        "detalleventa/detalleVenta/$id"
    );

   }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "detalleventa/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "detalleventa/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "detalleventa/eliminar/$id"
        );
    }
}