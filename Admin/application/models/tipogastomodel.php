<?php
 class TipogastoModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "tipogasto/listar"
        );
    }
    // public function listarPersonal()
    // {
    //     return RestApi::call(
    //         RestApiMethod::GET,
    //         "linea/listarPersonal"
    //     );
    // }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "tipogasto/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "tipogasto/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "tipogasto/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "tipogasto/eliminar/$id"
        );
    }
}