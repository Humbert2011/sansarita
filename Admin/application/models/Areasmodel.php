 <?php
 class AreasModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "area/listar"
        );
    }
}