 <?php
 class AlmacenesModel extends CI_Model{
    public function listar($a){
        return RestApi::call(
            RestApiMethod::GET,
            "almacenes/listar/$a"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "almacenes/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "almacenes/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "almacenes/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "almacenes/eliminar/$id"
        );
    }
}