<?php
 class CompraModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "compra/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "compra/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "compra/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "compra/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "compra/eliminar/$id"
        );
    }
}