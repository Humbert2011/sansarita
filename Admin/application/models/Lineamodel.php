<?php
 class LineaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "linea/listar"
        );
    }
    // public function listarPersonal()
    // {
    //     return RestApi::call(
    //         RestApiMethod::GET,
    //         "linea/listarPersonal"
    //     );
    // }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "linea/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "linea/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "linea/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "linea/eliminar/$id"
        );
    }
}