<?php
 class PresentacionModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "presentacion/listar"
        );
    }
       public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "presentacion/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "presentacion/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "presentacion/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "presentacion/eliminar/$id"
        );
    }
}