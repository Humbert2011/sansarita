<?php
 class CuentascobrarModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "cuentascobrar/listar"
        );
    }

    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "cuentascobrar/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "cuentascobrar/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "cuentascobrar/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "cuentascobrar/eliminar/$id"
        );
    }
}