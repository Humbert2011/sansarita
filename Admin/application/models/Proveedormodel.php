 <?php
 class ProveedorModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "proveedor/listar"
        );
    }

   public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "proveedor/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "proveedor/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "proveedor/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "proveedor/eliminar/$id"
        );
    }
}