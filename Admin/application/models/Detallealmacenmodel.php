<?php
 class DetallealmacenModel extends CI_Model{
    public function listarMateriaP(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarMateriaP"
        );
    }

    public function listarMateriaInd(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarMateriaInd"
        );
    }

   public function listarLinea(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarLinea"
        );
    }
 
    public function listarInventario(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarInventario"
        );
    }

    public function listarProd($var){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarProd/$var"
        );
    }
    public function listarNoEx($id)
    {
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listarNoEx/$id"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/obtener/$id"
        );
    }
    public function registrarDetalle($data){
        return RestApi::call(
            RestApiMethod::POST,
            "detallealmacen/registrarDetalle",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "detallealmacen/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "detallealmacen/eliminar/$id"
        );
    }
}