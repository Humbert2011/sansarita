<?php
 class DetalleproduccionModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallealmacen/listar"
        );
    }

    public function listarProduccion($var){
        return RestApi::call(
            RestApiMethod::GET,
            "detalleproduccion/listarProd/$var"
        );
    }
       public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detalleproduccion/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "detalleproduccion/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "detalleproduccion/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "detalleproduccion/eliminar/$id"
        );
    }
}