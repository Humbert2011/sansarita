 <?php
 class ProduccionModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "produccion/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "produccion/obtener/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "produccion/registrar",
            $data
        );
    }
        public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "produccion/actualizar/$id",
            $data
        );
    }
    
}
