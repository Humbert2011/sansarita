<?php
 class DetallepedidosModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "detallepedidos/listar"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "detallepedidos/obtener/$id"
        );
    }
    
    public function detallePedidos($id)
    {
        return RestApi::call(
            RestApiMethod::GET,
            "detallepedidos/detallePedidos/$id"
        );
    }
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
            "detallepedidos/registrar",
            $data
        );
    }
    public function actualizar($data,$id){
        return RestApi::call(
            RestApiMethod::PUT,
            "detallepedidos/actualizar/$id",
            $data
        );
    }
    public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "detallepedidos/eliminar/$id"
        );
    }
}