<!--  -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Gastos</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Gastos</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                           <?php echo form_open('Gasto/index'); ?>
                           De:&nbsp;<input name="de" class="navbar-form" type="date" value="<?php echo $de; ?>" placeholder="">
                           <span></span>
                           A:&nbsp;<input name="a" class="navbar-form" type="date" value="<?php echo $a; ?>" placeholder="">

                            <button type="submit" class="btn btn-info" ><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                            <!-- <input type="submit" name="Buscar" value=" Buscar" class="btn btn-info" > -->
                           <?php form_close(); ?>
                            <a href="<?php echo site_url('Gasto/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>IdGasto</th>
                       <th>Concepto</th>
                       <th>Tipo Gasto</th>
                       <th>Fecha</th>
                       <th>Precio</th>
                       <!-- <th>Unidad</th> -->
                       <!-- <th>Action</th> -->
                   </tr>
                   </thead>
                   <tbody>
                    <?php $total = 0; ?>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idGasto; ?></td>
                        <td><?php echo $m->Concepto; ?></td>
                        <td><?php echo $m->Tipo_Gasto; ?></td>
                        <td><?php echo $m->Fecha; ?> </td>
                        <td>$ <?php echo number_format($m->Precio,2); ?></td>
                        <!-- <td><?php echo $m->Unidad; ?></td> -->
                        <!-- <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('Gasto/crud/'.$m->idGasto) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('Gasto/eliminar/'.$m->idGasto) ?>"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>
                        </td> -->
                        <?php $total = $total + $m->Precio; ?>
                    </tr>
                   <?php endforeach; ?>
                    <tr>
                       <th>Total</th>
                       <th></th>
                       <th></th>
                       <th></th>
                       <th>$<?php echo number_format($total,2); ?></th>
                   </tr>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>

       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Gasto?');
         }
       </script>
