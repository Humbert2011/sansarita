<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Gasto';
    if(is_object($model)){
        $titulo = 'Actualizar Gasto';
    } 
?>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Gasto/index/'); ?>" >Gasto</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Gasto Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <!-- <td><?php echo $m->Concepto; ?></td>
                        <td><?php echo $m->Tipo_Gasto; ?></td>
                        <td><?php echo $m->Fecha; ?> </td>
                        <td><?php echo $m->Precio; ?></td> -->
                        <div class="ibox-content">
                          <!-- <?php echo form_open('Gasto/registrar'); ?> -->
                        
                            <form class="form-horizontal" action="<?php echo site_url('Gasto/registrar'); ?>" method="post">
                                                              <p>Llena todos los campos para agregar un Gasto.</p>
                                                              <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idGasto;}  ?>">
                                
                                <div class="form-group"><label class="col-lg-2 control-label">Concepto</label>

                                    <div class="col-lg-10"><input name="Concepto" type="text" placeholder="Concepto" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Concepto;}  ?>"></div>
                                </div>
                                  <div class="form-group"><label class="col-lg-2 control-label">Tipo_Gasto</label>

                                    <div class="col-lg-10">
                                        <select name="idTipo_Gasto" id="idTipo_Gasto" class="form-control">
                                            <?php foreach ($tipogasto as $rec ):?>
                                            <option value="<?php echo $rec->idTipo_Gasto ?>" > <?php echo $rec->Tipo_Gasto ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Fecha</label>
                                    <div class="col-lg-10"><input name="Fecha" type="date" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Fecha;}else{
                                      echo date("Y-m-d");
                                    }  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Monto</label>
                                    <div class="col-lg-10"><input name="Precio" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Precio;}  ?>" ></div>                                    
                                </div>
                                <!-- div class="form-group"><label class="col-lg-2 control-label">Precentacion</label>
                                    <div class="col-lg-10"><input name="Precentacion" type="text" placeholder="Precentacion del Gasto" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Precentacion;}  ?>"></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Unidad</label>
                                    <div class="col-lg-10"><input name="Unidad" type="text" placeholder="Precentacion del Gasto" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Unidad;}  ?>"></div>                                    
                                </div> -->
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Gasto</button>
                                    </div>
                                </div>
                                <!-- <?php form_close(); ?> -->
                            </form>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                  // $(document).ready(function() {
                  //   document.getElementById("Fecha").value = new Date();
                  // });
                </script>