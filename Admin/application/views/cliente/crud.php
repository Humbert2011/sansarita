<?php 
    //  var_dump($model2);
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Cliente';
    if(is_object($model)){
        $titulo = 'Actualizar Cliente';
    } 
?>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Cliente/index/'); ?>" >Cliente</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Cliente Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        
                        <div class="ibox-content">
                          <!-- <?php echo form_open('Cliente/registrar'); ?> -->
                        
                            <form class="form-horizontal" action="<?php echo site_url('Cliente/registrar'); ?>" method="post">
                                                              <!-- <p>Llena todos los campos para agregar un Cliente.</p> -->
                                                              <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idCliente;}  ?>">
                                
                                <div class="form-group"><label class="col-lg-2 control-label">Razon Social</label>

                                    <div class="col-lg-10"><input id="Cliente" name="Cliente" type="text" placeholder="Cliente" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Cliente;}  ?>"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Nombre Comercial</label>
                                    <div class="col-lg-10"><input id="Nombre_Comercial" onkeyup = "cc()" name="Nombre_Comercial" type="text" placeholder="Nombre Comercial" class="form-control" value = "<?php  if(is_object($model)){echo $model->Nombre_Comercial;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Codigo Cliente</label>

                                    <div class="col-lg-10"><input name="Codigo" id="Codigo" type="text" placeholder="CC" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Codigo;}  ?>"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">RFC</label>
                                    <div class="col-lg-10"><input name="RFC" type="text" placeholder="RFC" class="form-control" value = "<?php  if(is_object($model)){echo $model->RFC;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Telefono</label>
                                    <div class="col-lg-10"><input name="Telefono" type="tel" placeholder="Telefono" class="form-control" required="true" value = "<?php  if(is_object($model))echo $model->Telefono;?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Correo</label>
                                    <div class="col-lg-10"><input name="Correo" type="email" placeholder="Correo" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Correo;}  ?>" ></div>                                    
                                </div>

                                <div class="form-group"><label class="col-lg-2 control-label">Codigo Postal</label>
                                    <div class="col-lg-10"><input onfocus="codigoPostal()" onkeyup = "codigoPostal()" name="CodigoPostal" id ="CodigoPostal" type="number" min="1" max="99999" placeholder="Codigo Postal" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->CodigoPostal;}  ?>">
                                    </div>
                                </div>  
                                <div class="form-group"><label class="col-lg-2 control-label">Colonia</label>
                                    <div class="col-lg-10">
                                    <select name="Colonia" disabled = "true" id="Colonia" onchange ="cambioColonia()" onfocus="cambioColonia()" class="form-control" >

                                    </select>
                                    <input type="hidden" id = "municipioEstado" value="">
                                    </div>                                 
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Calle y Numero</label>
                                    <div class="col-lg-10"><input disabled name="Calle" id="Calle"type="text" onkeyup ="cambioCalle()" placeholder="Calle y Numero" class="form-control" required="true" value="">
                                    </div>
                                </div> 
                                <div class="form-group"><label class="col-lg-2 control-label">Direccion</label>
                                    <div class="col-lg-10"><input name="Direccion" id="Direccion"type="text" placeholder="Direccion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Direccion;}  ?>">
                                    </div>
                                </div>  
                                <div class="form-group"><label class="col-lg-2 control-label">Contacto 1</label>
                                     <div class="col-lg-10">
                                      <button id="modalB1" type="button" onclick="showModal(1)" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-user-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span>
                                      </button>
                                      <input type="text" style="display: none;" required="true" id="idPersona1" name="idPersona1" value="<?php if(is_object($model))echo $model->idPersona1; ?>">
                                      <span id="succses1" style="visibility: hidden;"  class="btn btn-info btn-circle"><i class="fa fa-check"></i></span>&nbsp;&nbsp;
                                        <div id="cont1" style="display:none" >
                                            <br>
                                            <label id='nomContacto1' for=""></label><br>
                                            <label id='telContacto1' for=""></label><br>
                                            <label id='emailContacto1' for=""></label><br>
                                        </div>
                                    </div>
                                    <!-- fin boton modal -->
                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Contacto 2</label>
                                 <div class="col-lg-10">
                                  <button id="modalB2" type="button" onclick="showModal(2)" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-user-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span>
                                  </button>
                                  <input type="text" style="display: none;" id="idPersona2" name="idPersona2" value="<?php if(is_object($model))echo $model->idPersona2; ?>">
                                  <span id="succses2" style="visibility: hidden;"  class="btn btn-info btn-circle"><i class="fa fa-check"></i></span>&nbsp;&nbsp;
                                    <div id="cont2" style="display:none" >
                                            <br>
                                            <label id='nomContacto2' for=""></label><br>
                                            <label id='telContacto2' for=""></label><br>
                                            <label id='emailContacto2' for=""></label><br>
                                            
                                    </div>
                                </div>

                                <!-- fin boton modal -->
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Cuenta asiganda a</label>
                                    <div class="col-lg-10">
                                    <select name="Personal" id="Personal" class="form-control" >
                                    <?php if(is_object($model)): ?>
                                        <option selected value="<?php  if(is_object($model)){echo $model->Personal;}  ?>"><?php  if(is_object($model)){echo $model->nomPersonal;}  ?></option>
                                    <?php endif; ?>
                                        <?php foreach($model2 as $m):?>
                                            <option value="<?php echo $m->idPersona; ?>"><?php echo $m->Nombre.' '.$m->Apellidos ?></option>
                                        <?php endforeach;?>
                                       
                                    </select>
                                    </div>                                 
                                </div>                             
                                <div class="form-group"><label class="col-lg-2 control-label">Margen %</label>

                                    <div class="col-lg-10"><input name="Margen" type="text" placeholder="Margen" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Margen;}  ?>">
                                    </div>
                                </div>   
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                       <a href="<?php echo site_url('Cliente/index/'); ?>" > <button type="button" class="btn btn-w-m btn-warning">Atras</button></a>
                                        <button  class="btn btn-w-m btn-primary" onclick="validate()" type="submit">Registrar Cliente</button>
                                        <?php if (is_object($model)):?>
                                             <a onclick="return confirm('Esta seguro de querer Eliminar este elemento')" href="<?php  if(is_object($model))echo site_url('Cliente/eliminar/'.$model->idCliente) ?>"><button type="button" class="btn btn-danger">Dar de Baja</button></a>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- <?php form_close(); ?> -->
                            </form>


                        </div>
                    </div>
                </div>


                                
                                <!-- modal  -->
                                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-user-o modal-icon"></i>
                                            <h4 class="modal-title">Alta de Contacto</h4>
                                            <small class="font-bold">Agregue la informacion requerida para dar de alta el contacto de cliente espedificado.</small>
                                        </div>
                                        <div class="form-horizontal" style="padding: 10px;" >
                                            <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>

                                                      <div class="col-lg-10"><input id="NombreC" name="Nombre" type="text" placeholder="Nombre" class="form-control" value = "" required>
                                                      </div>
                                                  </div>
                                                  <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>

                                                      <div class="col-lg-10"><input id="ApellidosC" name="Apellidos" type="text" placeholder="Apellidos" class="form-control" value = "" required></div>
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Telefono</label>
                                                      <div class="col-lg-10"><input id="TelefonoC" name="Telefono" type="number" placeholder="Telefono" class="form-control" value = "" required></div>                                    
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Email</label>
                                                      <div class="col-lg-10"><input id="EmailC" name="Email" type="email" placeholder="Email" class="form-control" value = "" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required ></div>                                    
                                                  </div>               
                                                  <!-- <div class="form-group"><label class="col-lg-2 control-label">Foto</label>
                                                      <div class="col-lg-10"><input name="img" type="file" class="form-control" accept="image/png, image/jpeg, image/jpg" value = ""  required></div>                                    
                                                  </div> -->
                                                    <input type="hidden" id="idPersona" name="idPersona" value="0">
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal" onclick="limpiarForm()" >Cerrar</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardar($('#idPersona').val())" >Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <!-- fin modal -->

                <script type="text/javascript">
                  sw = `http://localhost:8080/sansarita/Backend/public/`
                  
                  function guardar($num = 0) {
                    
                    // consumo de alta de persona
                    parametros = {
                        "Nombre":document.getElementById('NombreC').value,
                        "Apellidos":document.getElementById('ApellidosC').value,
                        "Telefono":document.getElementById('TelefonoC').value,
                        "Email":document.getElementById('EmailC').value,
                        "idtipoPersona":"5",
                        "idArea":"5"
                    }

                    let idpersona = document.getElementById(`idPersona${$num}`).value
                    console.log(parametros)
                    document.getElementById("succses"+$num).style.visibility="visible";
                    if (idpersona != '') {
                        url = `${sw}persona/actualizar/${idpersona}`
                        $.ajax({
                            data:  parametros,
                            url:   url,
                            type:  'PUT',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                if (msg['response']) {
                                    
                                }else{
                                    if(msg['errors'].length == 0){
                                        error = msg['message']
                                    }else{
                                        // alert(msg['errors'])
                                        error = msg['errors']
                                    }
                                }
                                limpiarForm()
                                return true
                            }
                        });

                    }else{

                        url = `${sw}persona/registrar`
                        console.log(url)
                        $.ajax({
                            data:  parametros,
                            url:   url,
                            type:  'POST',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                if (msg['response']) {
                                    document.getElementById("modalB"+$num).disabled = true
                                    document.getElementById('idPersona'+$num).value = msg['result']
                                    console.log(document.getElementById('idPersona'+$num).value)
                                }else{
                                    if(msg['errors'].length == 0){
                                        error = msg['message']
                                    }else{
                                        // alert(msg['errors'])
                                        error = msg['errors']
                                    }
                                }
                                limpiarForm()
                                return true
                            }
                        });
                    }
                    
                    // fin
                  }

                  function showModal($num) {
                    document.getElementById('idPersona').value = $num
                    let idpersona = document.getElementById(`idPersona${$num}`).value
                    if (idpersona != '') {
                        mostrarContacto(idpersona);
                    }else{
                        console.log(null)
                    }
                  }

                  function validate() {
                    return false
                    alert('Cancelado')
                  }
                  function limpiarForm() {
                        document.getElementById('NombreC').value = ""
                        document.getElementById('ApellidosC').value = ""
                        document.getElementById('TelefonoC').value = ""
                        document.getElementById('EmailC').value = ""
                  }

                 function mostrarContacto(id){
                    // let id = document.getElementById('idPersona').value
                    let url = `${sw}persona/obtener/${id}`
                    console.log(url)
                    $.ajax({
                            // data:  parametros,
                            url:   url,
                            type:  'GET',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                console.log(msg)
                                document.getElementById('NombreC').value = msg['Nombre']
                                document.getElementById('ApellidosC').value = msg['Apellidos']
                                document.getElementById('TelefonoC').value = msg['Telefono']
                                document.getElementById('EmailC').value = msg['Email']
                                return true
                            }
                    });
                  }

                $( document ).ready(function() {
                    let id1 = document.getElementById('idPersona1').value
                    let id2 = document.getElementById('idPersona2').value

                    let url1 = `${sw}persona/obtener/${id1}`
                    let url2 = `${sw}persona/obtener/${id2}`
                    if (id1 != '' ) {
                         $.ajax({
                            // data:  parametros,
                            url:   url1,
                            type:  'GET',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                // console.log(msg)
                                document.getElementById('nomContacto1').innerHTML = `Nombre: ${msg['Nombre']} ${msg['Apellidos']}`
                                document.getElementById('telContacto1').innerHTML = `Telefono: ${msg['Telefono']}`
                                document.getElementById('emailContacto1').innerHTML = `Email: ${msg['Email']}`
                                document.getElementById('cont1').style.display = 'inline'
                                return true
                            }
                        });
                    }
                    if (id2 != '') {
                        $.ajax({
                            // data:  parametros,
                            url:   url2,
                            type:  'GET',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                // console.log(msg)
                                document.getElementById('nomContacto2').innerHTML = `Nombre: ${msg['Nombre']} ${msg['Apellidos']}`
                                document.getElementById('telContacto2').innerHTML = `Telefono: ${msg['Telefono']}`
                                document.getElementById('emailContacto2').innerHTML = `Email: ${msg['Email']}`
                                document.getElementById('cont2').style.display = 'inline'
                                return true
                            }
                        });
                    }
                });
                function cc() {
                    let idC = document.getElementById('Nombre_Comercial').value
                    let split = idC.split(' ')
                    split.length
                    let part
                    if (split.length >= 2) {
                        part = split[0].substr(0,1)
                        part += split[1].substr(0,1)
                    }else{
                        part = split[0].substr(0,2)
                    }
                    document.getElementById('Codigo').value = part
                }

                let codigoPostal = () =>{
                    let cp = document.getElementById('CodigoPostal').value
                    let sw = `https://api-codigos-postales.herokuapp.com/v2/codigo_postal/${cp}`
                    if(cp.length == 5 ){
                        // console.log(cp);
                        $.ajax({
                            // data:  parametros,
                            url:   sw,
                            type:  'GET',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function () {
                                    return true
                            },
                            success: function(msg)
                            {
                                console.log(msg);
                                let cad =  `${msg['municipio']}, ${msg['estado']}`
                                document.getElementById('Direccion').value = cad
                                document.getElementById('municipioEstado').value = cad
                                arrayColonia = msg['colonias']
                                let colonia = document.getElementById('Colonia') 
                                colonia.innerHTML = ``
                                document.getElementById('Colonia').disabled = false
                                document.getElementById('Calle').disabled = false
                                arrayColonia.forEach(element => {
                                    let option = document.createElement(`option`)
                                    option.innerHTML = `${element}`
                                    colonia.appendChild(option)
                                });
                                // document.getElementById('Colonia').focus()
                                return true
                            }
                        });
                    }else{
                        let cp = document.getElementById('CodigoPostal').value
                        document.getElementById('CodigoPostal').value = cp.substr(0,5)
                        document.getElementById('Colonia').disabled = true
                    }
                    return true
                }
                let cambioColonia = () => {
                    let col = document.getElementById('Colonia').value
                    let dir = document.getElementById('municipioEstado').value
                    let calle = document.getElementById('Calle').value

                    document.getElementById('Direccion').value = `${calle}, Colonia ${col}, ${dir}`

                    return true
                }
                let cambioCalle = () =>{
                    let col = document.getElementById('Colonia').value
                    let dir = document.getElementById('municipioEstado').value
                    let calle = document.getElementById('Calle').value

                    document.getElementById('Direccion').value = `${calle}, Colonia ${col}, ${dir}`

                    return true
                }
                </script>