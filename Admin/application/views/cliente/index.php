<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Clientes</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Clientes</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <a href="<?php echo site_url('Cliente/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                  <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>CC</th>
                       <th>Nombre_Comercial</th>
                       <th>Contacto</th>
                       <!-- <th>Contacto 2</th> -->
                       <th>RFC</th>
                       <th>Email</th>
                       <th>Telefono</th>
                       <th>Cuenta asiganda a</th>                       
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idCliente.$m->Codigo; ?></td>
                        <td><?php echo $m->Nombre_Comercial; ?></td>
                        <td><?php echo $m->NombreCompleto; ?></td>
                        <!-- <td><?php echo $m->idPersona2; ?></td> -->
                        <td><?php echo $m->RFC; ?></td>
                        <td><?php echo $m->Correo;?></td>
                        <td><?php echo $m->Telefono; ?></td>
                        <td><?php echo $m->nomPersonal;?></td>
                        <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('Cliente/crud/'.$m->idCliente) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <!-- <a onclick="return confirm('Esta seguro de querer Eliminar este elemento')" href="<?php echo site_url('Cliente/eliminar/'.$m->idCliente) ?>"><button type="button" class="btn btn-danger">Eliminar</button></a> -->
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>                  
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
