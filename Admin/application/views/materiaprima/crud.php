<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Materia Prima';
    if(is_object($model)){
        $titulo = 'Actualizar Materia Prima';
    } 
    // var_dump($model);
?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('MateriaPrima/index/'); ?>" >Productos</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Materia Prima de Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">                         
                            <form class="form-horizontal" action="<?php echo site_url('MateriaPrima/registrar'); ?>" method="post">
                                <p>Llena todos los campos para agregar un producto.</p>
                                <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idMat_Prima;}  ?>">
                                <div class="form-group"><label class="col-lg-2 control-label">Matria Prima</label>

                                    <div class="col-lg-10"><input name="Mat_Prima" type="text" placeholder="Materia Prima" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Mat_Prima;}  ?>" >
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Descripcion</label>

                                    <div class="col-lg-10"><input name="Descripcion" type="text" placeholder="Descripcion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Descripcion;}  ?>"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Presentacion</label>
                                    <div class="col-lg-10"><input name="Precentacion" type="text" placeholder="Granel/Caja/Etc." class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Precentacion;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Cantidad</label>
                                    <div class="col-lg-10"><input name="Cantidad" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Cantidad;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Unidad</label>
                                    <div class="col-lg-10"><input name="Unidad" type="text" placeholder="Lt/Kg/Gr" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Unidad;}  ?>" ></div>                                    
                                </div>
                                <!-- <div class="form-group"><label class="col-lg-2 control-label">Codigo de barras</label> -->
                                    <!-- <div class="col-lg-10"> -->
                                    <input name="CodigoBarras" type="hidden" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->CodigoBarras;}else{echo "00";}  ?>" >
                                    <!-- </div>                                     -->
                                <!-- </div> -->
                                <div class="form-group"><label class="col-lg-2 control-label">Precio</label>
                                    <div class="col-lg-10"><input step="0.00001" name="Precio" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Precio;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Materia Prima</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>