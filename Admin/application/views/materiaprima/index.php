<!-- <?php print_r($model) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Materia Prima</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Materia Prima</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <a href="<?php echo site_url('MateriaPrima/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <!-- <th>Id Materia</th> -->
                       <th>Materia</th>
                       <th>Descripcion</th>
                       <th>Precentacion</th>
                       <!-- <th>Cantidad</th> -->
                       <th>Unidad / 1</th>
                       <!-- <th>Codigo de Barras</th> -->
                       <th>Precio</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <!-- <td><?php echo $m->idMat_Prima; ?></td> -->
                        <td><?php echo $m->Mat_Prima; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <td><?php echo $m->Precentacion; ?> </td>
                        <!-- <td><?php echo $m->Cantidad; ?></td> -->
                        <td><?php echo $m->Unidad; ?></td>
                        <!-- <td><?php echo $m->CodigoBarras; ?></td> -->
                        <td>$<?php echo $m->Precio; ?></td>
                        <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('MateriaPrima/crud/'.$m->idMat_Prima) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('MateriaPrima/eliminar/'.$m->idMat_Prima) ?>"  ><button  type="button" class="btn btn-danger"><i class="fa fa-times"></i> Eliminar</button></a>
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar esta Materia Prima?');
         }
       </script>
