<!-- <?php print_r($model) ?> -->
<!-- <?php var_dump($construccion) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Elementos de la presentacion </h2>
                   <div class="col-lg-10">                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Presentacion/index/'); ?>" >Presentacion</a>
                       </li>
                       <li class="active">
                           <strong>Presentaciones</strong>
                       </li>
                </ol>
               </div>
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-7">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Materia Industrial para las Presentaciones</h5>
                       <div class="ibox-tools">
                           
                       </div>
                   </div>
                <div class="ibox-content">

                  <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                     <thead>
                     <tr>
                         <!-- <th>Id Materia</th> -->
                         <th>Materia</th>
                         <th>Descripcion</th>
                         <th>Precentacion</th>
                         <!-- <th>Cantidad</th> -->
                         <th>Unidad / 1</th>
                         <!-- <th>Codigo de Barras</th> -->
                         <th>Action</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php foreach ($model as $m):?>
                       <tr class="gradeX">
                          <td><?php echo $m->Mat_ind; ?></td>
                          <td><?php echo $m->Descripcion; ?></td>
                          <td><?php echo $m->Precentacion; ?> </td>
                          <td><?php echo $m->Unidad; ?></td>
                          <td style="padding-right:0px"> 
                              <button id="badd<?php echo $m->idMat_ind; ?>" onclick="agregar(<?php echo $m->idMat_ind; ?>,'<?php echo $m->Mat_ind; ?>','<?php echo $m->Descripcion; ?>','<?php echo $m->Unidad; ?>')" class="btn btn-info " type="button">Agregar <i class="fa fa-chevron-right"></i> </button>
                             
                          </td>
                      </tr>
                     <?php endforeach; ?>
                     </tbody>
                     </table>
                  </div>

                </div>
               </div>
           </div>
           <div class="col-lg-5">
                 <div class="ibox float-e-margins">
                   <div class="ibox-title">
                   <form id="formR" class="form-horizontal" action="<?php echo site_url('presentacion/registrar'); ?>" method="post"  onsubmit="return empty()" >
                   <h5>Informacion de la Presentacion</h5>
                   <div style="margin-left: 15px">
                      <div class="form-group"> 
                        <input id="presentacion" value="<?php if(is_object($presentacion))echo $presentacion->Presentacion; ?>" required name ="presentacion" type="text" class="form-control" placeholder="Presentacion" >
                      </div>
                      <div class="form-group">
                        <input id="descripcion" value="<?php if(is_object($presentacion))echo $presentacion->Descripcion; ?>" required name ="descripcion" type="text" class="form-control" placeholder="Descripcion" >
                      </div>
                     </div>
                      <h5>Materia Industrial en la conposicion de la presentacion</h5>
                    <div class="ibox-content">
                            <div id="construccion" name="construccion">
                              <input type="hidden" id="foo" name="foo" value="0"  required="true" >
                              <input type="hidden" name="id" value="<?php echo $id; ?>">
                            </div>
                            <button type="submit" id = 'm2' class="btn btn-w-m btn-primary" >Registrar Presentacion</button>
                        </form>
                      <!-- </div> -->
                    </div>
                   </div>
                  </div>          
           </div>
        </div>
  </div>
        <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript">
          $( document ).ready(function() {
              var data = <?php echo json_encode($construccion);?>;
             data.forEach(function(element) {
                agregar(element['idMat_ind'],element['mat_ind'],element['Descripcion'],element['Unidad'],element['Cantidad'])
              });construccion

          })
        </script>
       <script type="text/javascript">
         
         function confirmacio() {
           confirm('Esta seguro de eliminar esta Materia Prima?');
         }
         
         function agregar($id,$mat,$des,$uni,$value){
             // console.log($id,$mat,$des)
             var $modal = $('#construccion');
                 $modal.append($('<div class="form-group" id="'+$id+'" ><label class="col-lg-4 control-label">'+$mat+' - '+$des+'</label>'+
                              '<div class="col-lg-3"><input name="cantidad['+$id+']" type="number" placeholder="00" class="form-control" required="true" min="0" value="'+$value+'" step="0.0001" >'+
                              '</div><label class="col-lg-2 control-label">'+$uni+'</label><button onclick="quitar('+$id+')" class="btn btn-danger btn-circle"> <i class="fa fa-times" ></i>  </button> '+
                            '</div>'));
             // var $boton = $('#badd'+$id);
             //    $boton.disabled = true;
             //    console.log($boton);
             $su = document.getElementById('foo').value;
             $su = $su + 1;
             $su = document.getElementById('foo').value = $su;
             document.getElementById('badd'+$id).disabled = true;
             var $modal = $("#construccion")
             $modal.find('#alerta').remove()
         }
         function quitar($id) {
           //confirm('Esta seguro de elmiinar este item')
           if(confirm('Esta seguro de elmiinar este item'))
            {
              var $modal = $("#construccion");
              $modal.find('#'+$id).remove();
              document.getElementById('badd'+$id).disabled = false;
              $su = document.getElementById('foo').value;
             $su = $su - 1;
             $su = document.getElementById('foo').value = $su;
            }
         }
         function empty() {
           $foo = document.getElementById('foo').value;
           // console.log($foo)
           if($foo > 0){
              return true
           }else{
             var $modal = $("#construccion")
             $modal.find('#alerta').remove()
             $modal.append($('<div id = "alerta" class="alert alert-danger">'+
                              'Agrega una materia industrial.'+
                             '</div>'))
             // console.log('')
            return false
           }
         }
       </script>
