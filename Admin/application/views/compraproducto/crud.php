<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Compra Producto';
    $crud = 0;
    if(is_object($model)){
        $titulo = 'Actualizar Compra Producto';
        $crud = 1;
    }
?>
<style>
input[type="number"]::-webkit-outer-spin-button, 
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}
</style>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('CompraProducto/index/'); ?>" >Compra Producto</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Compra Sansarita</h5>
                        </div>
                        <div class="ibox-content">                        
                            <form class="form-horizontal" action="<?php echo site_url('CompraProducto/registrar'); ?>" method="post">
                            <div class="form-group"><label class="col-lg-2 control-label">Descripcion</label>
                                <div class="col-lg-10"><input name="Descripcion" type="text" placeholder="Descripcion" class="form-control" required="true" >
                            </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Almacen al que se agregara la Compra</label>
                                <div class="col-lg-10">
                                <select class="form-control" name="Almacenes" id="Almacenes">
                                    <option value="">Seleccione...</option>
                                    <?php foreach($almacenes as $a): ?>
                                        <option value="<?php echo $a->idAlmacen?>" ><?php echo $a->Nombre?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            </div>
                            <div class="ibox-tools">
                                <a class="btn btn-w-m btn-success" onclick="agregar()">+</a>
                                <!-- data-toggle="modal" data-target="#myModal2" usar el modal -->
                            </div>        
                    <div class="ibox-content">

                    <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>Tipo Producto</th>
                       <th>Producto</th>
                       <th>Cantidad</th>
                       <th>Precentacion</th>
                       <th>Precio Total</th>
                       <th>Action</th> 
                   </tr>
                   </thead>
                   <tbody id="cuerpo">
                     <!-- <tr class="gradeX">
                        <td></td>
                        <td></td>
                        <td></td> 
                        <td></td> 
                        <td></td> 
                        <td style="padding-right:0px"> 
                            <a onclick ="return confirm('Esta seguro de eliminar este Producto?')"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr> -->
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
                                <!-- <p>Llena todos los campos para agregar una Compra</p> -->
                                <!-- <input type="hidden" name ="idCompra_Producto" value ="<?php  if(is_object($model)){echo $model->idCompra_Producto;}  ?>">
                                <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>

                                    <div class="col-lg-10"><input name="Nombre" type="text" placeholder="Nombre" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Nombre;}  ?>" >
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Tipo_producto </label>

                                    <div class="col-lg-10"><input name="Tipo_producto" type="text" placeholder="Tipo_producto" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Tipo_producto;}  ?>"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Cantidad</label>

                                    <div class="col-lg-10"><input name="Cantidad" type="number" placeholder="Cantidad" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Cantidad;}  ?>"></div>
                                </div>
                               
                                 <div class="form-group"><label class="col-lg-2 control-label">Precio_total</label>

                                 <div class="col-lg-10"><input name="Precio_total" type="number" placeholder="Precio_total" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Precio_total;}  ?>"></div>
                                </div>
                                 <div class="form-group"><label class="col-lg-2 control-label">Presentacion</label>
                                    
                                 <div class="col-lg-10"><input name="Presentacion" type="text" placeholder="Presentacion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Presentacion;}  ?>"></div>
                                </div>

                                <div class="form-group"><label class="col-lg-2 control-label">Proveedor</label>
                                    <div class="col-lg-10">
                                        <select name="idProveedor" id="idProveedor" class="form-control">
                                            <?php foreach ($pro as $proveedor ):?>
                                            <option value="<?php echo $mat->idProveedor ?>" > <?php echo $mat->idProveedor ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                              
                               <div class="form-group"><label class="col-lg-2 control-label">Materia Prima</label>
                                    <div class="col-lg-10">
                                        <select name="idMat_Prima" id="idMat_Prima" class="form-control">
                                            <?php foreach ($materiaprima as $mat ):?>
                                            <option value="<?php echo $mat->idMat_Prima ?>" > <?php echo $mat->Mat_Prima ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                 <div class="form-group"><label class="col-lg-2 control-label">Materia Industrial</label>
                                    <div class="col-lg-10">
                                        <select name="idMat_ind" id="idMat_ind" class="form-control">
                                            <?php foreach ($matindus as $mat ):?>
                                            <option value="<?php echo $mat->idMat_ind ?>" > <?php echo $mat->Mat_ind ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                -->
                                
                                <div class="ibox-tools">
                                    <label for="">Total:</label>
                                    <label id="total" name="total" for="">$0</label>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Compra</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                            <!-- <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated flipInY">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Selecciones el tipo de Producto a Comprar</h4>
                                            <small class="font-bold"></small>
                                        </div>
                                        <div class="modal-body">
                                            <label for="tipo_prod"></label>
                                            <select name="tipo_prod" id="tipo_prod" class="form-control">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <button type="button" onclick="agregar()" class="btn btn-primary" data-dismiss="modal"> Acepto</button>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                <script>
                    // let cadena = '';
                    let i = 0;
                    let agregar = () => {
                        i++;
                        console.log('agregar');
                        let tr = document.createElement('tr');
                        tr.id = `elemento${i}`;
                        tr.className = `gradeX`;
                        tr.innerHTML =  `<tr >
                            <td><input type="text" class="form-control" placeholder="tipo producto" required="true" min="0"  step=".01" style="width:140px"></td>
                            <td><h4><select name="linea" id="linea" class="col-lg-19" onchange = "seleccioneMateriaP()"  required>
                            <option value="0">Seleccione...</option>
                              <?php foreach ($linea as $cli): ?>
                                <option value="<?php echo $cli->idLinea;?>"><?php echo $cli->Descripcion?></option>
                              <?php endforeach;?>
                              </select><h4>
                            </td>
                            <td><div id="tbpedidos" class="form-group">
                            <div class="col-lg-2">
                              <input disabled id="cantidad<?php //echo $m->idLinea ?>" name="cantidad[]" onkeyup="calPrecio(<?php //echo $m->PrecioPublico ;?>,<?php //echo $m->idProducto ?>,<?php //echo $m->Precentacion ;?>)"  type="number" placeholder="00" class="form-control" required="true" min="0"  step="1"  style="width:70px">
                            </div>
                            </div></td> 
                            <td> <input disabled id="cantidad<?php //echo $m->idLinea ?>" name="cantidad[]" onkeyup="calPrecio(<?php //echo $m->PrecioPublico ;?>,<?php //echo $m->idProducto ?>,<?php //echo $m->Precentacion ;?>)"  type="number" placeholder="00" class="form-control" required="true" min="0"  step="1"  style="width:70px"></td> 
                            <td> <input disabled id="cantidad<?php //echo $m->idLinea ?>" name="cantidad[]" onkeyup="calPrecio(<?php //echo $m->PrecioPublico ;?>,<?php //echo $m->idProducto ?>,<?php //echo $m->Precentacion ;?>)"  type="number" placeholder="00" class="form-control" required="true" min="0"  step="1"  style="width:70px"></td> 
                            <td style="padding-right:0px"> 
                                <a onclick ="eliminar(${i})"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>
                            </td>
                            </tr>`;
                    
                        // console.log(cadena);
                        document.getElementById('cuerpo').appendChild(tr);

                    }


                    let eliminar = (num) => {
                        let confirmar = confirm('Esta seguro de eliminar este Producto?');
                        if(confirmar){
                            document.getElementById(`elemento${num}`).remove();
                        }
                    }


                </script>