<!-- <?php print_r($model) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Compra Productos</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Productos</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <a href="<?php echo site_url('CompraProducto/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>IdCompra</th>
                       <th>Nombre</th>
                       <th>Tipo_Producto</th>
                       <th>Cantidad</th>
                       <th>Precio_total</th>
                       <th>Precentacion</th>
                       <th>Action</th> 
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idCompra_Producto; ?></td>
                        <td><?php //echo $m->Nombre; ?></td>
                        <td><?php echo $m->Tipo_producto; ?></td>
                        <td><?php echo $m->Cantidad; ?> </td> 
                        <td><?php echo $m->Precio_total; ?></td> 
                        <td><?php echo $m->Presentacion; ?></td> 
                        <td style="padding-right:0px"> 
                            <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('CompraProducto/eliminar/'.$m->idCompra_Producto) ?>"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
