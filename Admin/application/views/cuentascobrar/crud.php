<?php 
   // print_r($model);
//var_dump($cliente);
?>
<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Dar de Alta Cobro';
    if(is_object($model)){
        $titulo = 'Actualizar Cobro';
    } 
    // var_dump($model);
?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('CuentasCobrar/index/'); ?>" >Cuentas por Cuentas</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>

          <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-8">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Generar Cobro</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Productos/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <!-- <th>IdProducto</th> -->
                       <th>Cliente</th>
                       <th>Total_Pedido</th> 
                       <!-- <th>Codigo de barras</th> -->
                       <th>Fecha_pedido</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <!-- <td><?php echo $m->idProducto; ?></td> -->
                        <td><?php echo $m->Cliente; ?></td>
                        <td>$<?php echo $m->Total; ?></td> 
                        <!-- <td><?php echo $m->CodigoBarras; ?> </td> -->
                        <td><?php echo $m->Fecha_Pedido; ?></td>
                        <td style="padding-right:0px"> 
                            <button id="badd<?php echo $m->idPedidos; ?>" class="btn btn-info " onclick="Agregar(<?php echo $m->idPedidos; ?>,'<?php echo $m->Cliente; ?>','<?php echo $m->Total; ?>')" type="button">Agregar <i class="fa fa-chevron-right"></i> </button>
                            <!-- <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('Productos/eliminar/'.$m->idProducto) ?>"  ><button  type="button" class="btn btn-w-m btn-danger">Eliminar</button></a> -->
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>

           <div class="col-lg-4">
                 <div class="ibox float-e-margins">
                   <div class="ibox-title">
                    <h5>Cantidad de Producto que desea elaborar</h5><br>
                    <!-- <label class="control-label">Escoje almacen de materia prima </label> -->
                    <div class="ibox-content">
                    
                      <!-- <div class="table-responsive"> -->
                        <!-- <div class="col-md-3">u</div>
                        <div class="col-md-3">u</div>
                        <div class="col-md-3">u</div>
                        <div class="col-md-3">u</div> -->
                        <!-- <form class="form-horizontal" action="<?php echo site_url('Recetas/registrar'); ?>" method="post" > -->
                        <table class = "table table-striped">
                            <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Costo Pedido</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                      
                            
                        </table>
                        
                        <form id="formR" class="form-horizontal" action="<?php echo site_url('Ventas/crud'); ?>" method="post"  onsubmit="return empty()" >
                            <div id="pedidos" name="pedidos">
                              <input type="hidden" id="foo" name="foo" value="0"  required="true" >
                              <input type="hidden" name="id" value="<?php //echo $id; ?>">
                            </div>
                            

                                    
                                                      
 
                            <!--<h5><i class="fa fa-address-book-o" aria-hidden="true"></i> Elige el Cliente:</h5>
                            <select name="cliente" id="cliente" class="form-control" >
                            <?php 
                              foreach ($cliente as $cli){
                                if($cli->Activo == "1"){
                                  echo "<option value='".$cli->idCliente."' > ".$cli->Cliente." </option>";
                                }
                              }
                            ?>
                            </select><br>-->
                        <!--<div class="content">
                         <h5><i class="fa fa-address-book-o" aria-hidden="true"></i> id_Usuario: </h5>-->
                            <!--<?php var_dump($user->id) ?>
                            <?php $us = $user->id; ?>
                            <?php
                                //  if($us==0){
                                // }
                                //     for ($i=0;$i<count($user->id);$i++)
                                // {
                                    echo "  " .  "'$user->id'";
                                // } 
                            ?>
                      </div>-->

                            <div style="padding-left:0px">
                            <input id="badd4" class="btn btn-info" type ="submit" value="Alta de Ventas " >
                                        
                                       
                                    </div>
                        <!--    <h5><i class="fa fa-address-book-o" aria-hidden="true"></i> Usuario:</h5>
                            <select name="personal" id="personal" class="form-control" >
                            <?php 
                              foreach ($personal as $per){
                                if($per->Activo == "1"){
                                  echo "<option value='".$per->persona."' > ".$per->Nombre." </option>";
                                }
                              }
                            ?>
                            </select><br>-->
                      <!-- </div> -->
                      <div id="alerta">  </div>
                      
                      
                    </div>
                   </div>
                  </div>  
                     
                  </form>     
           </div>
           <label></label>
        </div>
      </div>
      
      <!-- <button type="button" class="btn btn-primary"  data-toggle="popover" data-placement="auto left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                Popover on left
                            </button> -->
     <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
     <script type="text/javascript">
      $( document ).ready(function() {
              var data = <?php echo json_encode($pedidos);?>;
             data.forEach(function(element) {
                agregar(element['idPedidos'],element['Fecha_Pedido'],element['Total'])
              });

          })
        </script>
       <script type="text/javascript">
       function confirmacio() {
         confirm('Esta seguro de eliminar este Producto?');
       }
       function Agregar($id,$nombre,$total) {
         // console.log($id,$mat,$des)
             var $modal = $('#pedidos');
                 $modal.append($('<div class="form-group" id="'+$id+'" ><label class="col-lg-4 control-label">'+$nombre+'</label>'           +'<div class="form-group" id="'+$id+'" ><label class="col-lg-3 control-label">'+$total+'</label>'+
                              '<label class="col-lg-2 control-label"></label><button onclick="quitar('+$id+')" class="btn btn-danger btn-circle"> <i class="fa fa-times" ></i>  </button> </div>'+
                            '</div>'));
             // var $boton = $('#badd'+$id);
             //    $boton.disabled = true;
             //    console.log($boton);
             $su = document.getElementById('foo').value;
             $su = $su + 1;
             $su = document.getElementById('foo').value = $su;
             document.getElementById('badd'+$id).disabled = true;
             var $modal = $("#alerta")
             $modal.find('#alert').remove()
       }
       function quitar($id) {
           //confirm('Esta seguro de elmiinar este item')
           if(confirm('Esta seguro de elmiinar este item'))
            {
              var $modal = $("#pedidos");
              $modal.find('#'+$id).remove();
              document.getElementById('badd'+$id).disabled = false;
              $su = document.getElementById('foo').value;
             $su = $su - 1;
             $su = document.getElementById('foo').value = $su;
            }
         }
       function empty() {
           $foo = document.getElementById('foo').value;
           // console.log($foo)
           if($foo > 0){
              return true
           }else{
             var $modal = $("#alerta")
             $modal.find('#alert').remove()
             $modal.append($('<div id = "alert" class="alert alert-danger alert-dismissable">'+
                              'Agrega un Producto.'+
                             '</div>'))
             // console.log('')
            return false
           }
         }

     </script>