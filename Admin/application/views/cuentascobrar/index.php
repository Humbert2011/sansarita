<!--<?php print_r($model) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Cuentas Por Cobrar</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Cuentas a Cobrar</h5>
                       <div class="ibox-tools">
                         <a href="<?php echo site_url('CuentasCobrar/crud'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar Cuenta</span></button></a>
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!--<a href="<?php echo site_url('CuentasCobrar/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>-->
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>idCuentas por Cobrar</th>
                       <th>Cliente</th>
                       <th>Descripcion</th>
                       <th>Fecha_Deuda</th>
                       <th>Total_deuda</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                     <?php $preT = 0; ?>
                    <?php $deu = 0;?>
                    <?php $MontoT = 0;?>
                    <?php $deuT = 0;?>

                   <?php foreach ($model as $m):
                    $preT = $m->Total + $preT;
                    $MontoT = $m->Monto_Pago + $MontoT;
                    $deu =  $m->Total - $m->Monto_Pago;
                    $deuT = $deu + $deuT;?>

                     <tr class="gradeX">
                        <td><?php echo $m->idCuentas_Cobrar; ?></td>
                        <td><?php echo $m->Cliente; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <td><?php echo $m->Fecha; ?></td>
                        <td>$<?php echo $deu; ?> </td>
                        <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('CuentasCobrar/crud/'.$m->idCuentas_Cobrar) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('CuentasCobrar/eliminar/'.$m->idCuentas_Cobrar) ?>"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
