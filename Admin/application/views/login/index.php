<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sansarita | Login</title>
    <link rel="stylesheet"  href="<?php echo base_url('css/bootstrap.min.css'); ?>">
    <!-- <link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet"> -->
    <link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

    <link href="<?php echo base_url('css/animate.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">

</head>

<body class="gray-bg">
<?php echo form_open('login/autenticar'); ?>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div style="padding: 5px">

                <!-- <h1 class="logo-name">IN+</h1> -->
                <img width="120px" src="<?php echo base_url('img/sansarita2.png');?>">  

            </div>
            <h3>Bienvenido a Sansarita</h3>
            <p>Por favor ingrese los datos que el administrador le ha proporcionado para iniciar su sesión
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <!-- <p>Login in. To see it in action.</p> -->
            <form class="m-t" role="form" action="index.html">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Usuario" required="" name="Email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="Password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger">
                                '.$error.'
                            </div>';
                    }
                ?>
                <a href="#"><small>Olvidaste tu contraseña ?</small></a>
                <p class="text-muted text-center"><small>No tienes una cuenta?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Crea una</a>
            </form>
            <p class="m-t"> <small>Un Sistema de Stardust inc SA de CV &copy; 2017</small> </p>
        </div>
    </div>
<?php form_close(); ?>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url('js/jquery-3.1.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

</body>

</html>
