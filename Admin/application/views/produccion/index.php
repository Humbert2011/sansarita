<!-- <?php print_r($almacenes) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Produccion</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-6">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Productos</h5>
                       <div class="ibox-tools">
                           
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <!-- <th>IdProducto</th> -->
                       <th>Producto</th>
                       <!-- <th>Descripcion</th> -->
                       <!-- <th>Codigo de barras</th> -->
                       <th>Presentacion</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <!-- <td><?php echo $m->idProducto; ?></td> -->
                        <td><?php echo $m->Producto; ?></td>
                        <!-- <td><?php echo $m->Descripcion; ?></td> -->
                        <!-- <td><?php echo $m->CodigoBarras; ?> </td> -->
                        <td><?php echo $m->Precentacion; ?></td>
                        <td style="padding-right:0px"> 
                            <button id="badd<?php echo $m->idProducto; ?>" class="btn btn-info " onclick="Agregar(<?php echo $m->idProducto; ?>,'<?php echo $m->Producto; ?>','<?php echo $m->Precentacion; ?>')" type="button">Agregar <i class="fa fa-chevron-right"></i> </button>
                            <!-- <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('Productos/eliminar/'.$m->idProducto) ?>"  ><button  type="button" class="btn btn-w-m btn-danger">Eliminar</button></a> -->
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>

           <div class="col-lg-6">
                 <div class="ibox float-e-margins">
                   <div class="ibox-title">
                    <h5>Cantidad de Producto que desea elaborar</h5><br>
                    <!-- <label class="control-label">Escoje almacen de materia prima </label> -->
                    <div class="ibox-content">
                    
                      
                        <table class = "table table-striped">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>#Frascos</th>
                                    <th>#Cajas</th>
                                    <th>#Kg</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                      
                            
                        </table>
                        
                        <form id="formR" class="form-horizontal" action="<?php echo site_url('Produccion/registrar'); ?>" method="post"  onsubmit="return empty()" >
                            <div id="receta" name="receta">
                              <input type="hidden" id="foo" name="foo" value="0"  required="true" >
                              <input type="hidden" name="id" value="<?php //echo $id; ?>">
                            </div>
                            
                            <div style="padding-right:0px">
                            <input id="badd4" class="btn btn-info" type ="submit" value="Alta de Produccion " >
                                        
                                       
                                    </div>
                                    
                            
                            <h5><i class="fa fa-signal" aria-hidden="true"></i> Elige el almacen:</h5>
                            <select name="almacen" id="almacen" class="form-control" >
                            <?php 
                              foreach ($almacenes as $al){
                                if($al->Tipo == "Materia"){
                                  echo "<option value='".$al->idAlmacen."' > ".$al->Nombre." </option>";
                                }
                              }
                            ?>
                            </select><br>
                      <!-- </div> -->
                      <div id="alerta">  </div>
                      
                      
                    </div>
                   </div>
                  </div>  
                     
                  </form>     
           </div>
           <label></label>
        </div>
      </div>
      
      <!-- <button type="button" class="btn btn-primary"  data-toggle="popover" data-placement="auto left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                Popover on left
                            </button> -->
     <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
     <script type="text/javascript">
       function confirmacio() {
         confirm('Esta seguro de eliminar este Producto?');
       }
       function Agregar($id,$nombre,$pres) {
         // console.log($id,$mat,$des)
             var $modal = $('#receta');
                 $modal.append($('<div class="form-group" id="'+$id+'" ><label class="col-lg-3 control-label">'+$nombre+'</label>'+
                              '<div class="col-lg-2"><input onkeyup="key(`cantidad`,'+$id+','+$pres+')" name="cantidad['+$id+']" id="cantidad'+$id+'" type="number" placeholder="00" class="form-control" required="true" min="0"  step=".01" style="width:60px">'+
                              '</div>'+
                              '<div class="col-lg-2"><input onkeyup="key(`cajas`,'+$id+','+$pres+')" name="cajas['+$id+']" id="cajas'+$id+'" type="number" placeholder="00" class="form-control" required="true" min="0"  step=".01" style="width:60px"></div >'+
                              '<div class="col-lg-2"><input onkeyup="key(`kg`,'+$id+','+$pres+')" name="kg['+$id+']" id="kg'+$id+'" type="number" placeholder="00" class="form-control" required="true" min="0"  step=".01" style="width:60px"></div >'+
                              '<div class="col-lg-2" ><button onclick="quitar('+$id+')" class="btn btn-danger btn-circle"> <i class="fa fa-times" ></i>  </button> </div>'+
                            '</div>'));
             // var $boton = $('#badd'+$id);
             //    $boton.disabled = true;
             //    console.log($boton);
             $su = document.getElementById('foo').value;
             $su = $su + 1;
             $su = document.getElementById('foo').value = $su;
             document.getElementById('badd'+$id).disabled = true;
             var $modal = $("#alerta")
             $modal.find('#alert').remove()
       }
       function quitar($id) {
           //confirm('Esta seguro de elmiinar este item')
           if(confirm('Esta seguro de elmiinar este item'))
            {
              var $modal = $("#receta");
              $modal.find('#'+$id).remove();
              document.getElementById('badd'+$id).disabled = false;
              $su = document.getElementById('foo').value;
             $su = $su - 1;
             $su = document.getElementById('foo').value = $su;
            }
         }
       function empty() {
           $foo = document.getElementById('foo').value;
           // console.log($foo)
           if($foo > 0){
              return true
           }else{
             var $modal = $("#alerta")
             $modal.find('#alert').remove()
             $modal.append($('<div id = "alert" class="alert alert-danger alert-dismissable">'+
                              'Agrega un Producto.'+
                             '</div>'))
             // console.log('')
            return false
           }
         }

         function key($this,$id,$pres) {
            switch($this) {
              case "cantidad":
                value = document.getElementById('cantidad'+$id).value
                cajas = value/12;
                kg = (value * $pres)/1000;
                document.getElementById('cajas'+$id).value = Number(cajas.toFixed(2));//redondear a 2 decimales
                document.getElementById('kg'+$id).value = Number(kg.toFixed(2));
                break;
              case "cajas":
                value = document.getElementById('cajas'+$id).value
                frasco = value * 12;
                kg = (value* 12 * $pres)/1000;
                // console.log(kg);
                document.getElementById('cantidad'+$id).value = Number(frasco.toFixed(2));//redondear a 2 decimales
                document.getElementById('kg'+$id).value = Number(kg.toFixed(2));
                break;
              case "kg":
                value = document.getElementById('kg'+$id).value
                // console.log(value)
                frasco = (value *1000)/$pres;
                cajas = frasco/12;

                // console.log(frasco)
                // console.log(cajas)
                document.getElementById('cantidad'+$id).value = Number(frasco.toFixed(2));//redondear a 2 decimales
                document.getElementById('cajas'+$id).value = Number(cajas.toFixed(2));
                break;
            }
         }

     </script>
