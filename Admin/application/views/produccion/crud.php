<!-- <?php print_r($model) ?><br><br>
<?php var_dump($prod) ?><br><br>
<?php var_dump($cantidad)?><br>
<?php var_dump($detalle)?><br> -->
<!--<?php var_dump($almacen); ?>-->
<?php
  $count = count($model);
  switch ($count) {
    case 0:
      redirect(site_url('Produccion/index'));
      break;
  }
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Materia Prima Necesaria Para La Produccion</h2>
                   <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Produccion/index/'); ?>" >Generar Produccion</a>
                       </li>
                       <li class="active">
                           <strong>Materia Prima Necesaria</strong>
                       </li>
                   </ol>
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                <form name="Action" action="<?php echo site_url('Produccion/alta'); ?>" method="post"  onsubmit="return matSu()" >
                   <div class="ibox-title">
                       <h5>Lista de Materia Prima</h5>
                       <div align="right"><button  type="submit" id = 'm2' class="btn btn-w-m btn-primary" >Dar de Alta Produccion</button></div>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Recetas/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">
                       <!-- info de pedido -->
                      <input type="hidden" name="ids_prod" value='<?php echo json_encode($cantidad); ?>'>
                      <!--  -->
                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>Producto</th> 
                         <th>Materia</th>
                         <th>Descripcion</th>
                         <th># Existencia</th>
                         <th># Necesaria</th>
                         <th>Unidad</th>
                         <th>Precio</th>
                         <th># de Productos</th>
                         <!-- <th>Action</th> -->
                   </tr>
                   </thead>
                   <tbody>
                    <?php
                      // para contar los valores superados 
                      $con = 0;
                      // var de precio total
                      $preT = 0;
                      //resta de materia prima por produccion 
                    ?>
                   <?php $val = count($model);
                   ?>
                   <?php for($i = 0;$i < $val;$i++){?>
                      <?php $num = 0 ?>
                     <?php foreach ($model[$i] as $m):?>
                      <?php
                          if ($num == 0) {
                            // echo  '<tr class="gradeX"><td>'.$m->Producto.'</td></tr>';
                          }
                          foreach ($cantidad as $key => $value) {
                            
                            if ($key == $m->idProducto) {
                              $can = $value * $m->Cantidad;
                              $pre = $can * $m->Precio;
                              $numP = $value;
                              $preT = $preT + $pre;
                              // echo $can;
                              // echo ' - '.$m->Cantidad;
                            }
                          }
                          $num++;
                          $ex = 0 ;
                          foreach($detalle as $detail){
                            if($detail->idMat_Prima == $m->idMat_Prima){
                                $ex = $detail->Cantidad;
                            }
                          }

                       ?>
                       <!--  <tr>
                          <td></td>
                        </tr> -->

                        <tr class="gradeX">
                          <td><?php echo $m->Producto; ?></td>
                           <input type="hidden" name="idprod[]" value="<?php echo $m->idProducto; ?>">
                          <td><?php echo $m->Mat_Prima; ?></td>
                           <input type="hidden" name="Mat_Prima[]" value="<?php echo $m->idMat_Prima; ?>">
                          <td><?php echo $m->Descripcion; ?></td>
                          <!-- aqui usamos con -->
                          <td <?php  if($ex < $can){ echo "class = 'text-danger'"; $con++;}else{echo "class = 'text-info'";} ?> ><?php echo number_format($ex); ?></td>
                          <!--  -->
                          <td class = "text-success"><?php echo number_format($can); ?></td>
                           <input type="hidden" name="cantidad[]" value="<?php echo $can; ?>">
                          <td><?php echo $m->Unidad; ?></td>
                           <td>$<?php echo number_format($pre,2); ?></td>
                           <input type="hidden" name="pre[]" value="<?php echo $pre; ?>">
                           <td><?php echo $numP; ?></td>
                           <input type="hidden" name="numP[]" value="<?php echo $numP; ?>">
                           <input type="hidden" name="almacen[]" value="<?php echo $almacen; ?>">

                      </tr>
                    <?php endforeach; ?>
                   <?php } ?>
                      <tr class="gradeX">
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th id="total" class="text-primary" > $<?php echo number_format($preT,2, ".", "") ?>  </th>
                        <input type="hidden" name="total" value="<?php echo number_format($preT,2, ".", "") ?>">
                        <td></td>
                      </tr>
                   </tbody>
                      
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>
                      <!-- <input type="label" name="" value=""> -->
                </form>
                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }

         function matSu() {
            var con = <?php echo $con; ?>;
            if(con == 0){
              return true
            }else{
              alert('No hay materia prima suficiente')
              return false
            }
         }

        //  $(function() {
        //      var total = <?php echo $preT ?>;
        //      console.log(total);

        //      document.getElementById('total').innerHTML =  '$' +  new Intl.NumberFormat('es-MX').format(total);
        // });
       </script>
