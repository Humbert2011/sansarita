<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Producto';
    $crud = 0;
    if(is_object($model)){
        $titulo = 'Actualizar Producto';
        $crud = 1;
    } 
?>
<style>
input[type="number"]::-webkit-outer-spin-button, 
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}
</style>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Productos/index/'); ?>" >Productos</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de productos Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                          <!-- <?php echo form_open('productos/registrar'); ?> -->
                        
                            <form class="form-horizontal" action="<?php echo site_url('Productos/registrar'); ?>" method="post">
                                                              <p>Llena todos los campos para agregar un producto.</p>
                                                              <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idProducto;}  ?>">
                                <div class="form-group"><label class="col-lg-2 control-label">Producto</label>

                                    <div class="col-lg-10"><input name="Producto" type="text" placeholder="Producto" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Producto;}  ?>" >
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Descripcion</label>

                                    <div class="col-lg-10"><input name="Descripcion" type="text" placeholder="Descripcion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Descripcion;}  ?>"></div>
                                </div>
                                <?php  if(is_object($model)): ?>
                                <div class="form-group"><label class="col-lg-2 control-label">Precio de produccion</label>
                                    <div class="col-lg-10"><input disabled name="PrecioProd" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->PrecioProd;}else{echo '0';}  ?>" ></div>                                    
                                </div>
                                <?php endif;?>
                                <!-- <div class="form-group"><label class="col-lg-2 control-label">Precio de publico</label>
                                    <div class="col-lg-10"><input name="PrecioPublico" id="PrecioPublico" onkeyup="cal()" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->PrecioPublico;}  ?>" ></div>                                    
                                </div> -->
                                <!-- <div class="form-group"><label class="col-lg-2 control-label">Precio de distribucion</label> -->
                                    <!-- <div class="col-lg-10"> -->
                                    <!-- <input name="PrecioDistribuidor" id ="PrecioDistribuidor" type="hidden" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->PrecioDistribuidor;}else{echo '0';}  ?>" > -->
                                    <!-- </div>                                     -->
                                <!-- </div> -->
                                <!-- <div class="form-group"><label class="col-lg-2 control-label">Codigo de barras</label>
                                    <div class="col-lg-10"><input name="CodigoBarras" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->CodigoBarras;}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Precentacion</label>
                                    <div class="col-lg-10"><input name="Precentacion" type="text" placeholder="Precentacion del producto" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Precentacion;}  ?>"></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Unidad</label>
                                    <div class="col-lg-10"><input name="Unidad" type="text" placeholder="Precentacion del producto" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Unidad;}  ?>"></div>                                    
                                </div> -->
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Producto</button>
                                        <?php if(is_object($model)): ?><a href = " <?php echo site_url('Recetas/crud/'.$model->idProducto); ?>" <?php if (!is_object($model)){ echo "disabled"; } ?>  class="btn btn-w-m btn-warning">Alta de Receta</a><?php endif;?>
                                    </div>
                                </div>
                                <!-- <?php form_close(); ?> -->
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    let cal = () => {
                        let cadena = document.getElementById('PrecioPublico').value
                        let precioDis = cadena * 0.80;
                        console.log(precioDis);
                        document.getElementById('PrecioDistribuidor').value = precioDis
                    }
                    window.onload = iniCrud();
                    function iniCrud (){
                        let trueCrud = <?php echo $crud; ?>;
                        console.log(trueCrud);
                        if (trueCrud === 1) {
                            alert('Puede dar de alta o editar una receta de este producto ')
                        }
                    }
                </script>