<!-- <?php print_r($model) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Recetas</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Recetas</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Recetas/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <!-- <th>IdProducto</th> -->
                       <th>Producto</th>
                       <th>Descripcion</th>
                       <!-- <th>Codigo de barras</th> -->
                       <!-- <th>Precentacion</th>
                       <th>Unidad</th> -->
                       <th>Receta</th>
                       <th>Costo/gr</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <!-- <td><?php echo $m->idProducto; ?></td> -->
                        <td><?php echo $m->Producto; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <!-- <td><?php echo $m->CodigoBarras; ?> </td> -->
                        <!-- <td><?php echo $m->Precentacion; ?></td>
                        <td><?php echo $m->Unidad; ?></td> -->
                        <td><?php if($m->Receta== 0){
                                echo('<i class="fa fa-times"></i>');
                            }else{
                                echo '<i class="fa fa-check"></i>';
                            }
                        ?></td>
                        <td><?php echo $m->costo ?></td>
                        <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('Recetas/crud/'.$m->idProducto) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <!-- <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('Recetas/eliminar/'.$m->idProducto) ?>"  ><button  type="button" class="btn btn-w-m btn-danger">Eliminar</button></a> -->
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
