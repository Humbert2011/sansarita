<?php 
 //var_dump($model);
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Detalle Venta</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Detalle de Venta</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Pedidos/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                            <a href="<?php echo site_url('Ventas/listaV/'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-reply"></i>&nbsp;&nbsp;<span class="bold">Lista Ventas</span></button></a>
                            
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>id_Venta</th>
                         <th>Precio_Unitario</th> 
                         <th>Precio_Venta</th>
                         <th>Unidades</th>
                         <th>Presentacion</th>
                         <th>Subtotal</th>
                         <th>IVA</th>
                         <th>id_Venta</th>
                         <th>id_Almacen</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idDetalle_venta; ?></td>
                        <td><?php echo $m->Precio_Unitario; ?></td>
                        <td><?php echo $m->Precio_Venta; ?></td>
                        <td><?php echo $m->Unidades; ?></td>
                        <td><?php echo $m->Presentacion; ?></td>
                        <td><?php echo $m->Subtotal; ?></td>
                        <td><?php echo $m->IVA; ?></td>
                        <td><?php echo $m->idVenta; ?></td>
                        <td><?php echo $m->idAlmacen; ?></td>
                        
                          
                    </tr>
                   <?php endforeach; ?>
                   
                   </tbody>
                    <!--<tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
