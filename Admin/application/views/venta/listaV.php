<?php 
   //var_dump($model);
?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Ventas</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Ventas</h5>
                       <div class="ibox-tools">
                        <a href="<?php echo site_url('Ventas/crud'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar Venta</span></button></a>
                            <!--<a href="<?php echo site_url('Ventas/index'); ?>"><button class="btn btn-w-m btn-primary" type="button"><i class="fa fa-circle"></i>&nbsp;&nbsp;<span class="bold">Lista Pedidos</span></button></a>-->
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Pedidos/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                             <!--<a href="<?php //echo site_url('listaP/listaP/pedidoos'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-folder"></i>&nbsp;&nbsp;<span class="bold">Detalle Pedidos</span></button></a>-->
                            
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>id_Venta</th>
                        <th>Cliente</th>
                        <th>Total_Pedido</th>
                        <th>Descripcion</th>
                        <th>Fecha</th>
                        <!-- <th>Descripcion</th>-->
                         <!--<th>Total_Pago</th>-->
                        <!-- <th>id_Persona</th>-->
                         <!--<th>id_Cliente</th>-->
                         <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <tr class="gradeX">
                    <?php $preT = 0; ?>
                  <!--  <?php $preT = 0; ?>
                    <?php $deu = 0;?>
                    <?php $MontoT = 0;?>
                    <?php $deuT = 0;?>-->

                   <?php foreach ($model as $m):
                    $preT = $m->Total + $m->Total;
                  //  $preT = $m->Total + $preT;
                  //  $MontoT = $m->Monto_Pago + $MontoT;
                  //  $deu =  $m->Total - $m->Monto_Pago;
                  //  $deuT = $deu + $deuT;?>
                    <?php //foreach ($transaccion as $t): $deu = $m->Monto-$t->Monto;?>

                     <td><?php echo $m->idVenta; ?></td>
                     <td><?php echo $m->Cliente; ?></td>
                     <td>$<?php echo $m->Total;?></td>
                     <td><?php echo $m->Descripcion; ?></td>
                     <td><?php echo $m->Fecha; ?></td>
                       <!--<td id="PrecioPublico<?php //echo $m->idProducto ?>" >$00<div value="$<?php //echo $m->PrecioPublico; ?>"type="number" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></div></td>
                       <td id="PrecioPublico<?php //echo $m->idProducto ?>" >$00<div value="$<?php //echo $m->PrecioPublico; ?>"type="number" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></div></td>-->
                        <!--<td><?php echo $m->Total_Pago; ?></td>-->
                       <!-- <td><?php echo $m->idPersona; ?></td>-->
                        <!--<td><?php echo $m->idCliente; ?></td>-->
                        <td style="padding-right:0px">
                            <a href="<?php echo site_url('Ventas/detalleV/'.$m->idVenta) ?>"><button class="btn btn-primary " type="button"><i class="fa fa-paste"></i> Detalle</button></a>
                           
                           <!-- <a href="<?php echo site_url('Ventas/listaT/'.$m->idVenta) ?>"><button class="btn btn-w-m btn-warning " type="button"><i class="fa fa-paste"></i> Abono</button></a> -->
                            
                          
                    </tr>
                   <?php //endforeach; ?>
                   <?php endforeach; ?>
                   <tr class="gradeX">
                       <th>Total</th>
                        <td></td>
                       <th id="total" class="text-primary" > $<?php echo number_format($preT,2, ".", "") ?> </th>
                      <!-- <th id="total" class="text-primary" > $<?php echo number_format($MontoT,2, ".", "") ?> </th>
                       <th id="total" class="text-primary" > $<?php echo number_format($deuT,2, ".", "") ?> </th></td>-->

                       <td></td>
                       <td></td>
                   </tr>
                  </tbody>
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>