<?php
    // var_dump($model);
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Abono</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista Abono</h5>
                       <div class="ibox-tools">
                           
                            <a href="<?php echo site_url('Ventas/listaV/'); ?>"><button class="btn btn-primary" type="button"><i class="fa fa-reply"></i>&nbsp;&nbsp;<span class="bold">Ventas</span></button></a>
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>idTransaccion</th>
                         <th>Tipo</th> 
                         <th>Monto_Pago</th>
                         <th>Fecha</th>
                         <th>idCliente</th>
                         <th>idPedidos</th>
                         <th>Descripcion</th>
                         <th>idPreveedor</th>
                         <th>idTrabajador</th>
                         <!-- <th>Action</th> -->
                   </tr>
                   </thead>

                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idTransaccion; ?></td>
                        <td><?php echo $m->Tipo; ?></td>
                        <td><?php echo $m->Monto_Pago; ?></td>
                        <td><?php echo $m->Fecha; ?></td>
                        <td><?php echo $m->idCliente; ?></td>
                        <td><?php echo $m->idPedidos; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <td><?php echo $m->idProveedor; ?></td>
                        <td><?php echo $m->idPersona; ?></td>
                        
                    </tr>
                   <?php endforeach; ?>
           </tbody>
                        </div>
                   </div>
               </div>
           </div>
           </div>
       </div>


                        <!-- modal -->
       <!--<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-random modal-icon"></i>
                                            <h4 class="modal-title">De que almacen saldra el pedido?</h4>
                                            <small class="font-bold">Selecciones el almacen.</small>
                                        </div>
                                        <div class="form-horizontal" style="padding: 10px;" >
                                            <div class="form-group" >
                                            <div class="col-lg-12">
                                                <select class="form-control" name="" id="">
                                                    
                                                    <?php foreach ($almacenes as $a): ?>
                                                        <?php if($a->Tipo === 'Almacen'): ?>
                                                            <option value="<?php echo $a->idAlmacen; ?>"><?php echo $a->Nombre; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?> 
                                                </select>
                                            </div>
                                            </div>-->
                                            <!-- <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>

                                                      <div class="col-lg-10"><input id="NombreC" name="Nombre" type="text" placeholder="Nombre" class="form-control" value = "" required>
                                                      </div>
                                                  </div>
                                                  <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>

                                                      <div class="col-lg-10"><input id="ApellidosC" name="Apellidos" type="text" placeholder="Apellidos" class="form-control" value = "" required></div>
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Telefono</label>
                                                      <div class="col-lg-10"><input id="TelefonoC" name="Telefono" type="number" placeholder="Telefono" class="form-control" value = "" required></div>                                    
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Email</label>
                                                      <div class="col-lg-10"><input id="EmailC" name="Email" type="email" placeholder="Email" class="form-control" value = "" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required ></div>                                    
                                                  </div>               
                                                  <div class="form-group"><label class="col-lg-2 control-label">Foto</label>
                                                      <div class="col-lg-10"><input name="img" type="file" class="form-control" accept="image/png, image/jpeg, image/jpg" value = ""  required></div>                                    
                                                  </div> 
                                                    <input type="hidden" id="idPersona" name="idPersona" value="0"> -->
                                        <!--</div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal" onclick="" >Cerrar</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="" >Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

<!-- fin modal -->

       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
