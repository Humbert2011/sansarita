<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.7
*
-->
<!-- <?php var_dump($user); ?>> -->
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Sansarita</title>
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url('css/plugins/toastr/toastr.min.css'); ?>" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url('js/plugins/gritter/jquery.gritter.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') ?>">
    <link href="<?php echo base_url('css/animate.css" rel="stylesheet'); ?>">
    <link href="<?php echo base_url('css/style.css" rel="stylesheet'); ?>">    
    <link href="<?php echo base_url('css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    
    <script>
        let selectMenu = (activo) =>{
            let menu = activo.split(',')
            sessionStorage.setItem('menu',JSON.stringify(menu))
            // console.log(menu);
            
        }

        let activeMenu = () =>{
            elementos = sessionStorage.getItem(`menu`)
            console.log(elementos);
            if (elementos != null ) {
                elementos = JSON.parse(elementos)
                elementos.forEach(element => {
                    console.log(element);
                    let lista = document.getElementById(element);
                    lista.classList.add("active");
                });
            }
            
        }

    </script>

</head>
<body onload="activeMenu()">
    <div id="wrapper">



        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="50px" class="img-circle" src="<?php echo $user->Foto ?>" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $user->NombreCompleto ?></strong>
                             </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Perfil</a></li>
                                <li><a href="contacts.html">Contactos</a></li>
                                <li><a href="mailbox.html">Mensajes</a></li>
                                <li class="divider"></li>
                                <li><a onclick="return confirm('Esta seguro de querer cerrar sesion')" href="<?php echo site_url('Login/logout'); ?>">Cerrar Sesion</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            <img src="<?php echo base_url('img/sansarita.png'); ?>" width="40">
                        </div>
                    </li>
                    <li id="Home">
                        <a onClick = "selectMenu('Home')" href="<?php echo site_url('Inicio/index/'); ?>"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                    </li>

                    <li>
                        <li id="AdministracionMenu">
                         <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Administracion</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                    
                      <li id="PedidosMenu">
                        <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Pedidos</span><span class="fa arrow"></span></a>
                        <!-- <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Produccion</span><span class="label label-warning pull-right">16/24</span></a> -->
                        <ul class="nav nav-second-level collapse">
                            <li id="PedidosGMenu" onClick ="selectMenu('AdministracionMenu,PedidosMenu,PedidosGMenu')">
                                <a href="<?php echo site_url('Pedidos/index'); ?>">Generar</a>
                            </li>
                            <li id="PedidosLMenu" onClick ="selectMenu('AdministracionMenu,PedidosMenu,PedidosLMenu')">
                                <a href="<?php echo site_url('Pedidos/listaP'); ?>">Lista</a>
                            </li>
                            <!-- <li><a href="email_template.html">Email templates</a></li> -->
                        </ul>
                      </li>
                    <li id="CuentasCMenu">
                        <a onClick ="selectMenu('AdministracionMenu,CuentasCMenu')" href="<?php site_url('CuentasCobrar/index')?>"><i class="fa fa-money"></i> <span class="nav-label">Cuentas por cobrar</span></a>
                    </li>
                    <li id="VentasMenu">
                         <a onClick ="selectMenu('AdministracionMenu,VentasMenu')" href="<?php site_url('Ventas/listaV')?>"><i class="fa fa-sort-up"></i> <span class="nav-label">Ventas</span></a>
                    </li>
                    <li id="GastosMenu">
                        <a onClick ="selectMenu('AdministracionMenu,GastosMenu')" href="<?php echo site_url('Gasto/index')?>"><i class="fa fa-sort-desc"></i> <span class="nav-label">Gastos</span></a>
                    </li>
                    <li id="ComprasMenu">
                        <a onClick ="selectMenu('AdministracionMenu,ComprasMenu')" href="<?php echo site_url('CompraProducto/index');?>"><i class="fa fa-chevron-circle-down"></i> <span class="nav-label">Compras</span></a>
                    </li>
                    <li id="ClientesMenu">
                        <a onClick ="selectMenu('AdministracionMenu,ClientesMenu')" href="<?php echo site_url('Cliente/index'); ?>"><i class="fa fa-address-book-o"></i> <span class="nav-label">Clientes</span></a>
                    </li>
                         <li id = "ProveedoresMenu">
                        <a onClick ="selectMenu('AdministracionMenu,ProveedoresMenu')" href="<?php echo site_url('Proveedores/index'); ?>"><i class="fa fa-address-book-o"></i> <span class="nav-label">Proveedores</span></a>
                    </li>
                   </ul>
                  </li>
                 </li>
               <li>
                  <li id="OperacionMenu" >
                        <a href="#"><i class="fa fa-tasks"></i> <span class="nav-label">Operacion</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                    <li id="InventarioMenu">
                        <a onClick = "selectMenu('OperacionMenu,InventarioMenu')" href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Almacenes</span><span class="fa arrow"></a>
                        <ul class="nav nav-second-level collapse">
                            <li id="AlmacenAdmin"><a onclick="selectMenu('OperacionMenu,InventarioMenu,AlmacenAdmin')" href="<?php echo site_url("Inventario/listarAlmacenes") ?>">Adminitrar Almacenes</a></li>
                            <li id="AlmacenAdmin"><a onclick="selectMenu('OperacionMenu,InventarioMenu,AlmacenAdmin')" href="<?php echo site_url("Inventario/mat_prima") ?>">Materia Prima</a></li>
                            <li id="AlmacenAdmin"><a onclick="selectMenu('OperacionMenu,InventarioMenu,AlmacenAdmin')" href="<?php echo site_url("Inventario/mat_ind") ?>">Materia Industrial</a></li>
                            <li id="AlmacenAdmin"><a onclick="selectMenu('OperacionMenu,InventarioMenu,AlmacenAdmin')" href="<?php echo site_url("Inventario/linea") ?>">Productos</a></li>
                            <li id="AlmacenAdmin"><a onclick="selectMenu('OperacionMenu,InventarioMenu,AlmacenAdmin')" href="<?php echo site_url("Inventario/inventario") ?>">Inventario</a></li>
                        </ul>
                    </li>
                    <li id = "ProduccionMenu">
                        <a href="#"><i class="fa fa-th"></i> <span class="nav-label">Produccion</span><span class="fa arrow"></span></a>
                        <!-- <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Produccion</span><span class="label label-warning pull-right">16/24</span></a> -->
                        <ul class="nav nav-second-level collapse">
                            <li id = "ProduccionGMenu"><a onClick ="selectMenu('OperacionMenu,ProduccionMenu,ProduccionGMenu')" href="<?php echo site_url('Produccion/index'); ?>">Generar</a></li>
                            <li id = "ProduccionLMenu"><a onClick ="selectMenu('OperacionMenu,ProduccionMenu,ProduccionLMenu')" href="<?php echo site_url('Produccion/listaProd'); ?>">Lista</a></li>
                            <!-- <li><a href="email_template.html">Email templates</a></li> -->
                        </ul>
                    </li>
                    <li id="PersonalMenu">
                        <a href="#"><i class="fa fa-address-book-o"></i> <span class="nav-label">Personal</span><span class="fa arrow"></span></a>
                        <!-- <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Produccion</span><span class="label label-warning pull-right">16/24</span></a> -->
                        <ul class="nav nav-second-level collapse">
                            <li id="PersonalAMenu"><a onClick ="selectMenu('OperacionMenu,PersonalMenu,PersonalAMenu')" href="<?php echo site_url('Personal/administrador/'); ?>">Administrador</a></li>
                            <li id="PersonalVMenu"><a onClick ="selectMenu('OperacionMenu,PersonalMenu,PersonalVMenu')" href="<?php echo site_url('Personal/vendedor/'); ?>">Vendedor</a></li>
                            <li id="PersonalCMenu"><a onClick ="selectMenu('OperacionMenu,PersonalMenu,PersonalCMenu')" href="<?php echo site_url('Personal/cosinero/'); ?>">Cocinero</a></li>
                            <!-- <li><a href="email_template.html">Email templates</a></li> -->
                        </ul>
                      </li>
                    </ul>
                  </li>
                </li>
                <li>
                    <li id="ProductosMenu">
                        <a href="#"><i class="fa fa-barcode"></i> <span class="nav-label">Productos</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                        <li id="ProductosMMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosMMenu')" href="<?php echo site_url('MateriaPrima/index')?>"><i class="fa fa-leaf"></i> <span class="nav-label">Materia Prima</span></a>
                        </li>
                        <li id="ProductosMatIMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosMatIMenu')" href="<?php echo site_url('Materiaindustrial/index')?>"><i class="fa fa-cube"></i> <span class="nav-label">Materia Industrial</span></a>
                        </li>
                        <li id="ProductosPMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosPMenu')" href="<?php echo site_url('Productos/index/'); ?>"><i class="fa fa-tag"></i> <span class="nav-label">Producto</span>  </a>
                        </li>
                        <li id="ProductosRMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosRMenu')" href="<?php echo site_url('Recetas/index')?>"><i class="fa fa-fire"></i> <span class="nav-label">Recetas</span></a>
                        </li>
                        <li id="ProductosPreMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosPreMenu')" href="<?php echo site_url('Presentacion/index')?>"><i class="fa fa-codepen"></i> <span class="nav-label">Presentacion</span></a>
                        </li>
                        <li id="ProductosLineaMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosLineaMenu')" href="<?php echo site_url('Linea/index')?>"><i class="fa fa-linode"></i> <span class="nav-label">Lineas</span></a>
                        </li>
                        <li id="ProductosEmbalajeMenu">
                            <a onClick ="selectMenu('ProductosMenu,ProductosLineaMenu')" href="<?php echo site_url('Embalaje/index')?>"><i class="fa fa-archive"></i> <span class="nav-label">Embalaje</span></a>
                        </li>
                        </ul>
                    </li>
                 </li>                   
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <!-- <input type="text" placeholder="Buscar..." class="form-control" name="top-search" id="top-search"> -->
                </div>
            </form>
        </div>

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Bienvenido al sistema de administracion Sansarita.</span>
                </li>
                <!-- <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url('img/a7.jpg'); ?>">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url('img/a4.jpg');?>">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url('img/profile.jpg');?>">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li> -->


                <li>
                    <a onclick="return confirm('Esta seguro de querer cerrar sesion')" href="<?php echo site_url('Login/logout'); ?>">
                        <i class="fa fa-sign-out"></i> Cerrar Sesion
                    </a>
                </li>
                <li>
                   <!--  <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a> -->
                </li>
            </ul>

        </nav>
        </div>
