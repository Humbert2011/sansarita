<!-- <?php var_dump($model); ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Proveedores</h2>                   
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Proveedores</h5>
                       <div class="ibox-tools">                           
                            <a href="<?php echo site_url('Proveedores/crud'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>                          
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>Proveedor</th>
                       <th>Direccion</th>
                       <th>Telefono</th>
                       <th>RFC</th>  
                       <th>Acciones</th>                                          
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->Proveedor; ?></td>
                        <td><?php echo $m->Direccion; ?></td>
                        <td><?php echo $m->Telefono; ?></td>                       
                        <td><?php echo $m->RFC; ?></td>                       
                        <td style="width: 340px"> 
                            <a href="<?php echo site_url('Proveedores/crud/'.$m->idProveedor); ?>">
                              <button class="btn btn-info " type="button">
                                <i class="fa fa-paste"></i> Editar
                              </button>
                            </a>
                            <a onclick ="return confirm('Esta seguro de eliminar este Proveedor?')" href="<?php echo site_url('Proveedores/eliminar/'.$m->idProveedor) ?>"  ><button  type="button" class="btn btn-danger">Eliminar</button></a>

                           <!-- <a href="<?= site_url('Personal/eliminar/'.$m->idProveedor);?>"><button type="button" class="btn btn-warning">Detalles</button></a>-->
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>                  
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
