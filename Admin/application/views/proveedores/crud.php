<?php 
  if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Proveedor';
    if(is_object($model)){
        $titulo = 'Actualizar Proveedor';
        // echo $proveedores;
        // echo("Tipo Persona= " .$model->idtipoPersona);
        // echo("  Area Persona= " .$model->idArea);
    }  
   // var_dump($model);
 ?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2><?php echo $titulo ?></h2>

                   <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Proveedores/index/'); ?>" >Proveedores</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                  </ol>                   
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5><?= $titulo ?></h5>
              <div class="ibox-tools">                  
              </div>
          </div>
          <div class="ibox-content">
              <form class="form-horizontal" action="<?= site_url('Proveedores/registrar') ?>" method="POST" enctype="multipart/form-data" >
                  <p>Llena todos los campos para <?php echo $titulo; ?></p>
                  <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idProveedor;}  ?>" required>

                  <div class="form-group"><label class="col-lg-2 control-label">Proveedor</label>

                      <div class="col-lg-10"><input name="Proveedor" type="text" placeholder="Proveedor" class="form-control" value = "<?php  if(is_object($model)){echo $model->Proveedor;}  ?>" required>
                      </div>
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Direccion</label>

                      <div class="col-lg-10"><input name="Direccion" type="text" placeholder="Direccion" class="form-control" value = "<?php  if(is_object($model)){echo $model->Direccion;}  ?>" required></div>
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Telefono</label>
                      <div class="col-lg-10"><input name="Telefono" type="number" placeholder="Telefono" class="form-control" value = "<?php  if(is_object($model)){echo $model->Telefono;}  ?>" required></div>                                    
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">RFC</label>
                      <div class="col-lg-10"><input name="RFC" type="email" placeholder="RFC" class="form-control" value = "<?php  if(is_object($model)){echo $model->RFC;}  ?>" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required ></div>                                    
                  </div>                  
                  <div class="form-group"><label class="col-lg-2 control-label">Contacto 1</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="contacto1" required>
                          <!-- consumir servicio para listar -->

                          <?php foreach ($contactos as $ListaP): ?>

                            <option value="<?= $ListaP->idPersona?>" <?php if(is_object($model) && $model->idPersona == $ListaP->idPersona) echo "selected"?>><?= $ListaP->Nombre ?><?= " ".$ListaP->Apellidos ?></option>
                            
                          <?php endforeach ?>
                            
                        </select>
                      </div>                                    
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Contacto 2</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="contacto2" required>
                          <!-- consumir servicio para listar -->

                          <?php foreach ($contactos as $ListaP): ?>

                            <option value="<?= $ListaP->idPersona?>" <?php if(is_object($model) && $model->idPersona == $ListaP->idPersona) echo "selected"?>><?= $ListaP->Nombre ?><?= " ".$ListaP->Apellidos ?></option>
                            
                          <?php endforeach ?>
                            
                        </select>
                      </div>                                    
                  </div>
                 
                  <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                          <button onclik="" class="btn btn-sm btn-primary" type="submit"><?= $titulo?></button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>