<?php
    // print_r($almacenes);
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Detalle Pedidos</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Detalle de Pedido</h5>
                       <div class="ibox-tools">
                           
                            <a href="<?php echo site_url('Pedidos/listaP/'); ?>"><button class="btn btn-primary" type="button"><i class="fa fa-reply"></i>&nbsp;&nbsp;<span class="bold">Pedidos</span></button></a>
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#myModal" ><i class="fa fa-mail-forward"></i>&nbsp;&nbsp;<span class="bold">Salida de Pedido</span></button>
                            <a href="<?php echo site_url('Pedidos/listaP/'); ?>"><button class="btn btn-danger" type="button"><i class="fa fa-times"></i>&nbsp;&nbsp;<span class="bold">Cancelar</span></button></a>
                           
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>Producto</th>
                         <th>Cantidad</th> 
                         <th>Precio Publico</th>
                         <th>Descuento</th>
                         <th>Monto</th>
                         <!-- <th>Action</th> -->
                   </tr>
                   </thead>

                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->Producto; ?></td>
                        <td><?php echo $m->Cantidad; ?></td>
                        <td><?php echo '$'.$m->PrecioPublico; ?></td>
                        <td><?php echo $m->Descuento; ?></td>
                        <td><?php echo '$'.number_format($m->Monto, 2, '.',''); ?></td>
                        
                    </tr>
                   <?php endforeach; ?>
                  
                   </tbody>
                   <tr>
                        <th>Subtotal</th>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo '$'.number_format($pedido->SubTotal, 2, '.',''); ?></td>
                   </tr>
                   <tr>
                        <th>IVA</th>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php if ($pedido->IVA == 1) {
                            echo "$".number_format(($pedido->SubTotal)*0.16, 2, '.','')."";
                        }else{
                            echo '$00.00';
                        } ?></td>
                   </tr>
                   <tfoot>
                   <tr>
                         <th>Total</th>
                         <th></th> 
                         <th></th>
                         <th></th>
                         <th><?php echo '$'.number_format($pedido->Total, 2, '.',''); ?></th>
                         <!-- <th>Action</th> -->
                   </tr>
                   </tfoot>
                   </table>
                       </div>
                       <div class="row">
                            <strong class="col-xs-12">
                            
                            <!-- <input type="checkbox" onClick = "if(this.checked == true){calIva()}else{quitarIva()}" name="ivaselect" class="iCheck-helper" id="ivaselect"><label for="iva">IVA</label> -->
                            </strong>
                            <strong class="col-xs-6">
                                <p><?php echo $pedido->Notas; ?></p>
                                <!-- <input type="text"  style="padding-bottom:100px " name="Notas" class="form-control" placeholder=" Notas:"></strong> -->
                            <strong div class="col-xs-6">
                                <p><?php echo $pedido->Politicas; ?></p>
                                <!-- <input type="text"  style="padding-bottom:100px " name="Politicas" class="form-control" placeholder="Politicas de Venta:"></strong> -->
                                  <div style="margin: 36px 0 0 200px;"></div>
                        </div>
                   </div>
               </div>
           </div>
           </div>
       </div>


                        <!-- modal -->
       <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-random modal-icon"></i>
                                            <h4 class="modal-title">De que almacen saldra el pedido?</h4>
                                            <small class="font-bold">Selecciones el almacen.</small>
                                        </div>
                                        <div class="form-horizontal" style="padding: 10px;" >
                                            <div class="form-group" >
                                            <div class="col-lg-12">
                                                <select class="form-control" name="" id="">
                                                    
                                                    <?php foreach ($almacenes as $a): ?>
                                                        <?php if($a->Tipo === 'Almacen'): ?>
                                                            <option value="<?php echo $a->idAlmacen; ?>"><?php echo $a->Nombre; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?> 
                                                </select>
                                            </div>
                                            </div>
                                            <!-- <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>

                                                      <div class="col-lg-10"><input id="NombreC" name="Nombre" type="text" placeholder="Nombre" class="form-control" value = "" required>
                                                      </div>
                                                  </div>
                                                  <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>

                                                      <div class="col-lg-10"><input id="ApellidosC" name="Apellidos" type="text" placeholder="Apellidos" class="form-control" value = "" required></div>
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Telefono</label>
                                                      <div class="col-lg-10"><input id="TelefonoC" name="Telefono" type="number" placeholder="Telefono" class="form-control" value = "" required></div>                                    
                                                  </div>
                                                  <div class="form-group"><label  class="col-lg-2 control-label">Email</label>
                                                      <div class="col-lg-10"><input id="EmailC" name="Email" type="email" placeholder="Email" class="form-control" value = "" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required ></div>                                    
                                                  </div>               
                                                  <div class="form-group"><label class="col-lg-2 control-label">Foto</label>
                                                      <div class="col-lg-10"><input name="img" type="file" class="form-control" accept="image/png, image/jpeg, image/jpg" value = ""  required></div>                                    
                                                  </div> 
                                                    <input type="hidden" id="idPersona" name="idPersona" value="0"> -->
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal" onclick="" >Cerrar</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="" >Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<!-- fin modal -->

       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
