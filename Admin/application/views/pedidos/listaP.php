<?php 
   // print_r($model);
//var_dump($cliente);
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Pedidos</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Pedidos</h5>
                       <div class="ibox-tools">

                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Pedidos/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                             <!--<a href="<?php //echo site_url('listaP/listaP/pedidoos'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-folder"></i>&nbsp;&nbsp;<span class="bold">Detalle Pedidos</span></button></a>-->
                            
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                         <th>id_Pedido</th>
                         <th>Cliente</th>
                         <th>Monto</th>
                         <th>Fecha_Pedido</th>
                         <th>Action</th>
                   </tr>
                   </thead>
               

                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idPedidos; ?></td>
                        <td><?php echo $m->Cliente;?></td>
                        <td><?php echo '$'.number_format($m->Total, 2, '.',''); ?></td>
                        <td><?php echo $m->Fecha_Pedido; ?></td>
                        <td style="padding-right:0px">
                            <a href="<?php echo site_url('Pedidos/detalleP/'.$m->idPedidos) ?>"><button class="btn btn-primary " type="button"><i class="fa fa-paste"></i> Detalle</button></a>
                            
                          
                    </tr>
                   <?php endforeach; ?>
                   
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
