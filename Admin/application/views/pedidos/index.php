<style>
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}

</style>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2><strong>Pedido Nuevo</strong></h2>
               </div>
               <div class="col-lg-2">
               </div>
           </div>
          <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-content">
                    <h2>Llena todos los campos para agregar un Pedido.</h2>
                    <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $m->idPedidos;}  ?>">
                <form name="Action" action="<?php echo site_url('Pedidos/alta'); ?>" method="post"  onsubmit="return matSu()" >
              <div align="right"><button disabled type="submit" id="tbpedidos" class="btn btn-w-m btn-primary" >Dar de Alta Pedidos</button></div>
               <h4><i class="fa fa-address-book-o" aria-hidden="true"></i> Pedido realizado por: </h4>
                       <div class="table-responsive">

                   <table class="table table-striped table-bordered table-hover dataTables-example">
                   <thead>
                    <td>
                          <label for=""><h4>Elige El Cliente:</h4></label>
                            <select name="cliente" id="cliente" class="form-control" onchange = "calDescuento()"  required>
                            <option value="0">Seleccione...</option>
                              <?php foreach ($cliente as $cli): ?>
                                <option value="<?php echo $cli->idCliente;?>"><?php echo $cli->Cliente?></option>
                              <?php endforeach;?>
                              </select>
                            <!-- <input type="hidden" id="idCliente" name="idCliente" value = "0" > -->
                    </td>
                    <td>
                      <div class="col-lg-6">
                        <label for=""><h4>%Margen:</h4></label>
                        <input class="form-control" type="text" onkeyup="cambioMargen()" value = "" disabled required="true" wname="margen" id="margen" placeholder ="%00" >
                      </div>
                    </td>
                   <tr>
                       <th>Producto</th>
                       <th>#Frascos</th>
                       <th>$Precio</th>
                       <!-- <th>%Descuento</th> -->
                       <th>$Subtotal</th>
                       <th>#Cajas</th>
                       <th>#Kg</th>
                   </tr>
                   </thead>
                    <tbody>
                    
              <?php foreach ($model as $m):?>
                <tr class="gradeX">
                       <td><?php echo $m->Producto?></td>
                       <!-- <input name="Producto[]" type="hidden" class="form-control" required="true" value="<?php echo $m->Producto;?>"> -->
                       <input name="idProducto[]" type="hidden" class="form-control" required="true" value="<?php echo $m->idProducto;?>">
                       <td style="padding-right:0px">
                          <div id="tbpedidos" class="form-group">
                            <div class="col-lg-2">
                              <input disabled id="cantidad<?php echo $m->idProducto ?>" name="cantidad[]" onkeyup="calPrecio(<?php echo $m->PrecioPublico ;?>,<?php echo $m->idProducto ?>,<?php echo $m->Precentacion ;?>)"  type="number" placeholder="00" class="form-control" required="true" min="0"  step="1"  style="width:70px">
                            </div>
                            </div>
                        </td>

                        <td id="PrecioPublico<?php echo $m->idProducto ?>" ><?php echo  '$'.$m->PrecioPublico; ?></td>
                        <input type="hidden" id ="precio<?php echo $m->idProducto ?>" name="precio<?php echo $m->idProducto ?>" value="<?php echo  $m->PrecioPublico; ?>">
                        <input type="hidden" id ="presentacion<?php echo $m->idProducto ?>" name ="presentacion<?php echo $m->idProducto ?>" value ="<?php echo $m->Precentacion?>">
                        <!-- <td  style="padding-right:0px;"> -->
                            
                              <input disabled id="descuento<?php echo $m->idProducto ?>" name="descuento[]" onchange="calPrecio(<?php echo $m->PrecioPublico ;?>,<?php echo $m->idProducto ?>,<?php echo $m->Precentacion ;?>)" type="hidden" placeholder="0%" class="form-control" min="0"  step=".000000000000001" style="width:70px">
                              <input type="hidden" name="valores[]" value = "<?php echo $m->PrecioPublico ;?>,<?php echo $m->idProducto ?>,<?php echo $m->Precentacion ;?>">
                        <!-- </td> -->
                        <td id="subtotal<?php echo $m->idProducto ?>" name='subtotal' >$00</td>
                        <input type="hidden" id="subtotalUnitario<?php echo $m->idProducto ?>" name="subtotalUnitario[]">
                        <td id="cajas<?php echo $m->idProducto ?>" >00</td>
                        <td id="kg<?php echo $m->idProducto ?>" >00</td>


                   <?php endforeach; ?>
                  <tr>
                   <th>Subtotal</th>
                   <th></th>
                   <th></th>
                   <!-- <th></th> -->
                   <td id="subtotal" >$00<div value="$<?php echo $m->PrecioPublico; ?>"type="number" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></td>
                   <input id="subtotalVal" type="hidden" name="subtotalVal">
                   <th></th>
                   <th></th>
                  </tr>
                  <tr>
                   <th>%Descuento</th>
                   <th></th>
                   <th></th>
                   <!-- <th></th> -->
                   <td><input type="number" class="form-control" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></td>
                   <th></th>
                   <th></th>
                  </tr>
                   <tr lass="gradeX">
                   <th>IVA</th>
                   <th></th>
                   <th></th>
                   <!-- <th></th> -->
                   <td id="iva" >$00<div value="" type="number" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></td>
                   <input id="ivaVal" type="hidden" name="ivaVal" value ="0">
                   <th></th>
                   <th></th>
                   </tr>
                 <tr>
                   <th>Total</th>
                   <th></th>
                   <th></th>
                   <!-- <th></th> -->
                   <td id="totalP" >$00<div value="$<?php echo $m->PrecioPublico; ?>"type="number" placeholder="00" required="true" min="0"  step=".01" style="width:60px"></td>
                   <input id="totalPVal" type="hidden" name="totalPVal">
                   <th></th>
                   <th></th>
                 </tr>
                 <tr>
                 </tbody>
                 </table>
                 
                  <div class="row">
                    <strong class="col-xs-12">
                      <input type="checkbox" onClick = "if(this.checked == true){calIva()}else{quitarIva()}" name="ivaselect" class="iCheck-helper" id="ivaselect"><label for="iva">IVA</label>
                    </strong>
                    <strong class="col-xs-6">
                        <input type="text"  style="padding-bottom:100px " name="Notas" class="form-control" placeholder=" Notas:"></strong>
                    <strong div class="col-xs-6">
                        <input type="text"  style="padding-bottom:100px " name="Politicas" class="form-control" placeholder="Politicas de Venta:"></strong>
                                  <div style="margin: 36px 0 0 200px;"></div>
                  </div>
            </div>

            <div id="alerta">  </div>
            </tr>
            </form>
               <!--</form>-->
            </div>
         </div>
      </div>
  </div>
     <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
     <script type="text/javascript">
       function confirmacio() {
         confirm('Esta seguro de eliminar este Producto?');
       }

      function calPrecio(precio = 0,idProducto,gramos){
        let cantidad = document.getElementById(`cantidad${idProducto}`).value
        let total = precio
        // if (cantidad > 0 ) {
            total = cantidad * precio
            document.getElementById(`PrecioPublico${idProducto}`).innerHTML = `$${total}`
            calCajas(cantidad,idProducto)
            calKg(cantidad,idProducto,gramos)
            calSubtotal(cantidad,idProducto,precio)
            calTotal()
        // }

      }

    function desPrecio(precio = 0,idProducto){
        let descuento = document.getElementById(`descuento${idProducto}`).value
        let total = precio
        if (descuento > 0 ) {
            total = descuento - precio
            document.getElementById(`PrecioPublico${idProducto}`).innerHTML = `$${total}`
        }
      }
      function calSubtotal(cantidad = 0,idProducto = 0,precio = 0){
          let descuento =  document.getElementById(`descuento${idProducto}`).value
          if(descuento == '')descuento = 0;
          let subtotal = (precio * cantidad)
          subtotal = subtotal - (subtotal*(descuento/100))
          document.getElementById(`subtotal${idProducto}`).innerHTML = `$${subtotal}`
          document.getElementById(`subtotalUnitario${idProducto}`).value = subtotal
        }
      function calCajas(cantidad = 0,idProducto = 0){
          if(cantidad == 0) {
            document.getElementById(`cajas${idProducto}`).innerHTML = 0
             return true
          }
          let cajas = cantidad/12
          document.getElementById(`cajas${idProducto}`).innerHTML = cajas.toFixed(2)
      }
      function calKg(cantidad = 0,idProducto = 0,gramos = 0 ){
          let kg = (cantidad * gramos)/1000
          document.getElementById(`kg${idProducto}`).innerHTML = kg
      }

      let calTotal = () =>{
        let subtotal =  document.getElementsByName('subtotal')
        let total = 0
        subtotal.forEach(element => {
             let text = element.textContent
             let valor = text.substr(1,text.length)
             valor = parseFloat(valor)
             total = valor + total
        })
        document.getElementById('subtotal').innerHTML = `<b>$${total}</b>`
        document.getElementById('subtotalVal').value = total
        total = total + parseFloat(document.getElementById('ivaVal').value)
        total = total.toFixed(2)
        document.getElementById('totalP').innerHTML = `<b>$${total}</b>`
        document.getElementById('totalPVal').value = total
     }

    let calDescuento = (iva = 0) =>{
      let des = document.getElementById('cliente').value
      if(des != 0){
          let cliente = <?php echo json_encode($cliente); ?>;
          let listaDes = document.getElementsByName('descuento[]')
          if(iva == 0){
            cliente.forEach(element => {
              
              if(element['idCliente'] == des){
                document.getElementById('margen').value = element['Margen']
                listaDes.forEach(lista => {
                    lista.value = element['Margen']
                })
              }
            })
          }else{

          }
          document.getElementById('margen').disabled = false
          cantidad = document.getElementsByName('cantidad[]')
          cantidad.forEach(element => {
              element.disabled = false
          });
          descuento = document.getElementsByName('descuento[]')
          descuento.forEach(element =>{
            element.disabled = false
          })
          subtotal = document.getElementsByName('subtotal')
          subtotal.forEach(element => {
            let id = element.id
            let length = id.length
            id = id.substr(length - 1,1)
            let precio = document.getElementById(`precio${id}`).value
            let precentacion = document.getElementById(`presentacion${id}`).value
            calPrecio(precio,id,precentacion)
          });

          document.getElementById('tbpedidos').disabled = false
      }else{
        alert('No se lecciono un Cliente valido')
        document.getElementById('tbpedidos').disabled = true
        document.getElementById('margen').value = ''
        document.getElementById('margen').disabled = true
        cantidad = document.getElementsByName('cantidad[]')
        cantidad.forEach(element => {
            element.value = ''
            element.disabled = true
        })
        descuento = document.getElementsByName('descuento[]')
          descuento.forEach(element =>{
            element.value = ''
            element.disabled = true
          })

      }

    }
    
    let calIva = () =>{
      let cliente = document.getElementById('cliente').value
      if(cliente != 0){
        let descuento = document.getElementsByName('descuento[]')
        descuento.forEach(element => {
          let des = element.value
          let id = element.id
          let length = id.length
          id = id.substr(length - 1,1)
          let cantidad = document.getElementById(`cantidad${id}`).value
          if (cantidad > 0) {
              let precio = document.getElementById(`precio${id}`).value
              let valordes = precio * ((100-des)/100)
              des = 100-(((valordes)/(1.16*precio))*100)
              element.value = des
              // console.log(des);
              document.getElementById('margen').value = des.toFixed(2)
          }
          
        });
       
        calDescuento(1)
        let iva = document.getElementById('subtotalVal').value
        console.log(iva);
        iva = (iva *1.16)-document.getElementById('subtotalVal').value
        document.getElementById('ivaVal').value = iva
        document.getElementById('iva').innerHTML = `$${iva}`
        calTotal()
      }else{
        calDescuento()
        calTotal()
        document.getElementById('ivaselect').checked = 0
      }
    }
    let quitarIva = () => {
      calDescuento()
        calTotal()
        document.getElementById('ivaVal').value = 0
        document.getElementById('iva').innerHTML= `$00`
        calTotal()
    }

      let cambioMargen = () =>{
        let descuento = document.getElementsByName('descuento[]')
        descuento.forEach(element => {
          element.value = document.getElementById('margen').value
        });
        let valores = document.getElementsByName('valores[]')
        valores.forEach(element => {
            let array = element.value
            array = array.split(',')
            calPrecio(array[0],array[1],array[2])
        });
      }
     </script>
