    <!--<?php print_r($model) ?><br><br>
    <?php var_dump($almacen) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Almacen <?php echo $almacen->Nombre; ?> </h2>
                   <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Inventario/index/'); ?>" >Almacenes</a>
                       </li>
                       <li class="active">
                           <strong>Almacen <?php echo $almacen->Nombre; ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Almacenes</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <a href="<?php echo site_url('Inventario/add/'.$almacen->idAlmacen.'/'.$almacen->Tipo); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <!-- <th>Almacen</th> -->
                       <th>Producto</th>
                       <th>Precentacion</th>
                       <th>Existencia</th>
                       <?php if( $almacen->Tipo == 'Materia' ){ echo '<th>Unidad</th>'; }else{echo '<th>Cajas</th>';} ?>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php if( $almacen->Tipo == 'Materia' ){ echo $m->Mat_Prima; }else{echo $m->Producto;} ?></td>
                        <!-- <td><?php echo $m->Precentacion; ?></td> -->
                        <td><?php if($almacen->Tipo != 'Materia'){echo $m->Precentacion; }else{echo $m->Descripcion;}?></td>
                        <td><?php echo $m->Cantidad; ?></td>
                        <td><?php if($almacen->Tipo != 'Materia'){echo ($m->Cantidad)/12;}else{echo $m->Unidad;} ?></td>
                        <td style="padding-right:0px">
                            <a href="<?php echo site_url('Inventario/activar/'.$m->idAlmacen) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i>Editar</button></a>
                            <a onclick ="return confirm(`Esta seguro de cerrar este Elemento del Inventario?`)" href="<?php echo site_url('Inventario/eliminarElementoI/'.$m->idDetalle_Almacen.'/'.$almacen->idAlmacen) ?>"><button class="btn btn-danger " type="button"><i class="fa fa-paste"></i> Eliminar</button></a>
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
