<?php 
    var_dump($modo);
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Almacenes</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Almacenes</h5>
                       <div class="ibox-tools">
                       <a href="<?php echo site_url('Inventario/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                    <?php if($modo == 1): ?>
                        <a href="<?php echo site_url('Inventario/listarAlmacenes/0'); ?>"><button class="btn btn-w-m btn-warning" type="button"><i class="fa fa-ban"></i>&nbsp;&nbsp;<span class="bold">Inactivos</span></button></a>
                    <?php elseif ($modo == 0): ?>
                        <a href="<?php echo site_url('Inventario/listarAlmacenes/'); ?>"><button class="btn btn-w-m btn-primary" type="button"><i class="fa fa-circle-thin"></i>&nbsp;&nbsp;<span class="bold">Activos</span></button></a>
                    <?php endif; ?>
                    </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>Almacen</th>
                       <th>Descripcion</th>
                       <!-- <th>Tipo</th> -->
                       <th>Accion</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                    
                     <tr class="gradeX">
                        <td><?php echo $m->Nombre; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <!-- <td><?php echo $m->TipoAlmacen; ?></td> -->
                        <td>
                            <a href="<?php echo site_url('Inventario/crud/'.$m->idAlmacen);?>"><button class="btn btn-w-m btn-primary"><i class="fa fa-book"></i>&nbsp;&nbsp;Editar</button></a>
                        <?php if($modo == 1): ?>
                            <a href="<?php echo site_url('Inventario/eliminar/'.$m->idAlmacen);?>"><button  class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Cerrar</button></a>
                        <?php elseif ($modo == 0): ?>
                            <a href="<?php echo site_url('Inventario/activar/'.$m->idAlmacen);?>"><button  class="btn btn-w-m btn-info"><i class="fa fa-check"></i>&nbsp;&nbsp;Activar</button></a>
                        <?php endif; ?>
                        </td>
                    </tr>
                    <?php

                    ?>
                   <?php endforeach; ?>
                   
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
