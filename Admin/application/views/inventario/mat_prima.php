<?php 
    // print_r($model->data);
    // $model = $model->data;
?>
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Detalle de Almacenes en Materia Prima</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-4">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Materia prima</h5>
                       <div class="ibox-tools">
                              <!-- <a href="<?php echo site_url('Inventario/add/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-sup"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <!-- <a href="<?php echo site_url('Inventario/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a> -->
                   <!--          <a href="<?php echo site_url('Inventario/index/'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-reply"></i>&nbsp;&nbsp;<span class="bold">Almacenes</span></button></a>                -->
                            
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                        
                   <table class="table table-striped" >
                   <thead>
                   <tr>
                       <th>idMat_Prima</th>
                       <th>Descripcion</th>
                      <!-- <th>Action</th>-->
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):  ?>
                     <tr class="gradeX">
                        <td><?php echo $m->idMat_Prima; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <!--<td style="padding-right:0px"> 
                        <a href="<?php echo site_url('Inventario/add/'.$m->idMat_Prima) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                      </td>-->
                       <!-- <td><?php echo $m->Descripcion; ?></td>-->
                    </tr>
                   <?endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
                      <div class="col-lg-8">
                 <div class="ibox float-e-margins">
                   <div class="ibox-title">
                    <h3>Consultar Detalle de Almacenes con Materia Prima</h3><br>
                    <!-- <label class="control-label">Escoje almacen de materia prima </label> -->
                    <div class="ibox-content">
                    
                      
                         <table class="table table-striped" >
                   <thead>
                          <label for=""><h4>Elige El Almacen:</h4></label>
                           <div> <h4><select name="almacenes" id="almacenes" class="col-lg-19" onchange = "seleccioneMateriaP()"  required>
                            <option value="0">Seleccione...</option>
                              <?php foreach ($model as $cli): ?>
                                <option value="<?php echo $cli->idMat_Prima;?>"><?php echo $cli->Nombre?></option>
                              <?php endforeach;?>
                              </select><h4></div>
                            <!-- <input type="hidden" id="idCliente" name="idCliente" value = "0" > -->
                    
                   <tr>
                       <th>idMat_Prima</th>
                       <th>Descripcion</th>
                       <th>Unidad</th>
                       <!--<th>Activo</th>-->
                       <th>Cantidad</th>
                       <th>costo</th>
                      <!-- <th>Action</th>-->
                   </tr>
                   </thead>
                   <tbody>
                   <?php ?>
                     <tr class="gradeX">
                        <td>
                        <input class="form-control" type="text" value = "" disabled required="true" wname="idMat" id="idMat" placeholder ="id" >
                    </td>
                        <td>
                        <input class="form-control" type="text" value = "" disabled required="true" wname="descripcion" id="descripcion" placeholder ="Materia_Prima" >
                    </td>
                        <td>
                        <input class="form-control" type="text" value = "" disabled required="true" wname="unidad" id="unidad" placeholder ="Mr/Gr" >
                    </td>
                       <!-- <td><?php if($m->Materia== 0){
                                echo('<i class="fa fa-times"></i>');
                            }else{
                                echo '<i class="fa fa-check"></i>';
                            }
                        ?></td>-->
                     <td>
                        <input class="form-control" type="text" value = "" disabled required="true" wname="cantidad" id="cantidad" placeholder ="00" >
                    </td>
                    <td>
                        <input class="form-control" type="text" value = "" disabled required="true" wname="Costo" id="Costo" placeholder ="$00" >
                    </td>
                        <!--<td style="padding-right:0px"> 
                        <a href="<?php echo site_url('Inventario/add/'.$m->idMat_Prima) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                      </td>-->
                       <!-- <td><?php echo $m->Descripcion; ?></td>-->
                    </tr>
                   
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                           
           </div>
           <label></label>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
    let seleccioneMateriaP = (a = 0) =>{
      let des = document.getElementById('almacenes').value
      if(des != 0){
          let almacenes = <?php echo json_encode($model); ?>;
          if(a == 0){
            almacenes.forEach(element => {
              
              if(element['idMat_Prima'] == des){
                document.getElementById('idMat').value = element['idMat_Prima']
                document.getElementById('descripcion').value = element['Descripcion']
                 document.getElementById('unidad').value = element['Unidad']
                  document.getElementById('cantidad').value = element['Cantidad']
                   document.getElementById('Costo').value = element['costo']
                listaDes.forEach(lista => {
                    lista.value = element['idMat_Prima']
                    lista.value = element['Descripcion']
                    lista.value = element['Unidad']
                    lista.value = element['Cantidad']
                    lista.value = element['costo']
                })
              }
            })
          }else{
          }

          document.getElementById('tbalmacen').disabled = false
      }else{
        alert('No se lecciono un Almacen valido')
        document.getElementById('tbalmacen').disabled = true
      }
    }
       </script>
