 <!-- <?php print_r($model) ?><br><br> -->
<!-- <?php print_r($productos) ?> -->
<!-- <br><br> -->
<!-- <?php print_r($datos) ?> -->
<!-- <?php var_dump($token) ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Almacenes</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Almacenes</h5>
                       <div class="ibox-tools">
                           <!-- <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a> -->
                            <a href="<?php echo site_url('Inventario/crud/'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>
                            <?php 
                              if ($a==1) {
                                $url = site_url('Inventario/index/0');
                                echo '<a href="'.$url.'"><button class="btn btn-w-m btn-warning" type="button"><i class="fa fa-circle"></i>&nbsp;&nbsp;<span class="bold">Inactivos</span></button></a>';
                              }else{
                                $url = site_url('Inventario/index');                               
                                echo '<a href="'.$url.'"><button class="btn btn-w-m btn-primary" type="button"><i class="fa fa-circle"></i>&nbsp;&nbsp;<span class="bold">Activos</span></button></a>';
                              }
                             ?>
                             <a href="<?php echo site_url('Inventario/inventario/producto'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-folder"></i>&nbsp;&nbsp;<span class="bold">Inv. Prods</span></button></a>
                             <a href="<?php echo site_url('Inventario/inventario/mat_prima'); ?>"><button class="btn btn-primary " type="button"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;<span class="bold">Inv. Mat P</span></button></a>
                            
                           <!-- <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul> -->
                           <!-- <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a> -->
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>IdAlmacen</th>
                       <th>Nombre</th>
                       <th>Direccion</th>
                       <!-- cambiar a dinamico -->
                       <?php foreach ($productos as $prod): ?>
                       <th  colspan='2'><?php echo $prod->Producto;?></th>
                       <?php endforeach ?>
                       <!--  -->
                       <!-- <th>Precentacion</th> -->
                       <th>Tipo</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->idAlmacen; ?></td>
                        <td><?php echo $m->Nombre; ?></td>
                        <td><?php echo $m->Descripcion; ?></td>
                        <!-- cambiar por valores de la base de datos -->
                        <?php
                            foreach($productos as $prod){
                                $cant = 0;
                                foreach($datos as $d){
                                        if($d->idAlmacen == $m->idAlmacen && $d->idProducto == $prod->idProducto ){
                                            //echo "<td>".$d->Cantidad."</td>";
                                            $cant = $d->Cantidad;
                                            break;
                                        }else{
                                            $ex = 1;    
                                        }
                                }
                                if($cant == 0){echo "<td>0</td><td>0</td>";}else{echo "<td>".$cant."</td>"."<td>".round($cant/12,2)."</td>";}
                            }
                        ?>
                        <!--  -->
                        <td><?php echo $m->Tipo; ?></td>
                        <td style="padding-right:0px">
                            <a href="<?php echo site_url('Inventario/detail/'.$m->idAlmacen) ?>"><button class="btn btn-primary " type="button"><i class="fa fa-paste"></i> Detalle</button></a>
                            <a href="<?php echo site_url('Inventario/crud/'.$m->idAlmacen) ?>"><button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Editar</button></a>
                            <?php
                              if ($a == 1) {
                                $u = site_url('Inventario/eliminar/'.$m->idAlmacen);
                                echo '<a onclick ="return confirm(`Esta seguro de cerrar este Almacen?`)" href="'.$u.'"  ><button  type="button" class="btn btn btn-danger">Cerrar</button></a>';
                              }else{
                                $u = site_url('Inventario/activar/'.$m->idAlmacen);
                                echo '<a onclick ="return confirm(`Esta seguro de Activar este Almacen?`)" href="'.$u.'"  ><button  type="button" class="btn btn btn-success">Activar</button></a>';
                              }
                             ?>
                            
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <tfoot>
                   <tr>
                   <th>IdAlmacen</th>
                       <th>Nombre</th>
                       <th>Direccion</th>
                       <!-- cambiar a dinamico -->
                       <?php foreach ($productos as $prod): ?>
                              <?php
                                $cant = 0;
                                foreach($datos as $d){
                                    if($d->idProducto == $prod->idProducto ){
                                        $cant = $d->Cantidad+$cant;
                                    }
                                }
                               ?>
                            <th><?php echo "Total: ".$cant;?></th>
                            <th><?php echo "Cajas: ".round($cant/12,2); ?></th>
                       <?php endforeach ?>
                       <!--  -->
                       <!-- <th>Precentacion</th> -->
                       <th>Tipo</th>
                       <th>Action</th>
                   </tr>
                   </tfoot>
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
       <script type="text/javascript">
         function confirmacio() {
           confirm('Esta seguro de eliminar este Producto?');
         }
       </script>
