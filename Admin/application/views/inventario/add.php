<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Detalle Materia Prima';
    if(is_object($model)){
        $titulo = 'Actualizar Materia Prima';
    } 
    // var_dump($model);
?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Inventario/mat_prima/'); ?>" >Materia Prima</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-7">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Materia Prima para Almacen</h5>
                       <div class="ibox-tools">
                           
                       </div>
                   </div>
                <div class="ibox-content">

                  <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                     <thead>
                     <tr>
                         <!-- <th>Id Materia</th> -->
                         <th>Materia</th>
                         <th>Descripcion</th>
                         <th>Precentacion</th>
                         <!-- <th>Cantidad</th> -->
                         <th>Unidad / 1</th>
                         <!-- <th>Codigo de Barras</th> -->
                         <th>Action</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php foreach ($model as $m):?>
                       <tr class="gradeX">
                          <!-- <td><?php echo $m->idMat_Prima; ?></td> -->
                          <td><?php echo $m->Mat_Prima; ?></td>
                          <td><?php echo $m->Descripcion; ?></td>
                          <td><?php echo $m->Precentacion; ?> </td>
                          <!-- <td><?php echo $m->Cantidad; ?></td> -->
                          <td><?php echo $m->Unidad; ?></td>
                          <!-- <td><?php echo $m->CodigoBarras; ?></td> -->
                          <td style="padding-right:0px"> 
                              <!-- <a href="<?php //echo site_url('MateriaPrima/crud/'.$m->idMat_Prima) ?>"><button class="btn btn-info " type="button">Agregar <i class="fa fa-chevron-right"></i> </button></a> -->
                              <button id="badd<?php echo $m->idMat_Prima; ?>" onclick="agregar(<?php echo $m->idMat_Prima; ?>,'<?php echo $m->Mat_Prima; ?>','<?php echo $m->Descripcion; ?>','<?php echo $m->Unidad; ?>')" class="btn btn-info " type="button">Agregar <i class="fa fa-chevron-right"></i> </button>
                              <!-- <a onclick ="return confirm('Esta seguro de eliminar este Producto?')" href="<?php echo site_url('MateriaPrima/eliminar/'.$m->idMat_Prima) ?>"  ><button  type="button" class="btn btn-w-m btn-danger">Eliminar</button></a> -->
                          </td>
                      </tr>
                     <?php endforeach; ?>
                     </tbody>
                     </table>
                  </div>

                </div>
               </div>
           </div>
           <div class="col-lg-5">
                 <div class="ibox float-e-margins">
                   <div class="ibox-title">
                    <h5>Materia Prima en la Receta / para 1000 gr (1 KG) de producto</h5>
                    <div class="ibox-content">
                      <!-- <div class="table-responsive"> -->
                        <!-- <form class="form-horizontal" action="<?php echo site_url('Recetas/registrar'); ?>" method="post" > -->
                        <form id="formR" class="form-horizontal" action="<?php echo site_url('Inventario/registrarDetalle'); ?>" method="post"  onsubmit="return empty()" >
                            <div id="detallealmacen" name="detallealmacen">
                              <input type="hidden" id="foo" name="foo" value="0"  required="true" >
                              <input type="hidden" name="id" value="<?php echo $id; ?>">
                            </div>
                            <button type="submit" id = 'm2' class="btn btn-w-m btn-primary" >Registrar Almacen</button>
                        </form>
                      <!-- </div> -->
                    </div>
                   </div>
                  </div>          
           </div>
        </div>
  </div>
        <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript">
          $( document ).ready(function() {
              var data = <?php echo json_encode($detallealmacen);?>;
             data.forEach(function(element) {
                agregar(element['idMat_Prima'],element['Mat_Prima'],element['Descripcion'],element['Unidad'],element['Cantidad'])
              });

          })
        </script>
       <script type="text/javascript">
         
         function confirmacio() {
           confirm('Esta seguro de eliminar esta Materia Prima?');
         }
         
         function agregar($id,$mat,$des,$uni,$value){
             // console.log($id,$mat,$des)
             var $modal = $('#detallealmacen');
                 $modal.append($('<div class="form-group" id="'+$id+'" ><label class="col-lg-4 control-label">'+$mat+' - '+$des+'</label>'+
                              '<div class="col-lg-3"><input name="cantidad['+$id+']" type="number" placeholder="00" class="form-control" required="true" min="0" value="'+$value+'" step="0.0001" >'+
                              '</div><label class="col-lg-2 control-label">'+$uni+'</label><button onclick="quitar('+$id+')" class="btn btn-danger btn-circle"> <i class="fa fa-times" ></i>  </button> '+
                            '</div>'));
             // var $boton = $('#badd'+$id);
             //    $boton.disabled = true;
             //    console.log($boton);
             $su = document.getElementById('foo').value;
             $su = $su + 1;
             $su = document.getElementById('foo').value = $su;
             document.getElementById('badd'+$id).disabled = true;
             var $modal = $("#detallealmacen")
             $modal.find('#alerta').remove()
         }
         function quitar($id) {
           //confirm('Esta seguro de elmiinar este item')
           if(confirm('Esta seguro de elmiinar este item'))
            {
              var $modal = $("#detallealmacen");
              $modal.find('#'+$id).remove();
              document.getElementById('badd'+$id).disabled = false;
              $su = document.getElementById('foo').value;
             $su = $su - 1;
             $su = document.getElementById('foo').value = $su;
            }
         }
         function empty() {
           $foo = document.getElementById('foo').value;
           // console.log($foo)
           if($foo > 0){
              return true
           }else{
             var $modal = $("#detallealmacen")
             $modal.find('#alerta').remove()
             $modal.append($('<div id = "alerta" class="alert alert-danger">'+
                              'Agrega una materia prima.'+
                             '</div>'))
             // console.log('')
            return false
           }
         }
       </script>
