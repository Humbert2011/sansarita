<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Almacen';
    if(is_object($model)){
        $titulo = 'Actualizar Almacen';
    } 
?>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Inventario/listarAlmacenes/'); ?>" >Almacenes</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Almacenes Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                          <!-- <?php echo form_open('Inventario/registrar'); ?> -->
                        
                            <form class="form-horizontal" action="<?php echo site_url('Inventario/registrar'); ?>" method="post">
                                                              <p>Llena todos los campos para agregar un Almacen.</p>
                                                              <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idAlmacen;}  ?>">
                                <div class="form-group"><label class="col-lg-2 control-label">Almacen</label>

                                    <div class="col-lg-10"><input name="Almacen" type="text" placeholder="Almacen" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->Nombre;}  ?>" >
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Descripcion</label>

                                    <div class="col-lg-10"><input name="Descripcion" type="text" placeholder="Descripcion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Descripcion;}  ?>"></div>
                                </div>
                                <!-- <div class="form-group"><label class="col-lg-2 control-label">Tipo</label>

                                    <div class="col-lg-10"><select name="Tipo" id ="Tipo" class="form-control m-b">
                                      <option value="1">Materia Prima</option>
                                      <option value="2">Materia Industrial</option>
                                      <option value="3">Inventario</option>
                                      <option value="4">Producto</option>
                                    </select></div>
                                </div> -->
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Almacen</button>
                                    </div>
                                </div>
                                <!-- <?php form_close(); ?> -->
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    // window.onload(cargarTipo);
                    let tipo = <?php echo $model->Tipo;?>;
                    console.log(tipo)
                    // let cargarTipo  = () => {
                        document.getElementById('Tipo').value = tipo;
                    // }

                </script>
                <!-- <script src="<?php echo base_url('js/ajax.js'); ?>" type="text/javascript" ></script> -->