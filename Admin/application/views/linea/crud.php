<?php 
    if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Linea';
    $crud = 0;
    if(is_object($model)){
        $titulo = 'Actualizar Linea';
        $crud = 1;
    }
    // var_dump($presentacion);
    // var_dump($producto);
?>
<style>
input[type="number"]::-webkit-outer-spin-button, 
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}
</style>

    <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                 
                <h2><?php echo $titulo ?></h2>
                
                <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Linea/index/'); ?>" >Linea</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                </ol>
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta de Linea Sansarita</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                          <!-- <?php echo form_open('Linea/registrar'); ?> -->
                        
                            <form class="form-horizontal" action="<?php echo site_url('Linea/registrar'); ?>" method="post">
                                                              <p>Llena todos los campos para agregar un Linea.</p>
                                                              <input type="hidden" name ="idLinea" value ="<?php  if(is_object($model)){echo $model->idLinea;}  ?>">
                                <div class="form-group"><label class="col-lg-2 control-label">Producto</label>

                                    <div class="col-lg-10">
                                        <select name="idProducto" id="idProducto" class="form-control">
                                            <?php foreach ($receta as $rec ):?>
                                            <option value="<?php echo $rec->idProducto ?>" > <?php echo $rec->Producto ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- <input type="hidden" name ="idLinea" value ="<?php  if(is_object($model)){echo $model->idLinea;}  ?>"> -->
                                <div class="form-group"><label class="col-lg-2 control-label">Presentacion</label>

                                    <div class="col-lg-10">
                                    <select name="idPresentacion" id="idPresentacion" class="form-control">
                                            <?php foreach ($presentacion as $pres ):?>
                                            <option value="<?php echo $pres->idPresentacion ?>" > <?php echo $pres->Descripcion ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Descripcion</label>

                                    <div class="col-lg-10"><input name="Descripcion" type="text" placeholder="Descripcion" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->Descripcion;}  ?>"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Contenido Neto en gr</label>
                                    <div class="col-lg-10"><input onkeyup="constoProduccion()" id="ContenidoNeto" name="ContenidoNeto" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->ContenidoNeto;}else{echo '0';}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Precio de produccion</label>
                                    <div class="col-lg-10"><input step="0.001" name="PrecioProd" id="PrecioProd" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->PrecioProd;}else{echo '0';}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Precio al publico</label>
                                    <div class="col-lg-10"><input name="PrecioPublico" type="number" placeholder="00" class="form-control" required="true" value = "<?php  if(is_object($model)){echo $model->PrecioPublico;}else{echo '0';}  ?>" ></div>                                    
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">Codigo de Barras</label>

                                    <div class="col-lg-10"><input name="CodigoBarras" type="text" placeholder="Codigo de barras" class="form-control" required="true" value="<?php  if(is_object($model)){echo $model->CodigoBarras;}  ?>"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button  class="btn btn-w-m btn-primary" type="submit">Registrar Linea</button>
                                        <!-- <?php if(is_object($model)): ?><a href = " <?php echo site_url('Recetas/crud/'.$model->idLinea); ?>" <?php if (!is_object($model)){ echo "disabled"; } ?>  class="btn btn-w-m btn-warning">Alta de Receta</a><?php endif;?> -->
                                    </div>
                                </div>
                                <!-- <?php form_close(); ?> -->
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    // let cal = () => {
                    //     let cadena = document.getElementById('PrecioPublico').value
                    //     let precioDis = cadena * 0.80;
                    //     console.log(precioDis);
                    //     document.getElementById('PrecioDistribuidor').value = precioDis
                    // }
                    // window.onload = iniCrud();
                    // function iniCrud (){
                    //     let trueCrud = <?php echo $crud; ?>;
                    //     console.log(trueCrud);
                    //     if (trueCrud === 1) {
                    //         alert('Puede dar de alta o editar una receta de este Linea ')
                    //     }
                    // }


                    let presentacion = <?php echo json_encode($presentacion)?>;
                    let receta = <?php echo json_encode($receta)?>;
                    console.log(presentacion,receta)
                    let constoProduccion = () => {
                        let pesoGramos = document.getElementById('ContenidoNeto').value
                        let idPresentacion = document.getElementById('idPresentacion').value
                        let idProducto = document.getElementById('idProducto').value
                        if(pesoGramos != 0 && pesoGramos != null){
                            // console.log(pesoGramos);

                            let costo_presentacion = presentacion.find(p => p.idPresentacion == idPresentacion).costo
                            let costo_producto = receta.find(rec=>rec.idProducto == idProducto).costo
                            let costo_produccion = (pesoGramos*costo_producto)+parseFloat(costo_presentacion)

                            console.log(costo_produccion)
                            document.getElementById('PrecioProd').value = parseFloat(costo_produccion).toFixed(3)
                            // console.log(costo_presentacio);
                        }
                        
                    }

                </script>