<?php 
  if(isset($error)){
        var_dump($error);
    }
    $titulo = 'Agregar Personal';
    if(is_object($model)){
        $titulo = 'Actualizar Personal';
        // echo "Nombre= ".$model->Nombre;
        echo("  Tipo Persona= " .$model->idtipoPersona);
        echo("  Area Persona= " .$model->idArea);
    }   
 ?>

<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2><?php echo $titulo ?></h2>

                   <ol class="breadcrumb">
                       <li>
                           <a href="<?php echo site_url('Inicio/index/'); ?>">Inicio</a>
                       </li>
                       <li>
                           <a href="<?php echo site_url('Personal/administrador/'); ?>" >Personal</a>
                       </li>
                       <li class="active">
                           <strong><?php echo $titulo ?></strong>
                       </li>
                  </ol>                   
               </div>
               <div class="col-lg-2">

               </div>
    </div>
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5><?= $titulo ?></h5>
              <div class="ibox-tools">                  
              </div>
          </div>
          <div class="ibox-content">
              <form class="form-horizontal" action="<?= site_url('Personal/registrar')?>" method="POST" enctype="multipart/form-data">
                  <p>Llena todos los campos para <?php echo $titulo; ?></p>
                  <input type="hidden" name ="id" value ="<?php  if(is_object($model)){echo $model->idPersona;}  ?>" required>
                  
                  <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>

                      <div class="col-lg-10"><input name="Nombre" type="text" placeholder="Nombre" class="form-control" value = "<?php  if(is_object($model)){echo $model->Nombre;}  ?>" required>
                      </div>
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>

                      <div class="col-lg-10"><input name="Apellidos" type="text" placeholder="Apellidos" class="form-control" value = "<?php  if(is_object($model)){echo $model->Apellidos;}  ?>" required></div>
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Telefono</label>
                      <div class="col-lg-10"><input name="Telefono" type="number" placeholder="Telefono" class="form-control" value = "<?php  if(is_object($model)){echo $model->Telefono;}  ?>" required></div>                                    
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                      <div class="col-lg-10"><input name="Email" type="email" placeholder="Email" class="form-control" value = "<?php  if(is_object($model)){echo $model->Email;}  ?>" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required ></div>                                    
                  </div>
                  <?php if (!is_object($model)): ?>
                    <div class="form-group"><label class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10"><input name="Password" type="password" placeholder="Password" class="form-control" required></div>                                    
                    </div>
                  <?php endif ?>                 
                  <div class="form-group"><label class="col-lg-2 control-label">Foto</label>
                      <div class="col-lg-10"><input name="img" type="file" class="form-control" accept="image/png, image/jpeg, image/jpg" value = "<?php  if(is_object($model)){echo $model->Foto;}  ?>"  required></div>                                    
                  </div>                                              
                  <div class="form-group"><label class="col-lg-2 control-label">Grado de Estudio</label>
                      <div class="col-lg-10"><input name="GradoEstudios" type="text" placeholder="Grado de Estudio" class="form-control" value = "<?php  if(is_object($model)){echo $model->Grado_estudios;}  ?>" required ></div>                                    
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Cargo</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="Cargo" required>
                          <!-- consumir servicio para listar -->

                          <?php foreach ($cargos as $listacargos): ?>

                            <option value="<?= $listacargos->idtipoPersona?>" <?php if(is_object($model) && $model->idtipoPersona == $listacargos->idtipoPersona) echo "selected"?>><?= $listacargos->Descripcion ?></option>
                            
                          <?php endforeach ?>
                            
                        </select>
                      </div>                                    
                  </div>
                  <div class="form-group"><label class="col-lg-2 control-label">Area</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="Area" required>

                          <?php foreach ($areas as $listareas): ?>

                            <option value="<?= $listareas->idArea?>" <?php if(is_object($model) && $model->idArea == $listareas->idArea) echo "selected" ?>><?= $listareas->Descripcion ?></option>
                            
                          <?php endforeach ?>
                           
                        </select>
                      </div>                                    
                  </div>
                  <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                          <button onclik="" class="btn btn-sm btn-primary" type="submit"><?= $titulo?></button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
