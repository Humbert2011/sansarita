<!-- <?php var_dump($model); ?> -->
<div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                   <h2>Personal</h2>
                   <!-- <ol class="breadcrumb">
                       <li>
                           <a href="index.html">Home</a>
                       </li>
                       <li>
                           <a>Tables</a>
                       </li>
                       <li class="active">
                           <strong>Data Tables</strong>
                       </li>
                   </ol> -->
               </div>
               <div class="col-lg-2">

               </div>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
           <div class="row">
               <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Lista de Personal</h5>
                       <div class="ibox-tools">                          
                            <a href="<?php echo site_url('Personal/crud'); ?>"><button class="btn btn-success " type="button"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button></a>                           
                       </div>
                   </div>
                   <div class="ibox-content">

                       <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover dataTables-example" >
                   <thead>
                   <tr>
                       <th>Usuario</th>
                       <th>Telefono</th>
                       <th>Email</th>
                       <th>Cargo</th>
                       <th>Area</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   <?php foreach ($model as $m):?>
                     <tr class="gradeX">
                        <td><?php echo $m->Nombre; ?> <?php echo $m->Apellidos; ?></td>
                        <td><?php echo $m->Telefono; ?></td>
                        <td><?php echo $m->Email; ?></td>                       
                        <td>
                          <?php 
                            if($m->idtipoPersona == 1){
                              echo "Administrador";
                            }
                            if($m->idtipoPersona == 2){
                              echo "Vendedor";
                            } 
                            if($m->idtipoPersona == 3){
                              echo "Cosinero";
                            }
                          ?> 
                        </td>
                        <td>
                          <?php 
                           if($m->idArea == 1){
                            echo "Administracion";
                           }
                           if($m->idArea == 2){
                            echo "Ventas";
                           } 
                           if($m->idArea == 3){
                            echo "Produccion";
                           }  
                          ?>
                            
                          </td>
                        <td style="padding-right:0px"> 
                            <a href="<?php echo site_url('Personal/crud/'.$m->idPersona); ?>">
                              <button class="btn btn-info " type="button">
                                <i class="fa fa-paste"></i> Editar
                              </button>
                            </a>
                            <a href="<?= site_url('Personal/eliminar/'.$m->idPersona);?>"><button type="button" class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr>
                   <?php endforeach; ?>
                   </tbody>
                   <!-- <tfoot>
                   <tr>
                       <th>Rendering engine</th>
                       <th>Browser</th>
                       <th>Platform(s)</th>
                       <th>Engine version</th>
                       <th>CSS grade</th>
                   </tr>
                   </tfoot> -->
                   </table>
                       </div>

                   </div>
               </div>
           </div>
           </div>
       </div>
