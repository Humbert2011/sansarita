<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        //'test'     => new App\Model\TestModel($c->db),
        'persona' => new App\Model\PersonaModel($c->db),
        'auth' => new App\Model\AuthModel($c->db),
        'producto' => new App\Model\ProductoModel($c->db),
        'personal' => new App\Model\PersonalModel($c->db),
        'tipopersona' => new App\Model\TipoPersonaModel($c->db),
        'area' => new App\Model\AreaModel($c->db),
        'materiaprima'=> new App\Model\MateriaprimaModel($c->db),
        'receta' => new App\Model\RecetaModel($c->db),
        'proveedor' => new App\Model\ProveedorModel($c->db),
        'produccion' => new App\Model\ProduccionModel($c->db),
        'almacenes' => new App\Model\AlmacenesModel($c->db),
        'detallealmacen' => new App\Model\DetallealmacenModel($c->db),
        'img' => new App\Model\ImgModel($c->db),
        'loteproduccion' => new App\Model\LoteproduccionModel($c->db),
        'detalleproduccion' => new App\Model\DetalleproduccionModel($c->db),
        'gasto' => new App\Model\GastoModel($c->db),
        'venta' => new App\Model\VentaModel($c->db),
        'pedidos' => new App\Model\PedidosModel($c->db),
        'detalleventa' => new App\Model\DetalleventaModel($c->db),
        'cliente' => new App\Model\ClienteModel($c->db),
        'detallepedidos' => new App\Model\DetallepedidosModel($c->db),
        'transaccion' => new App\Model\TransaccionModel($c->db),
        'cuentascobrar' => new App\Model\CuentascobrarModel($c->db),
        'materiaindustrial' => new App\Model\MateriaindustrialModel($c->db),
        'presentacion' => new App\Model\PresentacionModel($c->db),
        'detallepresentacion' => new App\Model\DetallepresentacionModel($c->db),
        'linea' => new App\Model\LineaModel($c->db),
        'compraproducto' => new App\Model\CompraproductoModel($c->db),
        'compra' => new App\Model\CompraModel($c->db),
        'tipogasto' => new App\Model\TipogastoModel($c->db),
        'inventario' => new App\Model\inventarioModel($c->db)
    ];
};
