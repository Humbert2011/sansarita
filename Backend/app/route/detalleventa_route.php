<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\VentaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/detalleventa/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detalleventa->listar())
                   );
    });

    $this->get('detalleVenta/{var}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->detalleventa->detalleVenta($args['var']))
                 );
    });


    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detalleventa->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      // $r = almacenesValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detalleventa->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = almacenesValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detalleventa->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detalleventa->eliminar($args['id']))
                 );
    });
})->add(new AuthMiddleware($app));
