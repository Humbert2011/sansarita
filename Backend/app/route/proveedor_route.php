<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\personaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/proveedor/', function () {

    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->proveedor->listar())
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->proveedor->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {      

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->proveedor->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->proveedor->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->proveedor->eliminar($args['id']))
                 );
    });
});#->add(new AuthMiddleware($app));
