<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\lineaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/linea/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->linea->listar())
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->linea->obtener($args['id']))
                 );
    });


    $this->post('registrar', function ($req, $res, $args) {
      // $r = ventaValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->linea->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = ventaValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->linea->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->linea->eliminar($args['id']))
                 );
    });
})->add(new AuthMiddleware($app));
