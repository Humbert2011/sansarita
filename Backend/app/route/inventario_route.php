<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\inventarioValidation,
    App\Middleware\AuthMiddleware;

$app->group('/inventario/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->inventario->listar())
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->inventario->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      // $r = materiaprimaValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->inventario->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = materiaprimaValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->inventario->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->inventario->eliminar($args['id']))
                 );
    });
})->add(new AuthMiddleware($app));
