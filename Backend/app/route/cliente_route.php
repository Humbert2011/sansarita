<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\ClienteValidation,
    App\Middleware\AuthMiddleware;

$app->group('/cliente/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->cliente->listar())
                   );
    });
    $this->get('listarPersonal', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->cliente->listarPersonal())
                 );
    });
    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cliente->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      // $r = clienteValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cliente->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = clienteValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cliente->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cliente->eliminar($args['id']))
                 );
    });
})->add(new AuthMiddleware($app));
