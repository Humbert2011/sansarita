<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\AlmacenesValidation,
    App\Middleware\AuthMiddleware;

$app->group('/detallealmacen/', function () {
    $this->get('listarMateriaP', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallealmacen->listarMateriaP())
                   );
    });

        $this->get('listarMateriaInd', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallealmacen->listarMateriaInd())
                   );
    });

        $this->get('listarLinea', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallealmacen->listarLinea())
                   );
    });

        $this->get('listarInventario', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallealmacen->listarInventario())
                   );
    });

    $this->get('listarProd/{var}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallealmacen->listarProd($args['var']))
                   );
    });
    
    $this->get('listarNoEx/{var}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->detallealmacen->listarNoEx($args['var']))
                 );
    });
  
    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallealmacen->obtener($args['id']))
                 );
    });

    $this->post('registrarDetalle', function ($req, $res, $args) {
      // $r = almacenesValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallealmacen->registrarDetalle($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = almacenesValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallealmacen->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallealmacen->eliminar($args['id']))
                 );
    });
})->add(new AuthMiddleware($app));
