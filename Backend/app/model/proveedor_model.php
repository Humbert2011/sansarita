<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class ProveedorModel
{
    private $db;
    private $table = 'proveedor';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)                        
                         ->fetchAll();//para mas de un registro      
        $this->response->result =  $data;
        return $this->response->SetResponse(true);   
    }

    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idProveedor', $id)
                    ->fetch();//para un solo dato o linea
      $this->response->result =  $data;
      return $this->response->SetResponse(true);   

    }

/*
    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);
        // $fecha = date('Y-m-d H:i:s');
        // array_push($data, "Fecha_Alta",date('Y-m-d H:i:s'));
        //Foto de perfil
        $img_name = $data['Foto']['name'];
        $carpeta_destino = $_SERVER['DOCUMENT_ROOT'].'/sansarita/Backend/img/';     

        move_uploaded_file($data['Foto']['tmp_name'], $carpeta_destino.$img_name);
        $data['Foto'] = $carpeta_destino.$img_name;
        $insertarPersona = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarPersona;
        return $this->response->SetResponse(true);
    }
*/
      public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }
/*

    public function actualizar($data,$id)
    {

      if (isset($data['Password'])) {
        $data['Password'] = Cifrado::Sha512($data['Password']);
      }

      $img_name = basename($data['Foto']['name']);
      $tmp_name = $data['Foto']['tmp_name'];
      $carpeta_destino = "C:/AppServ/www/sansarita/Backend/img/";         
     
      // $t = $data['Foto']['tmp_name'];      
      try{
        move_uploaded_file($tmp_name,$carpeta_destino.$img_name);      
        $data['Foto'] = $carpeta_destino.$img_name;
        $this->db->update($this->table, $data)
                 ->where('idPersona',$id)
                 ->execute();
               $this->response->result=$carpeta_destino.$img_name;
        return $this->response->SetResponse(true);
      }
      catch (Exception $e){           
        $this->response->result=$e;
        return $this->response->SetResponse(false);
      }
    }
*/

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $actualizar = $this->db->update($this->table, $data)
                ->where('idProveedor',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idProveedor',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
