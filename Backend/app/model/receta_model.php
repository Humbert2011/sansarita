<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class RecetaModel
{
    private $db;
    private $table = 'receta';
    private $tableSec = 'producto';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    public function listar()
    {
        $data = $this->db->from($this->tableSec)
                         ->select('producto.idProducto,producto,producto.Descripcion,IFNULL(receta.idProducto,0) AS Receta, sum( materia.Precio * receta.Cantidad)/1000 as costo')
                         ->leftJoin('receta ON producto.idProducto = receta.idProducto')
                         ->leftJoin('mat_prima as materia on materia.idMat_prima = receta.idMat_prima')
                         ->orderBy('idProducto DESC')
                         ->groupBy('idProducto')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
    public function obtener($id)
    {
       $data = $this->db->from($this->table)
                    ->select(null)
                    ->select('idReceta,receta.Cantidad,receta.idMat_Prima,receta.idProducto,mat_prima.Mat_Prima,mat_prima.Descripcion,mat_prima.Unidad,producto.Producto,mat_prima.Precio')
                    ->where('receta.idProducto',$id)
                    ->leftJoin('mat_prima ON receta.idMat_Prima = mat_prima.idMat_Prima')
                    ->leftJoin('producto ON receta.idProducto = producto.idProducto')
                    ->fetchAll();//para un solo dato o linea
        return [
            'data'  => $data
        ];
    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);
        $insertarReceta = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarReceta;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }
        $this->db->update($this->table, $data)
                ->where('idReceta',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table)
                             ->where('idProducto',$id)
                             ->execute();
               $this->response->result =  $eliminar;
        return $this->response->SetResponse(true);
    }
}
