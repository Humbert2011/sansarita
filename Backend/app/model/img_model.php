<?php
namespace App\Model;

use App\Lib\Response;

class ImgModel
{
	private $db;
   #	private $table;
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar imagenes
    public function listar($l, $p , $user )
    {
        $data = $this->db->from($this->table)
                         ->where('IdSolicitud',$user)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
     #obtener imagenes
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La direccion no se encuentra';
            return $this->response->SetResponse(false);
         }

    }
    #cargar
    public function cargar($file,$a,$id)
    {
		
		#variables de subida ruta general
    	
    	$dir_subida = '/var/www/html/hooli-backend/img/';#ruta';#ruta de subida local #;linux
        $dir_server = 'http://localhost:8080/sansarita/Backend/img/';#linux
        // $dir_server = 'http://admin.sansarita.com/Backend/img/';#server
        // $dir_subida ='/home/stardust007/public_html/admin/Backend/img/';#ruta de subida en linea
        
		$fichero_subido = $dir_subida . basename($file['name']);
        $NombreArchivo = $dir_server . basename($file['name']);
    	#asignacion de tabla
		switch ($a) {
		    case "persona":
		        $a = 'persona';
		        $data = array("Foto" => $NombreArchivo);
		        break;
		    case "prod":
		        // $a = 'producto';
		        // $data = array('IdDetalleServicio' => $id,'FechaDeCarga' => $fecha,'NombreArchivo' => $NombreArchivo);
		        break;
		    case "mat_prima":
		        // $a = 'mat_prima';
		        // $data = array('FotoDetalleServicio_Id' => $id,'FechaDeCarga' => $fecha,'NombreArchivo' => $NombreArchivo);
		        break;    
		    }#tipoServicio
		try{
		    move_uploaded_file($file['tmp_name'], $fichero_subido);
			if($a === 'persona'){
                  $actualizar= $this->db->update($a, $data)
                                        ->where('idPersona', $id)
                         	 	        ->execute();
	              $this->response->result = $actualizar;
	              return $this->response->SetResponse(true,'Se subio la foto de perfil del usuario');
			}else{
				$insertarImg = $this->db->insertInto($a, $data)
	                 			->execute();
			    $this->response->result = $insertarImg;
            	return $this->response->SetResponse(true,'El fichero es válido y se subió con éxito');
            }
         } catch (Exception $e) {
            #echo ;
            $this->response->errors[]='Error encontrado: '.  $e->getMessage(). "\n";
            return $this->response->SetResponse(false);
         }	
		// $this->response->result = $data;
         // return $this->response->SetResponse(true,'El fichero es válido y se subió con éxito.\n');
    }

}	