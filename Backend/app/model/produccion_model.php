<?php
namespace App\Model;

use App\Lib\Response;

class ProduccionModel
{
    private $db;
    private $table = 'produccion';
  //private $tableLote = 'lote_produccion' ;
  //private $tableMatPrima = 'mat_prima';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         ->orderBy('idProduccion DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idProduccion',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idProduccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idProduccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
