<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class PersonalModel
{
    private $db;
    private $table = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($tipo)
    {
        $data = $this->db->from($this->table)
                         ->where('idtipoPersona', $tipo)
                         // ->limit($l)
                         // ->offset($p)
                         // ->orderBy('id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

    public function obtener($id)
    {
      return $this->db->from($this->table,$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarPersona = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarPersona;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      if (isset($data['Password'])) {
        $data['Password'] = Cifrado::Sha512($data['Password']);
      }

        $this->db->update($this->table, $data, $id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        //$data['Password'] = Cifrado::Sha512($data['Password']);

        $this->db->deleteFrom($this->table,$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
