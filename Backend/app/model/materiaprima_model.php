<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class MateriaprimaModel
{
    private $db;
    private $table = 'mat_prima';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         ->orderBy('idMat_Prima DESC')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
    
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idMat_Prima',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $actualizar = $this->db->update($this->table, $data)
                ->where('idMat_Prima',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table)
                 ->where('idMat_Prima',$id)
                 ->execute();
                 $this->response->result =  $eliminar;
        return $this->response->SetResponse(true);
    }
}
