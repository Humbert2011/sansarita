<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class GastoModel
{
    private $db;
    private $table = 'gasto';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($de,$a)
    {
        $data = $this->db->from($this->table)
                         ->select('tipo_gasto.Tipo_Gasto')
                         // ->limit($l)
                         // ->offset($p)
                         ->where("Fecha BETWEEN :de AND :a",
                            array(':de' => $de , ':a' => $a )
                            )
                         ->orderBy('idGasto DESC')
                         ->leftJoin('tipo_gasto ON tipo_gasto.idTipo_Gasto = gasto.idTipo_Gasto')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
        ];
    }
    
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idGasto',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $actualizar = $this->db->update($this->table, $data)
                ->where('idGasto',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table)
                 ->where('idGasto',$id)
                 ->execute();
                 $this->response->result =  $eliminar;
        return $this->response->SetResponse(true);
    }
}
