<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class ClienteModel
{
    private $db;
    private $table = 'cliente';
    private $table2 = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select(null)
                         ->select("`idCliente`,cliente.Codigo,`Cliente`,`Nombre_Comercial`,`RFC`,cliente.Telefono,`Correo`,cliente.Activo,`idPersona1`, concat(persona.Nombre,' ',persona.Apellidos) as NombreCompleto,`idPersona2`,`Personal`,concat(personal.Nombre,' ',personal.Apellidos) as nomPersonal,cliente.Margen")
                         ->where('cliente.Activo',1)
                         ->orderBy('idCliente DESC')
                         ->leftJoin('persona on persona.idPersona = cliente.idPersona1')
                         ->leftJoin('persona as personal on personal.idPersona = cliente.Personal')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
        ];
    }
    public function listarPersonal()
    {
        $data = $this->db->from($this->table2)
                         ->where('Activo',1)
                         ->where('idtipoPersona = 1 OR idtipoPersona = 2')
                         ->orderBy('idPersona DESC')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
        ];
    }
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->select(null)
                    ->select("`idCliente`,cliente.Codigo,`Cliente`,`Nombre_Comercial`,`RFC`,cliente.Telefono,`Correo`,cliente.Activo,`idPersona1`, concat(persona.Nombre,' ',persona.Apellidos) as nomContacto1,`idPersona2`,concat(persona2.Nombre,' ',persona2.Apellidos) as nomContacto2,`Personal`,concat(personal.Nombre,' ',personal.Apellidos) as nomPersonal,cliente.Direccion,cliente.Margen,cliente.CodigoPostal")
                    ->where('idCliente',$id)
                    ->leftJoin('persona on persona.idPersona = cliente.idPersona1')
                    ->leftJoin('persona as persona2 on persona2.idPersona = cliente.idPersona2')
                    ->leftJoin('persona as personal on personal.idPersona = cliente.Personal')
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertClient = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertClient;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $actualizar = $this->db->update($this->table, $data)
                ->where('idCliente',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $data = ['Activo'=> 2];
        $actualizar = $this->db->update($this->table, $data)
                ->where('idCliente',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }
}
