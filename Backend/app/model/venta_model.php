<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class VentaModel
{
    private $db;
    private $table = 'venta';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }


 public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select('cliente.Cliente')
                         ->select('pedidos.Fecha_Pedido')
                         ->select('pedidos.Total')
                       //  ->select('transaccion.Monto_Pago')
                         // ->limit($l)
                         // ->offset($p)
                          ->orderBy('idVenta DESC')
                         ->leftJoin('cliente ON cliente.idCliente = venta.idCliente')
                         ->leftJoin('pedidos ON pedidos.idPedidos= venta.idPedidos')
                        // ->leftJoin('transaccion ON transaccion.idTransaccion = venta.idTransaccion')
                         ->fetchAll();//para mas de un registro
//
        // $total = $this->db->from($this->table) 
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

//public function detalleVenta($id = 0)
//{
  //$data = $this->db->from($this->table)
                      //->select(null)
                      //->select('venta.idVenta,venta.Fecha,venta.Descripcion,venta.Total_Pago,venta.idPersona,venta.idCliente')
                      //->fetchAll();
                      
                      //$output = array();
                      //foreach($data as $d){
                        //if($id != $d->idVenta){
                        //$output[] = $d;
                  //}
               //}
                 //return [
                     //'data' => $output
                      //];
    //}

    public function obtener($id)
    {
   $data = $this->db->from($this->table)
                    //->select(null)
                    //->select('idVenta,venta.idVenta,venta.Fecha,venta.Descripcion,venta.Total_Pago,venta.idPersona,venta.idCliente')
                    ->where('idVenta',$id)
                    //->leftJoin('cliente ON pedidos.idPedidos = pedidos.idPedidos')
                    //->leftJoin('mat_prima on detalle_almacen.idMat_Prima = mat_prima.idMat_Prima')
                    //->leftJoin('almacenes ON detalle_almacen.idAlmacen = almacenes.idAlmacen')
                    ->fetch();

                    //return [
                        //'data'  => $data
                        // 'total' => $total
                    //];
    }



    public function registrar($data)
    {
        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idVenta',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idVenta',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
