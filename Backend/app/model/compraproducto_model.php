<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class CompraProductoModel
{
    private $db;
    private $table = 'compra_producto';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)

                         ->where('idCompra_Producto')
                         ->orderBy('idCompra_Producto DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

        public function compraproducto($id)
    {
        $data = $this->db->from($this->table)
                    ->where('idCompra_Producto',$id)
                    ->fetch();//para un solo dato o compra


        return [
            'data' =>$data
        ];
    }

    public function obtener($id)
    {
        // detallar
      return $this->db->from($this->table)
                    ->where('idCompra_Producto',$id)
                    ->fetch();//para un solo dato o linea
    }

    public function registrar($data)
    {
        $insertarCompra = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarCompra;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idCompra_Producto',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idCompra_Producto',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
