<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class TipoPersonaModel
{
    private $db;
    private $table = 'tipopersona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
      $data = $this->db->from($this->table)                         
                         ->fetchAll();        
      $this->response->result =  $data;
      return $this->response->SetResponse(true);   
    }
}
