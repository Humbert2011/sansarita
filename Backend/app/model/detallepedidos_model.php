<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class DetallepedidosModel
{
    private $db;
    private $table = 'detalle_pedidos';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         ->orderBy('iddetalle_pedidos DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

        public function detallePedidos($id)
    {
        $data = $this->db->from($this->table)
                ->select(null)
                ->select("`iddetalle_pedidos`,detalle_pedidos.`idProducto`,`Cantidad`,`Descuento`,`Monto`,`idPedido`,producto.Producto,producto.PrecioPublico")
                ->leftJoin('producto on producto.idProducto = detalle_pedidos.idProducto')
                ->where('idPedido',$id)
                ->fetchAll();

        return [
            'data' =>$data
        ];
    }

    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('iddetalle_pedidos',$id)
                    ->fetch();//para un solo dato o linea
                    return [
                        'data' =>$data
                           ];
    }

    public function registrar($data)
    {
        $insertarPedidos = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarPedidos;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('iddetalle_pedidos',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('iddetalle_pedidos',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
