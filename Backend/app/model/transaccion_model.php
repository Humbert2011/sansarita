<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class TransaccionModel
{
    private $db;
    private $table = 'transaccion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

 
    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select('cliente.Cliente')
                         ->select('Fecha_Pedido')
                         // ->limit($l)
                         // ->offset($p)
                         ->leftJoin('cliente ON cliente.idCliente = transaccion.idCliente')
                         ->leftJoin('pedidos ON pedidos.idPedidos = transaccion.idPedidos')
                         ->orderBy('idTransaccion DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function transaccionlista($id)
    {
        $data = $this->db->from($this->table)
         ->where('idTransaccion',$id)
                //->select(null)
                //->select('detalle_pedidos.iddetalle_pedidos, detalle_pedidos.Producto, detalle_pedidos.Cantidad, detalle_pedidos.Monto, detalle_pedidos.iddetalle_pedidos')
                //->leftJoin('pedidos ON detalle_pedidos.idPedido = pedidos.idPedidos')
                ->fetch();

        return [
            'data' =>$data
               ];
    }



    public function obtener($id)
    {
       return $this->db->from($this->table)
                    ->where('idTransaccion',$id)
                    ->fetch();//para un solo dato o linea
                    //->select(null)
                    //->select('idPedidos,pedidos.idPedidos,cliente.Cliente,pedidos.Fecha_Pedido,pedidos.Monto')
                    //->where('pedidos.idPedidos',$id)
                    //->leftJoin('cliente ON pedidos.idPedidos = pedidos.idPedidos')
                    //->leftJoin('mat_prima on detalle_almacen.idMat_Prima = mat_prima.idMat_Prima')
                    //->leftJoin('almacenes ON detalle_almacen.idAlmacen = almacenes.idAlmacen')
                   // ->fetchAll();

                   // return [
                     //   'data'  => $data
                        // 'total' => $total
                    //];
    }

    public function registrar($data)
    {
        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idTransaccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idTransaccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}