<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class DetalleventaModel
{
    private $db;
    private $table = 'detalle_venta';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         ->orderBy('idDetalle_venta DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function detalleVenta($id)
    {
        $data = $this->db->from($this->table)
         ->where('idDetalle_venta',$id)
                //->select(null)
                //->select('detalle_pedidos.iddetalle_pedidos, detalle_pedidos.Producto, detalle_pedidos.Cantidad, detalle_pedidos.Monto, detalle_pedidos.iddetalle_pedidos')
                //->leftJoin('pedidos ON detalle_pedidos.idPedido = pedidos.idPedidos')
                ->fetch();

        return [
            'data' =>$data
               ];
    }

    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idDetalle_venta',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idDetalle_venta',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idDetalle_venta',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
