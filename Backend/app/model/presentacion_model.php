<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class PresentacionModel
{
    private $db;
    private $table = 'presentacion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select('presentacion.idPresentacion,Presentacion,presentacion.Descripcion,IFNULL(construccion.idPresentacion,0) AS Construccion,sum( materia.Precio * construccion.Cantidad) as costo')
                         ->leftJoin('detalle_presentacion as construccion ON presentacion.idPresentacion = construccion.idPresentacion')
                         ->leftJoin('mat_ind as materia on materia.idMat_ind = construccion.idMat_ind')
                         ->orderBy('idPresentacion DESC')
                         ->groupBy('idPresentacion')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
    
    public function obtener($id)
    {
        $data = $this->db->from($this->table)
                        ->select(null)
                        // ->select('iddetallePresentacion,detalle_presentacion.Cantidad,detalle_presentacion.idMat_ind,detalle_presentacion.idPresentacion,mat_ind.Mat_ind,mat_ind.Descripcion,mat_ind.Unidad,presentacion.Presentacion,mat_ind.Precio')
                        ->select('iddetallePresentacion,detalle_presentacion.Cantidad,presentacion.presentacion,
                        detalle_presentacion.idMat_ind,presentacion.idPresentacion, 
                        mat_ind.mat_ind,mat_ind.Unidad,mat_ind.Precio,mat_ind.Descripcion,
                        presentacion.presentacion')
                        ->leftJoin('detalle_presentacion on detalle_presentacion.idPresentacion = presentacion.idPresentacion')
                        ->leftJoin('mat_ind on detalle_presentacion.idMat_ind = mat_ind.idMat_ind')
                        ->where('presentacion.idPresentacion',$id)
                        ->fetchAll();//para un solo dato o linea

        $inf = $this->db->from($this->table)
                         ->where('idPresentacion',$id)
                         ->fetch();

        return [
            'data'  => $data,
            'inf' => $inf
        ];
    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $actualizar = $this->db->update($this->table, $data)
                ->where('idPresentacion',$id)
                 ->execute();
               $this->response->result =  $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table)
                 ->where('idPresentacion',$id)
                 ->execute();
                 $this->response->result =  $eliminar;
        return $this->response->SetResponse(true);
    }
}
