<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class DetallealmacenModel
{
    private $db;
    private $table = 'detalle_almacen';
    private $tableSecu = 'mat_prima';
    private $tableSecun = 'mat_ind';
    private $tableSecund = 'linea';
    private $tableSecunda = 'inventario';

    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listarMateriaP()
      {
        $data = $this->db->from($this->tableSecu)
                         ->select('almacenes.Nombre,mat_prima.idMat_Prima,mat_prima.Descripcion,IFNULL(detalle_almacen.idMat_Prima,0) AS Materia, sum( materia.Precio * detalle_almacen.idMat_Prima)/1000 as costo,detalle_almacen.Cantidad')
                         ->leftJoin('detalle_almacen ON mat_prima.idMat_Prima = detalle_almacen.idMat_Prima')
                         ->leftJoin('mat_prima as materia on materia.idMat_prima = detalle_almacen.idMat_prima')
                         ->leftJoin('almacenes ON almacenes.idAlmacen = detalle_almacen.idAlmacen')
                         ->orderBy('idMat_Prima ASC')
                         ->groupBy('idMat_Prima')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
      public function listarMateriaInd()
      {
        $data = $this->db->from($this->tableSecun)
                         ->select('almacenes.Nombre,mat_ind.idMat_ind,mat_ind.Descripcion,IFNULL(detalle_almacen.idMat_ind,0) AS MateriaInd, sum( materiaInd.Precio * detalle_almacen.idMat_ind)/1000 as costo,detalle_almacen.Cantidad')
                         ->leftJoin('detalle_almacen ON mat_ind.idMat_ind = detalle_almacen.idMat_ind')
                         ->leftJoin('mat_ind as materiaInd on materiaInd.idMat_ind = detalle_almacen.idMat_ind')
                         ->leftJoin('almacenes ON almacenes.idAlmacen = detalle_almacen.idAlmacen')
                         ->orderBy('idMat_ind ASC')
                         ->groupBy('idMat_ind')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function listarLinea()
      {
        $data = $this->db->from($this->tableSecund)
                         ->select('almacenes.Nombre,linea.idLinea,linea.Descripcion,IFNULL(detalle_almacen.linea_idLinea,0) AS Linea, sum(lineas.PrecioProd * detalle_almacen.linea_idLinea)/1000 as costo,detalle_almacen.Cantidad')
                         ->leftJoin('detalle_almacen ON linea.idLinea = detalle_almacen.linea_idLinea')
                         ->leftJoin('linea as lineas on lineas.idLinea = detalle_almacen.linea_idLinea')
                         ->leftJoin('almacenes ON almacenes.idAlmacen = detalle_almacen.idAlmacen')
                         ->orderBy('linea_idLinea ASC')
                         ->groupBy('linea_idLinea')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

            public function listarInventario()
      {
        $data = $this->db->from($this->tableSecunda)
                         /* ->select('inventario.idInventario,inventario.Descripcion,IFNULL(detalle_almacen.idInventario,0) AS Inventario, sum( inventarios.Precio * detalle_almacen.Cantidad)/1000 as costo')
                         ->leftJoin('detalle_almacen ON inventario.idInventario = detalle_almacen.idInventario')
                         ->leftJoin('inventario as inventarios on inventarios.idInventario = detalle_almacen.idInventario') */
                         ->orderBy('idInventario DESC')
                         ->groupBy('idInventario')
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
  
    public function listarProd($var)
    {
        if ($var == 'mat_prima') {        
            $data = $this->db->from($this->table)
                        ->selct(null)
                        ->select('almacenes.idAlmacen,mat_prima.Mat_Prima,mat_prima.Descripcion,detalle_almacen.Cantidad,mat_prima.Unidad,almacenes.Nombre as Almacen')    
                        ->leftJoin('mat_prima on mat_prima.idMat_Prima = detalle_almacen.idMat_Prima')
                        ->leftJoin('almacenes on almacenes.idAlmacen = detalle_almacen.idAlmacen')
                        ->where('idProducto IS NULL')
                        ->fetchAll();
        }elseif ($var == 'producto' ) {
            $data = $this->db->from($this->table)
                         ->select(null)
                         ->select('almacenes.idAlmacen,almacenes.Nombre as Almacen,producto.Producto,detalle_almacen.Cantidad,detalle_almacen.Cantidad/12 as Cajas,producto.Descripcion')
                         ->leftJoin('almacenes on detalle_almacen.idAlmacen = almacenes.idAlmacen')
                         ->leftJoin('producto on detalle_almacen.idProducto = producto.idProducto')
                         ->orderBy('producto.Descripcion DESC')
                         ->where('detalle_almacen.idProducto is NOT NULL')
                         ->fetchAll();
        }

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function listarNoEx($id = 0 )
    {
        $data = $this->db->from($this->table)
                ->select(null)
                ->select('mat_prima.idMat_Prima,mat_prima.Mat_Prima,detalle_almacen.Cantidad,mat_prima.Unidad,detalle_almacen.idAlmacen')
                ->rightJoin('mat_prima on detalle_almacen.idMat_Prima = mat_prima.idMat_Prima')
                ->fetchAll();
                   
        $output = array();
        foreach($data as $d){
            if($id != $d->idAlmacen){
                $output[] = $d;
            }
        }
        return [
            'data'  => $output
            // 'total' => $total
         ];
    }

     public function obtener($id)
    {
       $data = $this->db->from($this->table)
                    ->select(null)
                    ->select('idDetalle_Almacen,detalle_almacen.idAlmacen,detalle_almacen.idMat_Prima,detalle_almacen.idMat_ind,detalle_almacen.linea_idLinea,detalle_almacen.Cantidad,mat_prima.Mat_Prima,mat_prima.Descripcion,mat_prima.Unidad,mat_prima.Precio')
                    ->where('detalle_almacen.idMat_Prima',$id)
                    //->leftJoin('almacenes ON detalle_almacen.idAlmacen = almacenes.idAlmacen')
                    ->leftJoin('mat_prima ON detalle_almacen.idMat_Prima = mat_prima.idMat_Prima')
                    ->fetchAll();//para un solo dato o linea
        return [
            'data'  => $data
        ];
    }

    public function registrarDetalle($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idDetalle_Almacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idDetalle_Almacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
