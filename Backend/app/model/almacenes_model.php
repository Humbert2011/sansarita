<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class AlmacenesModel
{
    private $db;
    private $table = 'almacenes';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($a)
    {
        $data = $this->db->from($this->table)
                        //  ->orderBy('idAlmacen DESC')
                        //  ->select('tipo_almacen.Descripcion AS TipoAlmacen')
                        //  ->leftJoin('tipo_almacen ON almacenes.Tipo = tipo_almacen.idTipo_almacen')
                         ->where('Activo',$a)
                         ->orderBy("idAlmacen ASC")
                         ->fetchAll();//para mas de un registro
        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
        
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idAlmacen',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idAlmacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idAlmacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
