<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class LoteproduccionModel
{
    private $db;
    private $table = 'lote_produccion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         ->orderBy('idLote_Produccion DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }

    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idLote_Produccion',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idLote_Produccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idLote_Produccion',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
