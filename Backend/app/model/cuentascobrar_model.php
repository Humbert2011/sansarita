<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class CuentascobrarModel
{
    private $db;
    private $table = 'cuentas_cobrar';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select('cliente.Cliente')
                         ->select('pedidos.Total')
                         ->select('transaccion.Monto_Pago')
                         ->select('transaccion.Fecha')
                         ->orderBy('idCuentas_Cobrar DESC')
                         ->leftJoin('cliente ON cliente.idCliente = cuentas_cobrar.idCliente')
                         ->leftJoin('pedidos ON pedidos.idPedidos= cuentas_cobrar.idPedidos')
                         ->leftJoin('transaccion ON transaccion.idTransaccion = cuentas_cobrar.idTransaccion')
                         ->orderBy('idCuentas_Cobrar ASC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }
        
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    ->where('idCuentas_Cobrar',$id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        // $data['Password'] = Cifrado::Sha512($data['Password']);

        $insertarProduccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProduccion;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idAlmacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idAlmacen',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
