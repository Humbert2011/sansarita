<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class PedidosModel
{
    private $db;
    private $table = 'pedidos';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

 
    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->select('cliente.Cliente')
                         // ->limit($l)
                         // ->offset($p)
                         ->leftJoin('cliente ON cliente.idCliente = pedidos.idCliente')
                         ->orderBy('idPedidos DESC')
                         ->fetchAll();//para mas de un registro

        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        return [
            'data'  => $data
            // 'total' => $total
        ];
    }



    public function obtener($id)
    {
       return $this->db->from($this->table)
                    ->where('idPedidos',$id)
                    ->fetch();//para un solo dato o linea
                    //->select(null)
                    //->select('idPedidos,pedidos.idPedidos,cliente.Cliente,pedidos.Fecha_Pedido,pedidos.Monto')
                    //->where('pedidos.idPedidos',$id)
                    //->leftJoin('cliente ON pedidos.idPedidos = pedidos.idPedidos')
                    //->leftJoin('mat_prima on detalle_almacen.idMat_Prima = mat_prima.idMat_Prima')
                    //->leftJoin('almacenes ON detalle_almacen.idAlmacen = almacenes.idAlmacen')
                   // ->fetchAll();

                   // return [
                     //   'data'  => $data
                        // 'total' => $total
                    //];
    }

    public function registrar($data)
    {
        $insertarProducto = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarProducto;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
      // if (isset($data['Password'])) {
      //   $data['Password'] = Cifrado::Sha512($data['Password']);
      // }

        $this->db->update($this->table, $data)
                ->where('idPedidos',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $this->db->deleteFrom($this->table)
                 ->where('idPedidos',$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
