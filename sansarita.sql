-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: sansarita
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `almacenes`
--

DROP TABLE IF EXISTS `almacenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacenes` (
  `idAlmacen` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Fecha_Baja` datetime DEFAULT NULL,
  `idDireccion` int(11) DEFAULT NULL,
  `Nombre` varchar(250) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Responsable` int(11) DEFAULT NULL,
  `Activo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idAlmacen`),
  KEY `fk_almacenes_direccion1_idx` (`idDireccion`),
  KEY `fk_almacenes_persona1_idx` (`Responsable`),
  CONSTRAINT `fk_almacenes_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_almacenes_persona1` FOREIGN KEY (`Responsable`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacenes`
--

LOCK TABLES `almacenes` WRITE;
/*!40000 ALTER TABLE `almacenes` DISABLE KEYS */;
INSERT INTO `almacenes` VALUES (1,'2019-09-05 10:40:11',NULL,NULL,'Stardust Materia Prima','Almacen de materia prima',NULL,0),(3,'2019-09-05 12:08:03',NULL,NULL,'Stardust Materia Industrial','Almacen de Materia Industrial',NULL,0),(4,'2019-09-05 12:39:35',NULL,NULL,'Stardust Inventario','Invetario',NULL,1),(5,'2019-09-05 12:39:54',NULL,NULL,'Stardust Producto','Producto Stardust',NULL,0);
/*!40000 ALTER TABLE `almacenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `idArea` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idArea`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Administracion'),(2,'Ventas'),(3,'Produccion'),(4,'Proveedor'),(5,'Cliente');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora` (
  `idBitacora` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idBitacora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora`
--

LOCK TABLES `bitacora` WRITE;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(2) NOT NULL,
  `Cliente` varchar(45) DEFAULT NULL,
  `Nombre_Comercial` varchar(250) DEFAULT NULL,
  `RFC` varchar(45) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `idPersona1` int(11) DEFAULT NULL,
  `idPersona2` int(11) DEFAULT NULL,
  `Personal` int(11) DEFAULT NULL,
  `Direccion` varchar(250) DEFAULT NULL,
  `Margen` double DEFAULT '0',
  `Activo` tinyint(4) DEFAULT '1',
  `CodigoPostal` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `fk_Cliente_Persona1_idx` (`idPersona1`),
  KEY `fk_Cliente_Persona2` (`idPersona2`),
  KEY `fk_cliente_Personal_idx` (`Personal`),
  CONSTRAINT `fk_Cliente_Persona1` FOREIGN KEY (`idPersona1`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Cliente_Persona2` FOREIGN KEY (`idPersona2`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_Personal` FOREIGN KEY (`Personal`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra` (
  `idcompra` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Total` double DEFAULT NULL,
  PRIMARY KEY (`idcompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra_producto`
--

DROP TABLE IF EXISTS `compra_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra_producto` (
  `idCompra_Producto` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo_producto` varchar(45) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Precio_total` decimal(4,2) NOT NULL,
  `Presentacion` varchar(45) DEFAULT NULL,
  `idProveedor` int(11) DEFAULT NULL,
  `idMat_Prima` int(11) DEFAULT NULL,
  `idMat_ind` int(11) DEFAULT NULL,
  `idcompra` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCompra_Producto`),
  KEY `fk_Compra_Producto_Proveedores_idx` (`idProveedor`),
  KEY `fk_Compra_Producto_Mat_Prima1_idx` (`idMat_Prima`),
  KEY `fk_Compra_Producto_Mat_ind1_idx` (`idMat_ind`),
  KEY `fk_compra_producto_compra1_idx` (`idcompra`),
  CONSTRAINT `fk_Compra_Producto_Mat_Prima1` FOREIGN KEY (`idMat_Prima`) REFERENCES `mat_prima` (`idMat_Prima`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_Producto_Mat_ind1` FOREIGN KEY (`idMat_ind`) REFERENCES `mat_ind` (`idMat_ind`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_Producto_Proveedores` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_producto_compra1` FOREIGN KEY (`idcompra`) REFERENCES `compra` (`idcompra`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra_producto`
--

LOCK TABLES `compra_producto` WRITE;
/*!40000 ALTER TABLE `compra_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `compra_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crédito`
--

DROP TABLE IF EXISTS `crédito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crédito` (
  `idCrédito` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Venta` int(11) DEFAULT NULL,
  `Saldo_Pagado` decimal(10,2) NOT NULL,
  `Adeudo` decimal(10,2) NOT NULL,
  `Plazo` datetime NOT NULL,
  `Créditocol` varchar(45) NOT NULL,
  `Venta_idVenta` int(11) NOT NULL,
  PRIMARY KEY (`idCrédito`,`Venta_idVenta`),
  KEY `fk_Crédito_Venta1_idx` (`Venta_idVenta`),
  CONSTRAINT `fk_Crédito_Venta1` FOREIGN KEY (`Venta_idVenta`) REFERENCES `venta` (`idVenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crédito`
--

LOCK TABLES `crédito` WRITE;
/*!40000 ALTER TABLE `crédito` DISABLE KEYS */;
/*!40000 ALTER TABLE `crédito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas_cobrar`
--

DROP TABLE IF EXISTS `cuentas_cobrar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas_cobrar` (
  `idCuentas_Cobrar` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(250) DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `idPedidos` int(11) DEFAULT NULL,
  `idTransaccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCuentas_Cobrar`),
  KEY `fk_cuentas_cobrar_cliente` (`idCliente`),
  KEY `fk_cuentas_cobrar_pedidos` (`idPedidos`),
  KEY `fk_cuentas_cobrar_transaccion` (`idTransaccion`),
  CONSTRAINT `fk_cuentas_cobrar_cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cuentas_cobrar_pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cuentas_cobrar_transaccion` FOREIGN KEY (`idTransaccion`) REFERENCES `transaccion` (`idTransaccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas_cobrar`
--

LOCK TABLES `cuentas_cobrar` WRITE;
/*!40000 ALTER TABLE `cuentas_cobrar` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuentas_cobrar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_almacen`
--

DROP TABLE IF EXISTS `detalle_almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_almacen` (
  `idDetalle_Almacen` int(11) NOT NULL AUTO_INCREMENT,
  `idAlmacen` int(11) NOT NULL,
  `idMat_Prima` int(11) DEFAULT NULL,
  `idMat_ind` int(11) DEFAULT NULL,
  `linea_idLinea` int(11) DEFAULT NULL,
  `idInventario` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDetalle_Almacen`),
  KEY `fk_detalle_almacen_almacenes1_idx` (`idAlmacen`),
  KEY `fk_detalle_almacen_mat_prima1_idx` (`idMat_Prima`),
  KEY `fk_detalle_almacen_mat_ind1_idx` (`idMat_ind`),
  KEY `fk_detalle_almacen_linea1_idx` (`linea_idLinea`),
  KEY `fk_detalle_almacen_inventario1_idx` (`idInventario`),
  CONSTRAINT `fk_detalle_almacen_almacenes1` FOREIGN KEY (`idAlmacen`) REFERENCES `almacenes` (`idAlmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_almacen_inventario1` FOREIGN KEY (`idInventario`) REFERENCES `inventario` (`idInventario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_almacen_linea1` FOREIGN KEY (`linea_idLinea`) REFERENCES `linea` (`idLinea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_almacen_mat_ind1` FOREIGN KEY (`idMat_ind`) REFERENCES `mat_ind` (`idMat_ind`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_almacen_mat_prima1` FOREIGN KEY (`idMat_Prima`) REFERENCES `mat_prima` (`idMat_Prima`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_almacen`
--

LOCK TABLES `detalle_almacen` WRITE;
/*!40000 ALTER TABLE `detalle_almacen` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_embalaje`
--

DROP TABLE IF EXISTS `detalle_embalaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_embalaje` (
  `iddetalle_embalaje` int(11) NOT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `idMat_ind` int(11) NOT NULL,
  `idEmbalaje` int(11) NOT NULL,
  PRIMARY KEY (`iddetalle_embalaje`),
  KEY `fk_detalle_embalaje_mat_ind1_idx` (`idMat_ind`),
  KEY `fk_detalle_embalaje_embalaje1_idx` (`idEmbalaje`),
  CONSTRAINT `fk_detalle_embalaje_embalaje1` FOREIGN KEY (`idEmbalaje`) REFERENCES `embalaje` (`idEmbalaje`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_embalaje_mat_ind1` FOREIGN KEY (`idMat_ind`) REFERENCES `mat_ind` (`idMat_ind`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_embalaje`
--

LOCK TABLES `detalle_embalaje` WRITE;
/*!40000 ALTER TABLE `detalle_embalaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_embalaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_pedidos`
--

DROP TABLE IF EXISTS `detalle_pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_pedidos` (
  `iddetalle_pedidos` int(11) NOT NULL AUTO_INCREMENT,
  `idProducto` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Descuento` double DEFAULT NULL,
  `Monto` double DEFAULT NULL,
  `idPedido` int(11) NOT NULL,
  PRIMARY KEY (`iddetalle_pedidos`),
  KEY `fk_detalle_pedidos_producto_idx` (`idProducto`),
  KEY `fkdetalle_peddos_pedidos_idx` (`idPedido`),
  CONSTRAINT `fk_detalle_pedidos_producto` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkdetalle_peddos_pedidos` FOREIGN KEY (`idPedido`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_pedidos`
--

LOCK TABLES `detalle_pedidos` WRITE;
/*!40000 ALTER TABLE `detalle_pedidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_presentacion`
--

DROP TABLE IF EXISTS `detalle_presentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_presentacion` (
  `iddetallePresentacion` int(11) NOT NULL AUTO_INCREMENT,
  `idPresentacion` int(11) NOT NULL,
  `idMat_ind` int(11) NOT NULL,
  `Cantidad` double DEFAULT NULL,
  PRIMARY KEY (`iddetallePresentacion`),
  KEY `fk_detallePresentacion_mat_ind1_idx` (`idMat_ind`),
  KEY `fk_detallePresentacion_presentacion1_idx` (`idPresentacion`),
  CONSTRAINT `fk_detallePresentacion_mat_ind1` FOREIGN KEY (`idMat_ind`) REFERENCES `mat_ind` (`idMat_ind`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detallePresentacion_presentacion1` FOREIGN KEY (`idPresentacion`) REFERENCES `presentacion` (`idPresentacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_presentacion`
--

LOCK TABLES `detalle_presentacion` WRITE;
/*!40000 ALTER TABLE `detalle_presentacion` DISABLE KEYS */;
INSERT INTO `detalle_presentacion` VALUES (1,3,1,1),(2,3,3,1),(3,3,2,1),(4,4,4,1),(5,4,5,1);
/*!40000 ALTER TABLE `detalle_presentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_produccion`
--

DROP TABLE IF EXISTS `detalle_produccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_produccion` (
  `iddetalle_produccion` int(11) NOT NULL AUTO_INCREMENT,
  `idMat_Prima` int(11) DEFAULT NULL,
  `Cantidad_Usa` int(11) DEFAULT NULL,
  `Unidad` varchar(45) DEFAULT NULL,
  `idLote_Produccion` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`iddetalle_produccion`),
  KEY `fk_detalle_produccion_produccion_idx` (`idLote_Produccion`),
  KEY `fk_detalle_produccion_mat_prima_idx` (`idMat_Prima`),
  CONSTRAINT `fk_detalle_produccion_mat_prima` FOREIGN KEY (`idMat_Prima`) REFERENCES `mat_prima` (`idMat_Prima`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_produccion_produccion` FOREIGN KEY (`idLote_Produccion`) REFERENCES `lote_produccion` (`idLote_Produccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_produccion`
--

LOCK TABLES `detalle_produccion` WRITE;
/*!40000 ALTER TABLE `detalle_produccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_produccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_turno`
--

DROP TABLE IF EXISTS `detalle_turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_turno` (
  `iddetalle_turno` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NOT NULL,
  `idturno` int(11) NOT NULL,
  PRIMARY KEY (`iddetalle_turno`),
  KEY `fk_detalle_turno_persona1_idx` (`idPersona`),
  KEY `fk_detalle_turno_turno1_idx` (`idturno`),
  CONSTRAINT `fk_detalle_turno_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_turno_turno1` FOREIGN KEY (`idturno`) REFERENCES `turno` (`idturno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_turno`
--

LOCK TABLES `detalle_turno` WRITE;
/*!40000 ALTER TABLE `detalle_turno` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_venta`
--

DROP TABLE IF EXISTS `detalle_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_venta` (
  `idDetalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `Precio_Unitario` decimal(10,2) DEFAULT NULL,
  `Precio_Venta` decimal(10,2) DEFAULT NULL,
  `Unidades` varchar(50) DEFAULT NULL,
  `Presentacion` varchar(45) DEFAULT NULL,
  `Subtotal` double DEFAULT NULL,
  `IVA` tinyint(1) DEFAULT NULL,
  `idVenta` int(11) NOT NULL,
  `idAlmacen` int(11) NOT NULL,
  PRIMARY KEY (`idDetalle_venta`),
  KEY `fk_Detalle_venta_Venta1_idx` (`idVenta`),
  KEY `fk_Detalle_venta_Almacen1_idx` (`idAlmacen`),
  CONSTRAINT `fk_Detalle_venta_Almacen1` FOREIGN KEY (`idAlmacen`) REFERENCES `almacenes` (`idAlmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Detalle_venta_Venta1` FOREIGN KEY (`idVenta`) REFERENCES `venta` (`idVenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_venta`
--

LOCK TABLES `detalle_venta` WRITE;
/*!40000 ALTER TABLE `detalle_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direccion` (
  `idDireccion` int(11) NOT NULL AUTO_INCREMENT,
  `Calle` varchar(100) NOT NULL,
  `Num` varchar(45) NOT NULL,
  `Colonia` varchar(45) NOT NULL,
  `Ciudad` varchar(45) NOT NULL,
  `Municipio` varchar(45) NOT NULL,
  `Estado` varchar(45) NOT NULL,
  `LonLat` varchar(45) NOT NULL,
  `idTrabajador` int(11) DEFAULT NULL,
  `idProveedor` int(11) DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDireccion`),
  KEY `fk_Direccion_Cliente1` (`idCliente`),
  KEY `fk_Direccion_Proveedor1` (`idProveedor`),
  KEY `fk_Direccion_Trabajador1` (`idTrabajador`),
  CONSTRAINT `fk_Direccion_Cliente1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Direccion_Proveedor1` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Direccion_Trabajador1` FOREIGN KEY (`idTrabajador`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embalaje`
--

DROP TABLE IF EXISTS `embalaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `embalaje` (
  `idEmbalaje` int(11) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Cantidad` varchar(45) DEFAULT NULL,
  `idLinea` int(11) NOT NULL,
  PRIMARY KEY (`idEmbalaje`),
  KEY `fk_embalaje_linea1_idx` (`idLinea`),
  CONSTRAINT `fk_embalaje_linea1` FOREIGN KEY (`idLinea`) REFERENCES `linea` (`idLinea`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `embalaje`
--

LOCK TABLES `embalaje` WRITE;
/*!40000 ALTER TABLE `embalaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `embalaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gasto`
--

DROP TABLE IF EXISTS `gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gasto` (
  `idGasto` int(11) NOT NULL AUTO_INCREMENT,
  `idTipo_Gasto` int(11) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Concepto` varchar(200) DEFAULT NULL,
  `Precio` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idGasto`),
  KEY `fk_gasto_tipo_gasto_idx` (`idTipo_Gasto`),
  CONSTRAINT `fk_gasto_tipo_gasto` FOREIGN KEY (`idTipo_Gasto`) REFERENCES `tipo_gasto` (`idTipo_Gasto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gasto`
--

LOCK TABLES `gasto` WRITE;
/*!40000 ALTER TABLE `gasto` DISABLE KEYS */;
INSERT INTO `gasto` VALUES (1,'2019-09-17','Gasolina','1','250');
/*!40000 ALTER TABLE `gasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario` (
  `idInventario` int(11) NOT NULL AUTO_INCREMENT,
  `Elemento` varchar(250) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idInventario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea` (
  `idLinea` int(11) NOT NULL AUTO_INCREMENT,
  `idProducto` int(11) NOT NULL,
  `idPresentacion` int(11) NOT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `PrecioProd` double DEFAULT NULL,
  `PrecioDistribuidor` double DEFAULT NULL,
  `PrecioPublico` double DEFAULT NULL,
  `CodigoBarras` varchar(45) DEFAULT NULL,
  `ContenidoNeto` double DEFAULT NULL,
  `Unidad` varchar(45) DEFAULT 'gr',
  PRIMARY KEY (`idLinea`),
  KEY `fk_producto_has_presentacion_presentacion1_idx` (`idPresentacion`),
  KEY `fk_producto_has_presentacion_producto1_idx` (`idProducto`),
  CONSTRAINT `fk_producto_has_presentacion_presentacion1` FOREIGN KEY (`idPresentacion`) REFERENCES `presentacion` (`idPresentacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_has_presentacion_producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` VALUES (1,3,3,'Almendra 195 gr',36.119,NULL,115,'7500463491320',195,'gr'),(2,2,3,'Cacahuate 210 gr',24.966,NULL,80,'7500463491313',210,'gr'),(3,1,3,'Morita 210 gr',25.963,NULL,80,'7500463491306',210,'gr'),(4,3,4,'Almendra 4 Kg',496.696,NULL,700,'7500463491320',4000,'gr');
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lote_produccion`
--

DROP TABLE IF EXISTS `lote_produccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lote_produccion` (
  `idLote_Produccion` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Nomenclatura` varchar(45) DEFAULT NULL,
  `Cantidad_Real` int(11) DEFAULT NULL,
  `Cantidad_Deseada` int(11) DEFAULT NULL,
  `Merma` int(11) DEFAULT NULL,
  `Fecha_Elaboracion` datetime DEFAULT NULL,
  `Fecha_Expiracion` datetime DEFAULT NULL,
  `idProduccion` int(11) DEFAULT NULL,
  `Monto` double DEFAULT NULL,
  `idLinea` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLote_Produccion`),
  KEY `fk_produccion_idLote_produccion` (`idProduccion`),
  KEY `fk_lote_produccion_producto_has_presentacion1_idx` (`idLinea`),
  CONSTRAINT `fk_lote_produccion_producto_has_presentacion1` FOREIGN KEY (`idLinea`) REFERENCES `linea` (`idLinea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_produccion_idLote_produccion` FOREIGN KEY (`idProduccion`) REFERENCES `produccion` (`idProduccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lote_produccion`
--

LOCK TABLES `lote_produccion` WRITE;
/*!40000 ALTER TABLE `lote_produccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `lote_produccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_ind`
--

DROP TABLE IF EXISTS `mat_ind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mat_ind` (
  `idMat_ind` int(11) NOT NULL AUTO_INCREMENT,
  `Mat_ind` varchar(250) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Cantidad` double DEFAULT NULL,
  `Precentacion` varchar(45) DEFAULT NULL,
  `Unidad` varchar(45) DEFAULT NULL,
  `CodigoBarras` varchar(45) DEFAULT NULL,
  `Precio` double DEFAULT NULL,
  PRIMARY KEY (`idMat_ind`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_ind`
--

LOCK TABLES `mat_ind` WRITE;
/*!40000 ALTER TABLE `mat_ind` DISABLE KEYS */;
INSERT INTO `mat_ind` VALUES (1,'Etiquetas 1','Etiquetas tipo 1 frascos de 7 onzas',1,'Rollo','Pz','00',6.26),(2,'Tapa 1','Tapa para frasco de 7 oz color negro',1,'Granel','Pz','00',1.41),(3,'Frasco 7oz','Frasco de cristal de 7oz',1,'Caja de 12','Pz','00',5.16),(4,'Cubeta 4 Kg','Cudeta de 4Kg',1,'Granel','Pz','00',12.8),(5,'Etiqueta 2','Etiqueta para cubeta',1,'Granel','Pz','00',6.18),(6,'Caja de corrugado','Caja de cartón para 12 frascos de 7 oz',1,'Granel','Pz','00',0.5);
/*!40000 ALTER TABLE `mat_ind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_prima`
--

DROP TABLE IF EXISTS `mat_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mat_prima` (
  `idMat_Prima` int(11) NOT NULL AUTO_INCREMENT,
  `Mat_Prima` varchar(50) NOT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Cantidad` double DEFAULT NULL,
  `Precentacion` varchar(45) DEFAULT NULL,
  `Unidad` varchar(45) DEFAULT NULL,
  `CodigoBarras` varchar(13) NOT NULL,
  `Precio` double DEFAULT NULL,
  PRIMARY KEY (`idMat_Prima`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_prima`
--

LOCK TABLES `mat_prima` WRITE;
/*!40000 ALTER TABLE `mat_prima` DISABLE KEYS */;
INSERT INTO `mat_prima` VALUES (1,'Aceite','aceite de canola',1,'Garrafon','ml','00',0.028),(2,'ChileM','Chile Morita',1,'Granel','gr','00',0.06),(3,'Chiltepin','Chile chiltepin',1,'Granel','gr','00',0.25),(4,'Cebolla','Cebollas',1,'Granel','gr','00',0.003),(5,'Tocineta','Tocineta de cerdo',1,'Granel','gr','00',0.08),(6,'Cacahuate','cacahuate peledo',1,'Granel','gr','00',0.048),(7,'Ajo','Ajo entero',1,'Granel','gr','00',2),(8,'Almendra','Almendra pelada',1,'Granel','gr','00',0.188),(10,'Sal','Sal',1,'Bolsa','gr','00',0.011),(11,'Manzano','Chile Manzano',0,'Granel','gr','00',0.033),(12,'Habanero','Chile Habanero',0,'Granel','gr','00',0.1),(13,'Miel','Miel de abeja natural',0,'Litro','ml','00',0.8),(14,'Pasas','Pasas naturales',1,'Granel','gr','00',1);
/*!40000 ALTER TABLE `mat_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos`
--

DROP TABLE IF EXISTS `movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientos` (
  `idMovimientos` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idMovimientos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos`
--

LOCK TABLES `movimientos` WRITE;
/*!40000 ALTER TABLE `movimientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos` (
  `idPedidos` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) DEFAULT NULL,
  `Fecha_Pedido` datetime DEFAULT CURRENT_TIMESTAMP,
  `Total` double DEFAULT NULL,
  `SubTotal` double DEFAULT NULL,
  `IVA` tinyint(1) DEFAULT NULL,
  `Notas` varchar(250) DEFAULT NULL,
  `Politicas` varchar(250) DEFAULT NULL,
  `Fecha_Entrega` datetime DEFAULT NULL,
  PRIMARY KEY (`idPedidos`),
  KEY `fk_pedidos_cliente` (`idCliente`),
  CONSTRAINT `fk_pedidos_cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `Apellidos` varchar(70) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Email` varchar(70) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Foto` varchar(250) DEFAULT NULL,
  `Fecha_Naci` datetime DEFAULT NULL,
  `Fecha_Alta` datetime DEFAULT CURRENT_TIMESTAMP,
  `FechaBaja` datetime DEFAULT NULL,
  `Grado_estudios` varchar(70) DEFAULT NULL,
  `idtipoPersona` int(11) NOT NULL,
  `idArea` int(11) NOT NULL,
  `Activo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idPersona`),
  KEY `fk_Trabajador_Area1` (`idArea`),
  KEY `fk_Trabajador_tipoTrabajador1` (`idtipoPersona`),
  CONSTRAINT `fk_Trabajador_Area1` FOREIGN KEY (`idArea`) REFERENCES `area` (`idArea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Trabajador_tipoTrabajador1` FOREIGN KEY (`idtipoPersona`) REFERENCES `tipopersona` (`idtipoPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Humberto','Martinez Cuautenco','7761052213','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','http://localhost:8080/sansarita/Backend/img/AMOR MIO.png',NULL,NULL,NULL,'Ing',1,1,1),(2,'Humberto','Martinez Cuautenco','7761052213','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','http://localhost:8080/sansarita/Backend/img/AMOR MIO.png',NULL,NULL,NULL,'Ing',5,5,1),(3,'Joel','Guerra Flores','7767673546','joel@stardust.com.mx','$6$rounds=5000$@startDustH00l10$MGRB3IZrx9Ju.kBvvt4KiRfWXgxJNmFPEnnSglm102dl8GeKJKd3pzdKssnt.0p7DOO0fqMkZ1zNk5Z.qLy04/','http://localhost:8080/sansarita/Backend/img/1_20160726-0004.jpeg',NULL,NULL,NULL,'Universidad',1,1,1),(4,'Carlos','Hdz Sanchez','0987654321','jorge@stardust.com.mx','$6$rounds=5000$@startDustH00l10$zKfxtReaqOHyNMu1KOSQC18.sf8s13zxH5b9QNB2qKnt.TCWTarNIG5wj207BRfhkibaRwudYflt4mCO2k7XU1','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'Universidad',4,4,1),(5,'Felipe','Duran','1234567890','juan@gmail.com','$6$rounds=5000$@startDustH00l10$zKfxtReaqOHyNMu1KOSQC18.sf8s13zxH5b9QNB2qKnt.TCWTarNIG5wj207BRfhkibaRwudYflt4mCO2k7XU1','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'Universidad',4,4,1),(6,'Jorge','Perez','0987654321','jorge@stardust.com.mx','$6$rounds=5000$@startDustH00l10$zKfxtReaqOHyNMu1KOSQC18.sf8s13zxH5b9QNB2qKnt.TCWTarNIG5wj207BRfhkibaRwudYflt4mCO2k7XU1','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'sexto',2,2,1),(7,'Juan','hdz','12345678990','juan@gmail.com','$6$rounds=5000$@startDustH00l10$sLXPbb.IaXAm.Mn3rwFKEl.9nyo5yAOYo49vcZZbXFRbaDE6YVucZdyQksj5jIMYF82dM9CeoVFVwcY1IOmG00','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'Universidad',3,3,1),(8,'Alex','Sosa','0987654321','alex@hotmail.com','$6$rounds=5000$@startDustH00l10$sLXPbb.IaXAm.Mn3rwFKEl.9nyo5yAOYo49vcZZbXFRbaDE6YVucZdyQksj5jIMYF82dM9CeoVFVwcY1IOmG00','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'sexto',3,3,1),(9,'Hum','Hernandez','1234567890','hum@gmail.com','$6$rounds=5000$@startDustH00l10$sLXPbb.IaXAm.Mn3rwFKEl.9nyo5yAOYo49vcZZbXFRbaDE6YVucZdyQksj5jIMYF82dM9CeoVFVwcY1IOmG00','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'Primaria',4,4,1),(10,'Miriam','Sosa','0987654321','miriam@stardust.com.mx','$6$rounds=5000$@startDustH00l10$zKfxtReaqOHyNMu1KOSQC18.sf8s13zxH5b9QNB2qKnt.TCWTarNIG5wj207BRfhkibaRwudYflt4mCO2k7XU1','C:\\AppServ\\www\\sansarita\\Backend\\img/',NULL,NULL,NULL,'Primaria',3,3,1),(13,'Luis','Dionisio Martinez','4443382696','luis@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','http://localhost:8080/sansarita/Backend/img/20160721-0001.jpeg.[t=1153564056,m=5].jpg',NULL,'2019-03-16 21:43:37',NULL,'Universidad',1,1,1),(14,'Humbert','Martinez','7761052213','hum@123123.com',NULL,NULL,NULL,'2019-04-22 14:53:19',NULL,NULL,5,5,1),(15,'Belen','Rodrigues','779456023','econosuoercompras@gmail.com',NULL,NULL,NULL,'2019-04-22 15:12:03',NULL,NULL,5,5,1),(16,'hom','mero','9960203110','hom@star.com.mx',NULL,NULL,NULL,'2019-04-22 15:12:29',NULL,NULL,5,5,1),(17,'Philip','Colman Ramos','5522785960','salomon@fg.com.mx',NULL,NULL,NULL,'2019-04-22 15:13:10',NULL,NULL,5,5,1),(18,'Amber','Mar','889456133','linua@hotmail.com',NULL,NULL,NULL,'2019-04-22 16:40:10',NULL,NULL,5,5,1),(19,'Lucas','Qwerty','7761035689','luo@hotmail.com',NULL,NULL,NULL,'2019-04-22 16:40:42',NULL,NULL,5,5,1),(20,'bety','garcia','773105889','bety@gmail.com',NULL,NULL,NULL,'2019-04-22 22:59:27',NULL,NULL,5,5,1),(21,'lucy','garcia','7761052269','Lucy@gmail.com',NULL,NULL,NULL,'2019-04-22 22:59:53',NULL,NULL,5,5,1),(22,'Roxana','Martinez','7745123690','rox@hotmail.com',NULL,NULL,NULL,'2019-04-23 11:55:49',NULL,NULL,5,5,1),(23,'Andres','Lopez Castro','1234567890','andres@gmail.com',NULL,NULL,NULL,'2019-05-06 14:37:01',NULL,NULL,5,5,1),(24,'Belen','Algo','78945555','belen@gmail.com',NULL,NULL,NULL,'2019-07-30 12:28:56',NULL,NULL,5,5,1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presentacion`
--

DROP TABLE IF EXISTS `presentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentacion` (
  `idPresentacion` int(11) NOT NULL AUTO_INCREMENT,
  `Presentacion` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idPresentacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presentacion`
--

LOCK TABLES `presentacion` WRITE;
/*!40000 ALTER TABLE `presentacion` DISABLE KEYS */;
INSERT INTO `presentacion` VALUES (3,'Presentación estándar','frasco de 7 oz y tapa negra'),(4,'Cubeta','Cubeta de producto de 4 Kg');
/*!40000 ALTER TABLE `presentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produccion`
--

DROP TABLE IF EXISTS `produccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produccion` (
  `idProduccion` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Monto` double DEFAULT NULL,
  PRIMARY KEY (`idProduccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produccion`
--

LOCK TABLES `produccion` WRITE;
/*!40000 ALTER TABLE `produccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `produccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `Producto` varchar(150) DEFAULT NULL,
  `Descripcion` varchar(250) NOT NULL,
  `PrecioProd` double NOT NULL,
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'Sansarita Morita','salsa de chile Morita con Tocino',0,1),(2,'Sansarita Cacahuate','Salsa de chiltepin con cacahuate',0,1),(3,'Sansarita Almendra','Salse de Almendar con chile habanero y manzano',0,1),(4,'Sansarita Pasas','Salsa de miel y pasas',0,1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL AUTO_INCREMENT,
  `Proveedor` varchar(45) DEFAULT NULL,
  `Direccion` varchar(45) DEFAULT NULL,
  `Telefono` varchar(11) DEFAULT NULL,
  `RFC` varchar(15) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `idPersona2` int(11) DEFAULT NULL,
  `Activo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idProveedor`),
  KEY `fk_Proveedor_Persona2` (`idPersona2`),
  KEY `fk_Proveedor_Persona1` (`idPersona`),
  CONSTRAINT `fk_Proveedor_Persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Proveedor_Persona2` FOREIGN KEY (`idPersona2`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receta`
--

DROP TABLE IF EXISTS `receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receta` (
  `idReceta` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Elab` datetime DEFAULT CURRENT_TIMESTAMP,
  `Cantidad` double DEFAULT NULL,
  `idMat_Prima` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  PRIMARY KEY (`idReceta`),
  KEY `fk_Receta_Mat_Prima1_idx` (`idMat_Prima`),
  KEY `fk_Receta_Producto1_idx` (`idProducto`),
  CONSTRAINT `fk_Receta_Mat_Prima1` FOREIGN KEY (`idMat_Prima`) REFERENCES `mat_prima` (`idMat_Prima`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Receta_Producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receta`
--

LOCK TABLES `receta` WRITE;
/*!40000 ALTER TABLE `receta` DISABLE KEYS */;
INSERT INTO `receta` VALUES (1,'2019-08-07 12:11:36',821,1,3),(2,'2019-08-07 12:11:36',5,7,3),(3,'2019-08-07 12:11:36',205,8,3),(4,'2019-08-07 12:11:36',205,12,3),(5,'2019-08-07 12:11:36',821,11,3),(6,'2019-08-07 12:11:36',28,10,3),(7,'2019-08-07 12:22:25',417,1,2),(8,'2019-08-07 12:22:25',547,6,2),(9,'2019-08-07 12:22:25',78,3,2),(10,'2019-08-07 12:22:25',27,10,2),(11,'2019-08-07 12:22:25',19.754,4,2),(12,'2019-08-07 13:29:03',635,1,1),(13,'2019-08-07 13:29:03',19.754,4,1),(14,'2019-08-07 13:29:03',317,2,1),(15,'2019-08-07 13:29:03',29,10,1),(16,'2019-08-07 13:29:03',317,5,1);
/*!40000 ALTER TABLE `receta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopersona`
--

DROP TABLE IF EXISTS `tipopersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopersona` (
  `idtipoPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipoPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopersona`
--

LOCK TABLES `tipopersona` WRITE;
/*!40000 ALTER TABLE `tipopersona` DISABLE KEYS */;
INSERT INTO `tipopersona` VALUES (1,'Administrador'),(2,'Vendedor'),(3,'Cosinero'),(4,'Proveedor'),(5,'Cliente');
/*!40000 ALTER TABLE `tipopersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_gasto`
--

DROP TABLE IF EXISTS `tipo_gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_gasto` (
  `idTipo_Gasto` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo_Gasto` varchar(200) DEFAULT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idTipo_Gasto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `transaccion`
--

DROP TABLE IF EXISTS `transaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccion` (
  `idTransaccion` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(50) DEFAULT NULL,
  `Monto_Pago` double DEFAULT NULL,
  `Fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `idCliente` int(11) DEFAULT NULL,
  `idPedidos` int(11) DEFAULT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  `idProveedor` int(11) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTransaccion`),
  KEY `fk_transaccion_pedidos_idx` (`idPedidos`),
  KEY `fk_transaccion_proveedor_idx` (`idProveedor`),
  KEY `fk_transaccion_persona_idx` (`idPersona`),
  KEY `fk_transaccion_cliente` (`idCliente`),
  CONSTRAINT `fk_transaccion_cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaccion_pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaccion_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaccion_proveedor` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='				';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaccion`
--

LOCK TABLES `transaccion` WRITE;
/*!40000 ALTER TABLE `transaccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `idturno` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Encargado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idturno`),
  KEY `fk_turno_persona1_idx` (`Encargado`),
  CONSTRAINT `fk_turno_persona1` FOREIGN KEY (`Encargado`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Total_Pago` decimal(10,2) DEFAULT NULL,
  `idPersona` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idPedidos` int(11) NOT NULL,
  `idTransaccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idVenta`),
  KEY `fk_Venta_Trabajador1_idx` (`idPersona`),
  KEY `fk_Venta_Cliente1_idx` (`idCliente`),
  KEY `fk_Venta_Pedidos_idx` (`idPedidos`),
  KEY `fk_Venta_transaccion` (`idTransaccion`),
  CONSTRAINT `fk_Venta_Cliente1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_Pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_transaccion` FOREIGN KEY (`idTransaccion`) REFERENCES `transaccion` (`idTransaccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 11:06:48
